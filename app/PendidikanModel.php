<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PendidikanModel extends Model
{
    //
    public $timestamps = false;
    protected $table = 'data_pendidikan';
    protected $primaryKey = 'ID_DATA_PEND';
    public $incrementing = false;

    protected $fillable = ['ID_DATA_PEND','ID_PENDAFTAR','SD','LOKASI_SD','THN_LULUS_SD','SMP','LOKASI_SMP','THN_LULUS_SMP','SMA','LOKASI_SMA','THN_LULUS_SMA','PEND_TINGGI','LOKASI_PEND_TINGGI','THN_LULUS_PEND_TINGGI'];
}
