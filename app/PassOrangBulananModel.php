<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PassOrangBulananModel extends Model
{
    //
    public $timestamps = false;
    protected $table = 'pass_orang_bulan';
    protected $fillable = ['SERI_PASS_ORG_BLN', 'ID_PENDAFTAR', 'ID_JENIS_PASS', 'ID_TRANSAKSI', 'TGL_MULAI_ORG_BLN', 'TGL_SELESAI_ORG_BLN', 'STATUS_ORG_BLN', 'KET_ZONA_BLN'];

    protected $primaryKey = 'SERI_PASS_ORG_BLN';

    public function TransaksiIDBLN1()
    {
        return $this->belongsTo('App\TransaksiModel', 'ID_TRANSAKSI');
    }

    public function PendaftarIDBLN1()
    {
        return $this->belongsTo('App\PendaftarModel', 'ID_PENDAFTAR');
    }

    public function JenisPassBln()
    {
        return $this->belongsTo('App\JenisPassModel', 'ID_JENIS_PASS');
    }
}
