<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisPassModel extends Model
{
    //
    public $timestamps = false;
    protected $table = 'jenis_pass';
    protected $primaryKey ='ID_JENIS_PASS';
    protected $fillable = ['ID_JENIS_PASS','NAMA_JENIS_PASS'];

}
