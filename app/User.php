<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $username = 'username';
    public $timestamps = false;
    protected $table = 'user';
    protected $fillable = [
        'id_user', 'id_unit', 'id_role','NAMA_USER','username','password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $primaryKey = 'id_user';
    public function Role()
        {
            return $this->belongsTo('App\Role','id_role');
        }
    public function UnitKerja()
    		{
    		    return $this->belongsTo('App\UnitKerja','id_unit');
    		}
    public function setPasswordAttribute($value)
        {
        $this->attributes['password'] = \Hash::make($value);
        }
}
