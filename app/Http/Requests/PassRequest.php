<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PassRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ID_TRANSAKSI' => 'required',
            'ID_PENDAFTAR' => 'required',
            'NAMA_PENDAFTAR' => 'required',
            'NO_IDENTITAS' => 'required',
            'TEMPAT_LAHIR' => 'required',
            'INSTANSI' => 'required',
            'ALAMAT_KANTOR' => 'required',
            'ALAMAT_RUMAH' => 'required',
            'NO_TELP' => 'required',
            'NO_TELP_KANTOR' => 'required',
            'ktp' => 'required',
            'cv' => 'required',
            'skck' => 'required'

        ];
    }

    public function messages()
    {
        return [
            'NAMA_PENDAFTAR.required' => 'Nama Tidak boleh kosong',
            'NO_IDENTITAS.required' => 'No Identitas tidak boleh kosong',
            'TEMPAT_LAHIR.required' => 'Tempat lahir tidak boleh kosong',
            'INSTANSI.required' => 'Instansi tidak boleh kosong',
            'ALAMAT_KANTOR.required' => 'Alamat Kantor tidak boleh kosong',
            'ALAMAT_RUMAH.required' => 'Alamat Rumah tidak boleh kosong',
            'NO_TELP.required' => 'No telepon tidak boleh kosong',
            'NO_TELP_KANTOR.required' => 'No Telepon kantor tidak boleh kosong',
            'ktp.required' => 'File KTP Tidak boleh kosong',
            'cv.required' => 'File CV tidak boleh kosong',
            'skck.required' => 'File SKCK tidak boleh kosong'
        ];
    }

}
