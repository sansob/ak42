<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\KendaraanModel;

class KendaraanControl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('data-kendaraan');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $user_data = $request->except('_token');
        KendaraanModel::create($user_data);
        return redirect()->route('kendaraan.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $kendaraan = KendaraanModel::where('ID_KENDARAAN', '=', $id)->first();
        return view('kendaraan-update', compact('kendaraan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $user_data = $request->except('_token');
        DB::table('data_kendaraan')
            ->where('ID_KENDARAAN', $user_data['ID_KENDARAAN'])
            ->update(['NAMA_KENDARAAN' => $user_data['NAMA_KENDARAAN'], 'ID_JENIS_KEND' => $user_data['ID_JENIS_KEND'], 'NO_POLISI' => $user_data['NO_POLISI'], 'WARNA_PLAT' => $user_data['WARNA_PLAT']]);
        return redirect()->route('kendaraan.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
