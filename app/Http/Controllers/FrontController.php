<?php

namespace App\Http\Controllers;

use App\PendaftarModel;
use App\TransaksiModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use UxWeb\SweetAlert\SweetAlert as Alert;

class FrontController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('front.index');
    }

    public function dashboard()
    {
        $sessid = \session('trans_id');
        $cekTransaksi = PendaftarModel::where('NO_IDENTITAS', $sessid)->first();
        return view('front.dashboard')
            ->with('status', $cekTransaksi);
    }

    public function printNota(){
        return view('front.nota');
    }

    public function status()
    {
        return view('front.status');
    }

    public function cek(Request $request)
    {
        $user_data = $request->except('_token');

        $cekT = $user_data['ID_TRANSAKSI'];
        $cekTransaksi = PendaftarModel::where('NO_IDENTITAS', $cekT)->first();


        if ($cekTransaksi == null) {
            alert()->error('Transaksi Tidak Ditemukan', 'Error')->persistent('Tutup');
            return redirect(route('front.status'));
        } else {
            \session(['trans_id' => $cekT]);
            return redirect(route('front.dashboard'));
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
