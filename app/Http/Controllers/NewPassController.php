<?php

namespace App\Http\Controllers;

use App\Http\Requests\PassRequest;
use App\PassOrangBulanModel;
use App\PassOrangTahunModel;
use App\TransaksiModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;

use Illuminate\Support\Facades\Session;
use UxWeb\SweetAlert\SweetAlert as Alert;

class NewPassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Session::forget('trans_id');
        $rr = "TR-" . date("ymdhms") . rand(0, 99);

        $getmax = DB::table('data_pendaftar')->max('ID_PENDAFTAR');
        if ($getmax == null) {
            $next = "0001";
        } else {
            $angka = substr($getmax, 3, 4);
            $next = str_repeat("0", 4 - strlen($angka + 1)) . ($angka + 1);
        }
        $id = "PD-" . $next;


        return view('form-front.newpass')
            ->with('randoms', $rr)
            ->with('idpendaftar', $id);
    }

    public function pass()
    {
        $getmax = DB::table('data_pendaftar')->max('ID_PENDAFTAR');
        if ($getmax == null) {
            $next = "0001";
        } else {
            $angka = substr($getmax, 3, 4);
            $next = str_repeat("0", 4 - strlen($angka + 1)) . ($angka + 1);
        }
        $id = "PD-" . $next;


        return view('form-front.pass')
            ->with('idpendaftar', $id);

    }

    public function register()
    {
        Session::forget('trans_id');

        $rr = "TR-" . date("ymdhms") . rand(0, 99);

        return view('form-front.register')
            ->with('idpendaftar', $rr);
    }

    public function registergo(Request $request)
    {
        $user_data = $request->except('_token');

        $idtrans = $user_data['ID_TRANSAKSI'];

        //$user_data['STATUS_TRANSAKSI'] = 'Belum Ajukan Konfirmasi';
        DB::table('transaksi_pass')->insert(
            ['ID_TRANSAKSI' => $user_data['ID_TRANSAKSI'],
                'NAMA_PEMOHON' => $user_data['NAMA_PEMOHON'],
                'NO_SURAT_PEMOHON' => $user_data['NO_SURAT_PEMOHON'],
                'STATUS_TRANSAKSI' => "Belum Ajukan Konfirmasi",
                'TGL_TRANSAKSI' => date('Y-m-d H:i:s')

            ]);

        \session(['trans_id' => $idtrans]);
        alert()
            ->success('Silakan mengisi formulir dengan lengkap', 'Informasi')
            ->persistent('Tutup');
        return redirect(route('front.pass'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function perpanjang()
    {

    }

    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PassRequest $request)
    {
       

        $uploadedKTP = $request->file('ktp');
        $uploadedSKCK = $request->file('skck');
        $uploadedDokumen = $request->file('dokumen');
        $uploadedCV = $request->file('cv');
        $uploadedNPWP = $request->file('npwp');

        $destinationPath = 'uploads';


        $user_data = $request->except('_token');
        $MASA_KERJA_join = $user_data['MASA_KERJA_tahun'] . " tahunan " . $user_data['MASA_KERJA_bulan'] . " bulan";
        $pendaftar = DB::table('data_pendaftar')->where('STATUS_PENDAFTAR', '=', 'Blacklist')->where('NAMA_PENDAFTAR', '=', $user_data['NAMA_PENDAFTAR'])->orWhere('NO_IDENTITAS', '=', $user_data['NO_IDENTITAS'])->orWhere('NO_PEGAWAI', '=', $user_data['NO_PEGAWAI'])->first();

        $idtrans = $user_data['ID_TRANSAKSI'];

        if ($pendaftar == null) {

            DB::table('data_pendaftar')->insert(['ID_PENDAFTAR' => $user_data['ID_PENDAFTAR'], 'NAMA_PENDAFTAR' => $user_data['NAMA_PENDAFTAR'], 'NO_IDENTITAS' => $user_data['NO_IDENTITAS'], 'NO_PEGAWAI' => $user_data['NO_PEGAWAI'], 'TEMPAT_LAHIR' => $user_data['TEMPAT_LAHIR'], 'TGL_LAHIR' => $user_data['TGL_LAHIR'],
                'AGAMA' => $user_data['AGAMA'], 'INSTANSI' => $user_data['INSTANSI'], 'ALAMAT_KANTOR' => $user_data['ALAMAT_KANTOR'], 'ALAMAT_RUMAH' => $user_data['ALAMAT_RUMAH'], 'NO_TELP' => $user_data['NO_TELP'], 'NO_TELP_KANTOR' => $user_data['NO_TELP_KANTOR'], 'PEKERJAAN' => $user_data['PEKERJAAN'], 'STATUS_KERJA' => $user_data['STATUS_KERJA'],
                'MASA_KERJA' => $MASA_KERJA_join, 'TEMPAT_KERJA_SBLMNYA' => $user_data['TEMPAT_KERJA_SBLMNYA'], 'STATUS_PENDAFTAR' => 'Terdaftar'
            ]);

            if ($uploadedKTP != NULL) {
                $foo = \File::extension($uploadedKTP->getClientOriginalName());
                $nameKTP = "ktp_" . rand(10, 99999) . $idtrans . "." . $foo;
                $uploadedKTP->move($destinationPath, $nameKTP);

                DB::table('documents')->insert(
                    ['ID_TRANSAKSI' => $user_data['ID_TRANSAKSI'],
                        'DOCUMENT_NAME' => $nameKTP,
                        'DOCUMENT_TYPE' => "KTP",
                        'ID_PENDAFTAR' => $user_data['ID_PENDAFTAR']
                    ]);
            }
            if ($uploadedSKCK != NULL) {
                $foo = \File::extension($uploadedSKCK->getClientOriginalName());
                $nameSKCK = "skck_" . rand(10, 99999) . $idtrans . "." . $foo;
                $uploadedSKCK->move($destinationPath, $nameSKCK);

                DB::table('documents')->insert(
                    ['ID_TRANSAKSI' => $user_data['ID_TRANSAKSI'],
                        'DOCUMENT_NAME' => $nameSKCK,
                        'DOCUMENT_TYPE' => "SKCK",
                        'ID_PENDAFTAR' => $user_data['ID_PENDAFTAR']
                    ]);
            }
            if ($uploadedDokumen != NULL) {
                $foo = \File::extension($uploadedDokumen->getClientOriginalName());
                $nameDokumen = "dok_" . rand(10, 99999) . $idtrans . "." . $foo;
                $uploadedDokumen->move($destinationPath, $nameDokumen);
                DB::table('documents')->insert(
                    ['ID_TRANSAKSI' => $user_data['ID_TRANSAKSI'],
                        'DOCUMENT_NAME' => $nameDokumen,
                        'DOCUMENT_TYPE' => "DOKUMEN",
                        'ID_PENDAFTAR' => $user_data['ID_PENDAFTAR']
                    ]);
            }

            if ($uploadedCV != NULL) {
                $foo = \File::extension($uploadedCV->getClientOriginalName());
                $nameDokumen = "cv_" . rand(10, 99999) . $idtrans . "." . $foo;
                $uploadedCV->move($destinationPath, $nameDokumen);
                DB::table('documents')->insert(
                    ['ID_TRANSAKSI' => $user_data['ID_TRANSAKSI'],
                        'DOCUMENT_NAME' => $nameDokumen,
                        'DOCUMENT_TYPE' => "CV",
                        'ID_PENDAFTAR' => $user_data['ID_PENDAFTAR']
                    ]);
            }

            if ($uploadedNPWP != NULL) {
                $foo = \File::extension($uploadedNPWP->getClientOriginalName());
                $nameDokumen = "npwp_" . rand(10, 99999) . $idtrans . "." . $foo;
                $uploadedNPWP->move($destinationPath, $nameDokumen);
                DB::table('documents')->insert(
                    ['ID_TRANSAKSI' => $user_data['ID_TRANSAKSI'],
                        'DOCUMENT_NAME' => $nameDokumen,
                        'DOCUMENT_TYPE' => "NPWP",
                        'ID_PENDAFTAR' => $user_data['ID_PENDAFTAR']
                    ]);
            }

            $curYear = intval(date("Y"));
            if ($user_data['PERIODE'] == "Bulanan") {
                $OrangBulan = PassOrangBulanModel::all();

                $getmax = DB::table('pass_orang_bulan')->max('SERI_PASS_ORG_BLN');
                $lastYear = intval(substr($getmax, 0, 4));
                if ($getmax == null) {
                    $next = "0001";
                } else {
                    if ($curYear > $lastYear) {
                        $next = "0001";
                    } else if ($curYear == $lastYear) {
                        $angka = substr($getmax, 5, 4);
                        $next = str_repeat("0", 4 - strlen($angka + 1)) . ($angka + 1);

                    }

                }
                $id = $curYear . "-" . $next;

                DB::table('pass_orang_bulan')->insert([
                    ['SERI_PASS_ORG_BLN' => $id, 'ID_PENDAFTAR' => $user_data['ID_PENDAFTAR'], 'ID_TRANSAKSI' => $user_data['ID_TRANSAKSI'], 'TGL_MULAI_ORG_BLN' => $user_data['TGL_MULAI'], 'TGL_SELESAI_ORG_BLN' => $user_data['TGL_SELESAI'], 'STATUS_ORG_BLN' => "Belum Lengkap", 'KET_ZONA_BLN' => "", 'CATATAN_ORG_BLN' => '', 'NO_IDENTITAS' => $user_data['NO_IDENTITAS']]
                ]);
            } else if ($user_data['PERIODE'] == "Tahunan") {
                $OrangTahun = PassOrangTahunModel::all();
                $getmax = DB::table('pass_orang_tahun')->max('SERI_PASS_ORG_THN');
                $lastYear = intval(substr($getmax, 0, 4));
                if ($getmax == null) {
                    $next = "0001";
                } else {
                    if ($curYear > $lastYear) {
                        $next = "0001";
                    } else if ($curYear == $lastYear) {
                        $angka = substr($getmax, 5, 4);
                        $next = str_repeat("0", 4 - strlen($angka + 1)) . ($angka + 1);

                    }

                }
                $id = $curYear . "-" . $next;

                DB::table('pass_orang_tahun')->insert([
                    ['SERI_PASS_ORG_THN' => $id, 'ID_PENDAFTAR' => $user_data['ID_PENDAFTAR'], 'ID_TRANSAKSI' => $user_data['ID_TRANSAKSI'], 'TGL_MULAI_ORG_THN' => $user_data['TGL_MULAI'], 'TGL_SELESAI_ORG_THN' => $user_data['TGL_SELESAI'], 'STATUS_ORG_THN' => "Belum Lengkap", 'KET_ZONA_THN' => "", 'CATATAN_ORG_THN' => '', 'NO_IDENTITAS' => $user_data['NO_IDENTITAS']]
                ]);
            }
//            return view('form-front.pass', compact('idtrans'))
//                ->with('idshow', $idtrans);
            Alert::success('Registrasi Pendaftar Berhasil');

            return redirect(route('front.pass'));

        } else if ($pendaftar != null) {
            DB::table('pass_orang_bulan')->where('ID_TRANSAKSI', '=', $idtrans)->delete();
            DB::table('pass_orang_tahun')->where('ID_TRANSAKSI', '=', $idtrans)->delete();
            DB::table('transaksi_pass')->where('ID_TRANSAKSI', '=', $idtrans)->delete();

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
