<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\PassOrangBulanModel;
use App\PassOrangTahunModel;
use App\PendaftarModel;
use App\BlacklistModel;
use DB;
use Alert;

class PassOrangControl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        return view('pass-input');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $user_data = $request->except('_token');
        $MASA_KERJA_join = $user_data['MASA_KERJA_tahun'] . " tahunan " . $user_data['MASA_KERJA_bulan'] . " bulan";
        $pendaftar = DB::table('data_pendaftar')->where('STATUS_PENDAFTAR', '=', 'Blacklist')->where('NAMA_PENDAFTAR', '=', $user_data['NAMA_PENDAFTAR'])->orWhere('NO_IDENTITAS', '=', $user_data['NO_IDENTITAS'])->orWhere('NO_PEGAWAI', '=', $user_data['NO_PEGAWAI'])->first();

        $idtrans = $user_data['ID_TRANSAKSI'];

        if ($pendaftar == null) {

            DB::table('data_pendaftar')->insert(['ID_PENDAFTAR' => $user_data['ID_PENDAFTAR'], 'NAMA_PENDAFTAR' => $user_data['NAMA_PENDAFTAR'], 'NO_IDENTITAS' => $user_data['NO_IDENTITAS'], 'NO_PEGAWAI' => $user_data['NO_PEGAWAI'], 'TEMPAT_LAHIR' => $user_data['TEMPAT_LAHIR'], 'TGL_LAHIR' => $user_data['TGL_LAHIR'],
                'AGAMA' => $user_data['AGAMA'], 'INSTANSI' => $user_data['INSTANSI'], 'ALAMAT_KANTOR' => $user_data['ALAMAT_KANTOR'], 'ALAMAT_RUMAH' => $user_data['ALAMAT_RUMAH'], 'NO_TELP' => $user_data['NO_TELP'], 'NO_TELP_KANTOR' => $user_data['NO_TELP_KANTOR'], 'PEKERJAAN' => $user_data['PEKERJAAN'], 'STATUS_KERJA' => $user_data['STATUS_KERJA'],
                'MASA_KERJA' => $MASA_KERJA_join, 'TEMPAT_KERJA_SBLMNYA' => $user_data['TEMPAT_KERJA_SBLMNYA'], 'STATUS_PENDAFTAR' => 'Terdaftar'
            ]);
            $curYear = intval(date("Y"));
            if ($user_data['periode'] == "Bulanan") {
                $OrangBulan = PassOrangBulanModel::all();

                $getmax = DB::table('pass_orang_bulan')->max('SERI_PASS_ORG_BLN');
                $lastYear = intval(substr($getmax, 0, 4));
                if ($getmax == null) {
                    $next = "0001";
                } else {
                    if ($curYear > $lastYear) {
                        $next = "0001";
                    } else if ($curYear == $lastYear) {
                        $angka = substr($getmax, 5, 4);
                        $next = str_repeat("0", 4 - strlen($angka + 1)) . ($angka + 1);

                    }

                }
                $id = $curYear . "-" . $next;

                DB::table('pass_orang_bulan')->insert([
                    ['SERI_PASS_ORG_BLN' => $id, 'ID_PENDAFTAR' => $user_data['ID_PENDAFTAR'], 'ID_TRANSAKSI' => $user_data['ID_TRANSAKSI'], 'TGL_MULAI_ORG_BLN' => $user_data['TGL_MULAI'], 'TGL_SELESAI_ORG_BLN' => $user_data['TGL_SELESAI'], 'STATUS_ORG_BLN' => "Belum Lengkap", 'KET_ZONA_BLN' => "", 'CATATAN_ORG_BLN' => '']
                ]);
            } else if ($user_data['periode'] == "Tahunan") {
                $OrangTahun = PassOrangTahunModel::all();
                $getmax = DB::table('pass_orang_tahun')->max('SERI_PASS_ORG_THN');
                $lastYear = intval(substr($getmax, 0, 4));
                if ($getmax == null) {
                    $next = "0001";
                } else {
                    if ($curYear > $lastYear) {
                        $next = "0001";
                    } else if ($curYear == $lastYear) {
                        $angka = substr($getmax, 5, 4);
                        $next = str_repeat("0", 4 - strlen($angka + 1)) . ($angka + 1);

                    }

                }
                $id = $curYear . "-" . $next;

                DB::table('pass_orang_tahun')->insert([
                    ['SERI_PASS_ORG_THN' => $id, 'ID_PENDAFTAR' => $user_data['ID_PENDAFTAR'], 'ID_TRANSAKSI' => $user_data['ID_TRANSAKSI'], 'TGL_MULAI_ORG_THN' => $user_data['TGL_MULAI'], 'TGL_SELESAI_ORG_THN' => $user_data['TGL_SELESAI'], 'STATUS_ORG_THN' => "Belum Lengkap", 'KET_ZONA_THN' => "", 'CATATAN_ORG_THN' => '']
                ]);
            }
            return view('pass-input', compact('idtrans'));

        } else if ($pendaftar != null) {
            DB::table('pass_orang_bulan')->where('ID_TRANSAKSI', '=', $idtrans)->delete();
            DB::table('pass_orang_tahun')->where('ID_TRANSAKSI', '=', $idtrans)->delete();
            DB::table('transaksi_pass')->where('ID_TRANSAKSI', '=', $idtrans)->delete();

            // return view('transaksi-create');
            return redirect()->route('transaksi.index2');
        }


        //return redirect()->route('passorang.index',$idtrans);
    }

    public function getPendaf($id, $idtrans)
    {

        $pendaftar = PendaftarModel::where('ID_PENDAFTAR', '=', $id)->first();
        $trans = $idtrans;
        return view('input-perpanjang', compact('pendaftar', 'trans'));
    }

    public function perpanjangPass(Request $request)
    {

        $user_data = $request->except('_token');
        $trans = $user_data['ID_TRANSAKSI'];
        DB::table('data_pendaftar')
            ->where('ID_PENDAFTAR', $user_data['ID_PENDAFTAR'])
            ->update(['NAMA_PENDAFTAR' => $user_data['NAMA_PENDAFTAR'], 'NO_IDENTITAS' => $user_data['NO_IDENTITAS'], 'NO_PEGAWAI' => $user_data['NO_PEGAWAI'], 'TEMPAT_LAHIR' => $user_data['TEMPAT_LAHIR'], 'TGL_LAHIR' => $user_data['TGL_LAHIR'], 'AGAMA' => $user_data['AGAMA'], 'INSTANSI' => $user_data['INSTANSI'], 'ALAMAT_KANTOR' => $user_data['ALAMAT_KANTOR'], 'ALAMAT_RUMAH' => $user_data['ALAMAT_RUMAH'], 'NO_TELP' => $user_data['NO_TELP'], 'NO_TELP_KANTOR' => $user_data['NO_TELP_KANTOR'], 'PEKERJAAN' => $user_data['PEKERJAAN'], 'STATUS_KERJA' => $user_data['STATUS_KERJA'], 'MASA_KERJA' => $user_data['MASA_KERJA'], 'TEMPAT_KERJA_SBLMNYA' => $user_data['TEMPAT_KERJA_SBLMNYA']]);
        $curYear = intval(date("Y"));
        if ($user_data['periode'] == "Bulanan") {
            $OrangBulan = PassOrangBulanModel::all();
            $getmax = DB::table('pass_orang_bulan')->max('SERI_PASS_ORG_BLN');
            $lastYear = intval(substr($getmax, 0, 4));
            if ($getmax == null) {
                $next = "0001";
            } else {
                if ($curYear > $lastYear) {
                    $next = "0001";
                } else if ($curYear == $lastYear) {
                    $angka = substr($getmax, 5, 4);
                    $next = str_repeat("0", 4 - strlen($angka + 1)) . ($angka + 1);

                }

            }
            $id = $curYear . "-" . $next;

            DB::table('pass_orang_bulan')->insert([
                ['SERI_PASS_ORG_BLN' => $id, 'ID_PENDAFTAR' => $user_data['ID_PENDAFTAR'], 'ID_TRANSAKSI' => $user_data['ID_TRANSAKSI'], 'TGL_MULAI_ORG_BLN' => $user_data['TGL_MULAI'], 'TGL_SELESAI_ORG_BLN' => $user_data['TGL_SELESAI'], 'STATUS_ORG_BLN' => "Belum Lengkap", 'KET_ZONA_BLN' => "", 'CATATAN_ORG_BLN' => '']
            ]);
        } else if ($user_data['periode'] == "Tahunan") {
            $OrangTahun = PassOrangTahunModel::all();
            $getmax = DB::table('pass_orang_tahun')->max('SERI_PASS_ORG_THN');
            $lastYear = intval(substr($getmax, 0, 4));
            if ($getmax == null) {
                $next = "0001";
            } else {
                if ($curYear > $lastYear) {
                    $next = "0001";
                } else if ($curYear == $lastYear) {
                    $angka = substr($getmax, 5, 4);
                    $next = str_repeat("0", 4 - strlen($angka + 1)) . ($angka + 1);

                }

            }
            $id = $curYear . "-" . $next;

            DB::table('pass_orang_tahun')->insert([
                ['SERI_PASS_ORG_THN' => $id, 'ID_PENDAFTAR' => $user_data['ID_PENDAFTAR'], 'ID_TRANSAKSI' => $user_data['ID_TRANSAKSI'], 'TGL_MULAI_ORG_THN' => $user_data['TGL_MULAI'], 'TGL_SELESAI_ORG_THN' => $user_data['TGL_SELESAI'], 'STATUS_ORG_THN' => "Belum Lengkap", 'KET_ZONA_THN' => "", 'CATATAN_ORG_THN' => '']
            ]);
        }
        //return redirect()->route('passorang.show',$idtrans);
        return view('pass-perpanjang', compact('trans'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $trans = $id;
        return view('pass-perpanjang', compact('trans'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($seri, $id)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if ($user_data['periode'] == "Bulanan") {
            $OrangBulan = PassOrangBulanModel::all();
            $getmax = DB::table('pass_orang_bulan')->max('SERI_PASS_ORG_BLN');
            if ($getmax == null) {
                $next = "0001";
            } else {
                $angka = substr($getmax, 5, 4);
                $next = str_repeat("0", 4 - strlen($angka + 1)) . ($angka + 1);
            }

            $curYear = date('Y');
            $id = $curYear . "-" . $next;

            DB::table('pass_orang_bulan')->insert([
                ['SERI_PASS_ORG_BLN' => $id, 'ID_PENDAFTAR' => $user_data['ID_PENDAFTAR'], 'ID_TRANSAKSI' => $user_data['ID_TRANSAKSI'], 'TGL_MULAI_ORG_BLN' => $user_data['TGL_MULAI'], 'TGL_SELESAI_ORG_BLN' => $user_data['TGL_SELESAI'], 'STATUS_ORG_BLN' => "Belum Lengkap", 'KET_ZONA_BLN' => ""]
            ]);
        } else if ($user_data['periode'] == "Tahunan") {
            $OrangTahun = PassOrangTahunModel::all();
            $getmax = DB::table('pass_orang_tahun')->max('SERI_PASS_ORG_THN');
            if ($getmax == null) {
                $next = "0001";
            } else {
                $angka = substr($getmax, 5, 4);
                $next = str_repeat("0", 4 - strlen($angka + 1)) . ($angka + 1);
            }
            $curYear = date('Y');
            $id = $curYear . "-" . $next;

            DB::table('pass_orang_tahun')->insert([
                ['SERI_PASS_ORG_THN' => $id, 'ID_PENDAFTAR' => $user_data['ID_PENDAFTAR'], 'ID_TRANSAKSI' => $user_data['ID_TRANSAKSI'], 'TGL_MULAI_ORG_THN' => $user_data['TGL_MULAI'], 'TGL_SELESAI_ORG_THN' => $user_data['TGL_SELESAI'], 'STATUS_ORG_THN' => "Belum Lengkap", 'KET_ZONA_THN' => ""]
            ]);
        }

        return redirect()->route('passorang.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($seri, $id, $periode)
    {
        //
        if ($periode == 'Bulanan') {
            DB::table('pass_orang_bulan')
                ->where('SERI_PASS_ORG_BLN', $seri)
                ->delete();

            DB::table('data_pendaftar')
                ->where('ID_PENDAFTAR', $id)
                ->delete();
        } else if ($periode == 'Tahunan') {
            DB::table('pass_orang_tahun')
                ->where('SERI_PASS_ORG_THN', $seri)
                ->delete();

            DB::table('data_pendaftar')
                ->where('ID_PENDAFTAR', $id)
                ->delete();
        }

        return redirect()->route('passorang.index');
    }
}
