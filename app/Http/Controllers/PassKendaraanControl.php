<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\PassKendBulanModel;
use App\PassKendTahunModel;
use App\KendaraanModel;
use DB;

class PassKendaraanControl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('pass-input');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $user_data = $request->except('_token');
        DB::table('data_kendaraan')->insert(['ID_KENDARAAN' => $user_data['ID_KENDARAAN'], 'NAMA_KENDARAAN' => $user_data['NAMA_KENDARAAN'], 'ID_JENIS_KEND' => $user_data['ID_JENIS_KEND'], 'NO_POLISI' => $user_data['NO_POLISI'], 'WARNA_PLAT' => $user_data['WARNA_PLAT']]);

        $curYear = intval(date("Y"));

        if ($user_data['periode'] == "Bulanan") {
            $getmax = DB::table('pass_kend_bulan')->max('SERI_PASS_KEND_BLN');

            $lastYear = intval(substr($getmax, 0, 4));
            if ($getmax == null) {
                $next = "0001";
            } else {
                if ($curYear > $lastYear) {
                    $next = "0001";
                } else if ($curYear == $lastYear) {
                    $angka = substr($getmax, 5, 4);
                    $next = str_repeat("0", 4 - strlen($angka + 1)) . ($angka + 1);
                }
            }

            $id = $curYear . "-" . $next;


            DB::table('pass_kend_bulan')->insert([
                ['SERI_PASS_KEND_BLN' => $id, 'ID_KENDARAAN' => $user_data['ID_KENDARAAN'], 'ID_TRANSAKSI' => $user_data['ID_TRANSAKSI'], 'TGL_MULAI_KEND_BLN' => $user_data['TGL_MULAI'], 'TGL_SELESAI_KEND_BLN' => $user_data['TGL_SELESAI'], 'STATUS_KEND_BLN' => "Belum Lengkap", 'PEMEGANG_BLN' => $user_data['PEMEGANG']]
            ]);
        } else if ($user_data['periode'] == "Tahunan") {
            $getmax = DB::table('pass_kend_tahun')->max('SERI_PASS_KEND_THN');

            $lastYear = intval(substr($getmax, 0, 4));
            if ($getmax == null) {
                $next = "0001";
            } else {
                if ($curYear > $lastYear) {
                    $next = "0001";
                } else if ($curYear == $lastYear) {
                    $angka = substr($getmax, 5, 4);
                    $next = str_repeat("0", 4 - strlen($angka + 1)) . ($angka + 1);
                }

            }

            $id = $curYear . "-" . $next;

            DB::table('pass_kend_tahun')->insert([
                ['SERI_PASS_KEND_THN' => $id, 'ID_KENDARAAN' => $user_data['ID_KENDARAAN'], 'ID_TRANSAKSI' => $user_data['ID_TRANSAKSI'], 'TGL_MULAI_KEND_THN' => $user_data['TGL_MULAI'], 'TGL_SELESAI_KEND_THN' => $user_data['TGL_SELESAI'], 'STATUS_KEND_THN' => "Belum Lengkap", 'PEMEGANG_THN' => $user_data['PEMEGANG']]
            ]);
        }
        $idtrans = $user_data['ID_TRANSAKSI'];
        return view('pass-input', compact('idtrans'));
        //return redirect()->route('passkendaraan.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($seri, $id, $periode)
    {
        //
        if ($periode == 'Bulanan') {
            DB::table('pass_kend_bulan')
                ->where('SERI_PASS_KEND_BLN', $seri)
                ->delete();

            DB::table('data_kendaraan')
                ->where('ID_KENDARAAN', $id)
                ->delete();
        } else if ($periode == 'Tahunan') {
            DB::table('pass_kend_tahun')
                ->where('SERI_PASS_KEND_THN', $seri)
                ->delete();

            DB::table('data_kendaraan')
                ->where('ID_KENDARAAN', $id)
                ->delete();
        }


        return redirect()->route('passkendaraan.index');
    }

    public function getKend($id, $idtrans)
    {

        $kendaraan = KendaraanModel::where('ID_KENDARAAN', '=', $id)->first();
        $trans = $idtrans;
        return view('input-perpanjang-kend', compact('kendaraan', 'trans'));
    }

    public function perpanjangPassKend(Request $request)
    {
        $user_data = $request->except('_token');
        $trans = $user_data['ID_TRANSAKSI'];
        DB::table('data_kendaraan')
            ->where('ID_KENDARAAN', $user_data['ID_KENDARAAN'])
            ->update(['NAMA_KENDARAAN' => $user_data['NAMA_KENDARAAN'], 'ID_JENIS_KEND' => $user_data['ID_JENIS_KEND'], 'NO_POLISI' => $user_data['NO_POLISI'], 'WARNA_PLAT' => $user_data['WARNA_PLAT']]);

        $curYear = intval(date("Y"));

        if ($user_data['periode'] == "Bulanan") {
            $getmax = DB::table('pass_kend_bulan')->max('SERI_PASS_KEND_BLN');
            $lastYear = intval(substr($getmax, 0, 4));
            if ($getmax == null) {
                $next = "0001";
            } else {
                if ($curYear > $lastYear) {
                    $next = "0001";
                } else if ($curYear == $lastYear) {
                    $angka = substr($getmax, 5, 4);
                    $next = str_repeat("0", 4 - strlen($angka + 1)) . ($angka + 1);

                }

            }
            $id = $curYear . "-" . $next;

            DB::table('pass_kend_bulan')->insert([
                ['SERI_PASS_KEND_BLN' => $id, 'ID_KENDARAAN' => $user_data['ID_KENDARAAN'], 'ID_TRANSAKSI' => $user_data['ID_TRANSAKSI'], 'TGL_MULAI_KEND_BLN' => $user_data['TGL_MULAI'], 'TGL_SELESAI_KEND_BLN' => $user_data['TGL_SELESAI'], 'STATUS_KEND_BLN' => "Belum Lengkap", 'PEMEGANG_BLN' => $user_data['PEMEGANG']]
            ]);
        } else if ($user_data['periode'] == "Tahunan") {
            $getmax = DB::table('pass_kend_tahun')->max('SERI_PASS_KEND_THN');
            $lastYear = intval(substr($getmax, 0, 4));
            if ($getmax == null) {
                $next = "0001";
            } else {
                if ($curYear > $lastYear) {
                    $next = "0001";
                } else if ($curYear == $lastYear) {
                    $angka = substr($getmax, 5, 4);
                    $next = str_repeat("0", 4 - strlen($angka + 1)) . ($angka + 1);

                }

            }
            $id = $curYear . "-" . $next;

            DB::table('pass_kend_tahun')->insert([
                ['SERI_PASS_KEND_THN' => $id, 'ID_KENDARAAN' => $user_data['ID_KENDARAAN'], 'ID_TRANSAKSI' => $user_data['ID_TRANSAKSI'], 'TGL_MULAI_KEND_THN' => $user_data['TGL_MULAI'], 'TGL_SELESAI_KEND_THN' => $user_data['TGL_SELESAI'], 'STATUS_KEND_THN' => "Belum Lengkap", 'PEMEGANG_THN' => $user_data['PEMEGANG']]
            ]);
        }
        return view('pass-perpanjang', compact('trans'));

    }
}
