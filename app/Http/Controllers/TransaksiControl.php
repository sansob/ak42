<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Mail;
use App\Http\Requests;
use App\TransaksiModel;
use App\PassOrangBulanModel;
use App\PassOrangTahunModel;
use App\PassKendBulanModel;
use App\PassKendTahunModel;
use App\PassOrangBulananModel;
use App\PassOrangTahunanModel;
use App\PassKendaraanBulananModel;
use App\PassKendaraanTahunanModel;
use Auth;
Use App;
Use PDF;


use Session;

class TransaksiControl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $transaksi = TransaksiModel::all();
        $transaksiboss = TransaksiModel::where('STATUS_TRANSAKSI', '=', 'Sudah Ajukan Konfirmasi')->get();
        $transaksiplyn = TransaksiModel::where('STATUS_TRANSAKSI', '=', 'Belum Ajukan Konfirmasi')->get();
        $transaksisales = TransaksiModel::where('STATUS_TRANSAKSI', '=', 'Sudah Konfirmasi')->get();
        $transaksiscrt = TransaksiModel::where('STATUS_TRANSAKSI', '=', 'Menuju Screening')->get();
        return view('usulan-transaksi', compact('transaksi', 'transaksiboss', 'transaksiplyn', 'transaksisales', 'transaksiscrt'));
    }

    public function index2()
    {
        //

        return view('transaksi-create');
    }

    public function logtampil()
    {
        //

        return view('log-transaksi');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $user_data = $request->except('_token');

        if ($user_data['STATUS_DAFTAR'] == "Baru") {
            $idtrans = $user_data['ID_TRANSAKSI'];
            $user_data['STATUS_TRANSAKSI'] = 'Belum Ajukan Konfirmasi';
            TransaksiModel::create($user_data);
            //return redirect()->route('passorang.index',$idtrans);
            return view('pass-input', compact('idtrans'));
        } else if ($user_data['STATUS_DAFTAR'] == 'Perpanjang') {
            $trans = $user_data['ID_TRANSAKSI'];
            $user_data['STATUS_TRANSAKSI'] = 'Perpanjang';
            TransaksiModel::create($user_data);
            return view('pass-perpanjang', compact('trans'));
            //return redirect()->route('passorang.show',$trans);
        }
        DB::table('log_transaksi')->insert([
            ['ID_TRANSAKSI' => $user_data['ID_TRANSAKSI'], 'TGL_LOG' => date('Y-m-d h:i:s'), 'STATUS_TERAKHIR' => $user_data['STATUS_TRANSAKSI'], 'PJ_TRANSAKSI' => Auth::user()->NAMA_USER]]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function back1()
    {
        //
        return redirect()->back();
    }

    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function cetak($id)
    {
        //
        //set_paper(array(0, 0, 595, 841), 'portrait');
        // PDF->set_paper(array(0, 0, 595, 841), 'portrait');
        $pdf = PDF::loadView('nota_cetak', compact('id'));

        return $pdf->stream();

        // return view('nota_cetak', compact('id'));
    }

    public function edit($id)
    {
        //
        $seripass = PassOrangBulanModel::where('SERI_PASS_ORG_BLN', '=', $id)->first();
        $periode = 'bulanan';
        return view('transaksi-screening', compact('seripass', 'periode'));
    }

    public function editorangtahun($id)
    {
        //
        $seripass = PassOrangTahunModel::where('SERI_PASS_ORG_THN', '=', $id)->first();
        $periode = 'tahunan';
        return view('transaksi-screening', compact('seripass', 'periode'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function konfirmasi($id)
    {

        $transaksiss = TransaksiModel::where('ID_TRANSAKSI', '=', $id)->first();

        if ($transaksiss->STATUS_TRANSAKSI == "Menuju Screening") {
            DB::table('transaksi_pass')
                ->where('ID_TRANSAKSI', $id)
                ->update(['STATUS_TRANSAKSI' => "Sudah Ajukan Konfirmasi"]);

            $data = array(
                'id' => $transaksiss->ID_TRANSAKSI,
            );

            Mail::send('emails.welcome', $data, function ($message) {
                $message->from('qampus.id@gmail.com', 'Konfirmasi');
                $message->to('warungcoffe11@gmail.com')
                    ->subject('Konfirmasi Pass Bandara');
            });

            DB::table('log_transaksi')->insert([
                ['ID_TRANSAKSI' => $id, 'TGL_LOG' => date('Y-m-d h:i:s'), 'STATUS_TERAKHIR' => 'Sudah Ajukan Konfirmasi', 'PJ_TRANSAKSI' => Auth::user()->NAMA_USER]]);
            return redirect(route('transaksi.index'));
        }
        // else
        // {
        //   DB::table('transaksi_pass')
        //       ->where('ID_TRANSAKSI',$id)
        //       ->update(['STATUS_TRANSAKSI' => "Belum Ajukan Konfirmasi"]);
        //    return redirect()->route('transaksi.index');
        //
        // }

    }


    public function isikan($id)
    {

        $transaksiss = TransaksiModel::where('ID_TRANSAKSI', '=', $id)->first();

        if ($transaksiss->STATUS_TRANSAKSI == "Menuju Screening") {
            DB::table('transaksi_pass')
                ->where('ID_TRANSAKSI', $id)
                ->update(['STATUS_TRANSAKSI' => "Isikan"]);

            $data = array(
                'id' => $transaksiss->ID_TRANSAKSI,
            );

//            Mail::send('emails.welcome', $data, function ($message) {
//                $message->from('fanggipamungkas@gmail.com', 'Konfirmasi');
//                $message->to('warungcoffe11@gmail.com')
//                    ->subject('Konfirmasi Pass Bandara');
//            });

            DB::table('log_transaksi')->insert([
                ['ID_TRANSAKSI' => $id, 'TGL_LOG' => date('Y-m-d h:i:s'), 'STATUS_TERAKHIR' => 'Isikan', 'PJ_TRANSAKSI' => Auth::user()->NAMA_USER]]);
            return redirect()->route('transaksi.index');
        }
        // else
        // {
        //   DB::table('transaksi_pass')
        //       ->where('ID_TRANSAKSI',$id)
        //       ->update(['STATUS_TRANSAKSI' => "Belum Ajukan Konfirmasi"]);
        //    return redirect()->route('transaksi.index');
        //
        // }

    }

    public function log($id)
    {

        $lempar = $id;


        return view('detil-log', compact('lempar'));


    }

    public function menujuscrn($id)
    {

        $transaksiss = TransaksiModel::where('ID_TRANSAKSI', '=', $id)->first();

        if ($transaksiss->STATUS_TRANSAKSI == "Belum Ajukan Konfirmasi") {
            DB::table('transaksi_pass')
                ->where('ID_TRANSAKSI', $id)
                ->update(['STATUS_TRANSAKSI' => "Menuju Screening"]);
            DB::table('log_transaksi')->insert([
                ['ID_TRANSAKSI' => $id, 'TGL_LOG' => date('Y-m-d h:i:s'), 'STATUS_TERAKHIR' => 'Menuju Screening', 'PJ_TRANSAKSI' => Auth::user()->NAMA_USER]]);

            return redirect()->route('transaksi.index');


        }
        //  else
        //  {
        //    DB::table('transaksi_pass')
        //        ->where('ID_TRANSAKSI',$id)
        //        ->update(['STATUS_TRANSAKSI' => "Belum Ajukan Konfirmasi"]);
        //     return redirect()->route('transaksi.index');
        //
        //  }

    }

    public function aktifasi($id)
    {
        DB::table('transaksi_pass')
            ->where('ID_TRANSAKSI', $id)
            ->update(['STATUS_TRANSAKSI' => "Sudah Konfirmasi"]);
        DB::table('log_transaksi')->insert([['ID_TRANSAKSI' => $id, 'TGL_LOG' => date('Y-m-d h:i:s'), 'STATUS_TERAKHIR' => 'Sudah Konfirmasi', 'PJ_TRANSAKSI' => Auth::user()->NAMA_USER]]);
        return redirect()->route('transaksi.index');


    }


    public function detil($id)
    {
        //
        $transaksi = TransaksiModel::where('ID_TRANSAKSI', '=', $id)->first();
        $pass_orang_bulan = PassOrangBulanModel::where('ID_TRANSAKSI', '=', $id)
            ->get();
        //  DB::table('pass_orang_bulan')
        //  ->where('ID_TRANSAKSI', '=', $id)
        //  ->get();
        $pass_orang_tahun = PassOrangTahunModel::where('ID_TRANSAKSI', '=', $id)->get();
        //  DB::table('pass_orang_tahun')
        //  ->where('ID_TRANSAKSI', '=', $id)
        //  ->get();
        $pass_kend_bulan = PassKendBulanModel::where('ID_TRANSAKSI', '=', $id)->get();
        //  DB::table('pass_kend_bulan')
        //  ->where('ID_TRANSAKSI', '=', $id)
        //  ->get();
        $pass_kend_tahun = PassKendTahunModel::where('ID_TRANSAKSI', '=', $id)->get();
        // DB::table('pass_kend_tahun')
        //  ->where('ID_TRANSAKSI', '=', $id)
        //  ->get();

        return view('transaksi-detil', compact('transaksi', 'pass_orang_bulan', 'pass_orang_tahun', 'pass_kend_bulan', 'pass_kend_tahun'));


    }

    public function isikonfirmasiorgbulan($id)
    {

        $passorgbln = PassOrangBulananModel::where('SERI_PASS_ORG_BLN', '=', $id)->first();

        if ($passorgbln->STATUS_ORG_BLN == "Lengkap Belum Screening") {
            DB::table('pass_orang_bulan')
                ->where('SERI_PASS_ORG_BLN', $id)
                ->update(['STATUS_ORG_BLN' => "Isikan"]);
            return redirect()->route('transaksi.detil', $passorgbln->ID_TRANSAKSI);


        } else {
            DB::table('pass_orang_bulan')
                ->where('SERI_PASS_ORG_BLN', $id)
                ->update(['STATUS_ORG_BLN' => "Lengkap Sudah Screening"]);
            return redirect()->route('transaksi.detil', $passorgbln->ID_TRANSAKSI);


        }

    }

    public function isikonfirmasiorgtahun($id)
    {

        $passorgthn = PassOrangTahunanModel::where('SERI_PASS_ORG_THN', '=', $id)->first();

        if ($passorgthn->STATUS_ORG_THN == "Lengkap Belum Screening") {
            DB::table('pass_orang_tahun')
                ->where('SERI_PASS_ORG_THN', $id)
                ->update(['STATUS_ORG_THN' => "Isikan"]);
            return redirect()->route('transaksi.detil', $passorgthn->ID_TRANSAKSI);


        } else {
            DB::table('pass_orang_tahun')
                ->where('SERI_PASS_ORG_THN', $id)
                ->update(['STATUS_ORG_THN' => "Lengkap Sudah Screening"]);
            return redirect()->route('transaksi.detil', $passorgthn->ID_TRANSAKSI);


        }

    }

    public function aktivasideaktivasiorangbulan($id)
    {

        $passorgbln = PassOrangBulananModel::where('SERI_PASS_ORG_BLN', '=', $id)->first();

        if ($passorgbln->STATUS_ORG_BLN == "Belum Lengkap") {
            DB::table('pass_orang_bulan')
                ->where('SERI_PASS_ORG_BLN', $id)
                ->update(['STATUS_ORG_BLN' => "Lengkap Belum Screening"]);
            return redirect()->route('transaksi.detil', $passorgbln->ID_TRANSAKSI);


        } else {
            DB::table('pass_orang_bulan')
                ->where('SERI_PASS_ORG_BLN', $id)
                ->update(['STATUS_ORG_BLN' => "Belum Lengkap"]);
            return redirect()->route('transaksi.detil', $passorgbln->ID_TRANSAKSI);


        }

    }

    public function aktivasideaktivasiorangtahun($id)
    {

        $passorgthn = PassOrangTahunanModel::where('SERI_PASS_ORG_THN', '=', $id)->first();
        if ($passorgthn->STATUS_ORG_THN == "Belum Lengkap") {
            DB::table('pass_orang_tahun')
                ->where('SERI_PASS_ORG_THN', $id)
                ->update(['STATUS_ORG_THN' => "Lengkap"]);
            return redirect()->route('transaksi.detil', $passorgthn->ID_TRANSAKSI);
        } else {
            DB::table('pass_orang_tahun')
                ->where('SERI_PASS_ORG_THN', $id)
                ->update(['STATUS_ORG_THN' => "Belum Lengkap"]);
            return redirect()->route('transaksi.detil', $passorgthn->ID_TRANSAKSI);
        }

    }

    public function aktivasideaktivasikendbulan($id)
    {

        $passkendbln = PassKendBulanModel::where('SERI_PASS_KEND_BLN', '=', $id)->first();
        if ($passkendbln->STATUS_KEND_BLN == "Belum Lengkap") {
            DB::table('pass_kend_bulan')
                ->where('SERI_PASS_KEND_BLN', $id)
                ->update(['STATUS_KEND_BLN' => "Lengkap"]);
            return redirect()->route('transaksi.detil', $passkendbln->ID_TRANSAKSI);
        } else {
            DB::table('pass_kend_bulan')
                ->where('SERI_PASS_KEND_BLN', $id)
                ->update(['STATUS_KEND_BLN' => "Belum Lengkap"]);
            return redirect()->route('transaksi.detil', $passkendbln->ID_TRANSAKSI);
        }

    }

    public function aktivasideaktivasikendtahun($id)
    {

        $passkendthn = PassKendTahunModel::where('SERI_PASS_KEND_THN', '=', $id)->first();
        if ($passkendthn->STATUS_KEND_THN == "Belum Lengkap") {
            DB::table('pass_kend_tahun')
                ->where('SERI_PASS_KEND_THN', $id)
                ->update(['STATUS_KEND_THN' => "Lengkap"]);
            return redirect()->route('transaksi.detil', $passkendthn->ID_TRANSAKSI);
        } else {
            DB::table('pass_kend_tahun')
                ->where('SERI_PASS_KEND_THN', $id)
                ->update(['STATUS_KEND_THN' => "Belum Lengkap"]);
            return redirect()->route('transaksi.detil', $passkendthn->ID_TRANSAKSI);
        }

    }

    public function editnodis($id)
    {
        //
        $transaksi = TransaksiModel::where('ID_TRANSAKSI', '=', $id)->first();
        return view('transaksi-updatenodis', compact('tranksasi'));
    }

    public function updatenodis(Request $request)
    {
        //
        $user_data = $request->except('_token');
        DB::table('transaksi_pass')
            ->where('ID_TRANSAKSI', $user_data['ID_TRANSAKSI'])
            ->update(['NOTA_DINAS' => $user_data['NOTA_DINAS']]);

        if ($user_data['tipenota'] == "bulanan") {
            return redirect()->to("http://localhost/ak42/nota_cetak.php?id=$user_data[ID_TRANSAKSI]");
        } else {
            return redirect()->to("http://localhost/ak42/nota_cek_tahun.php?id=$user_data[ID_TRANSAKSI]");
        }
    }

    public function update(Request $request)
    {
        //
        $user_data = $request->except('_token');
        $id = DB::table('pass_orang_bulan')
            ->where('SERI_PASS_ORG_BLN', $user_data['SERI_PASS_ORG_BLN'])->first();

        $check = implode(",", $user_data['KET_ZONA_BLN']);


        DB::table('pass_orang_bulan')
            ->where('SERI_PASS_ORG_BLN', $user_data['SERI_PASS_ORG_BLN'])
            ->update(['ID_JENIS_PASS' => $user_data['ID_JENIS_PASS'], 'KET_ZONA_BLN' => $check, 'CATATAN_ORG_BLN' => $user_data['CATATAN_ORG_BLN']]);
        return redirect()->route('transaksi.detil', $id->ID_TRANSAKSI);
    }

    public function updateorangtahun(Request $request)
    {
        //
        $user_data = $request->except('_token');
        $id = DB::table('pass_orang_tahun')
            ->where('SERI_PASS_ORG_THN', $user_data['SERI_PASS_ORG_THN'])->first();


        DB::table('pass_orang_tahun')
            ->where('SERI_PASS_ORG_THN', $user_data['SERI_PASS_ORG_THN'])
            ->update(['ID_JENIS_PASS' => $user_data['ID_JENIS_PASS'], 'KET_ZONA_THN' => $user_data['KET_ZONA_THN'], 'CATATAN_ORG_THN' => $user_data['CATATAN_ORG_THN']]);
        return redirect()->route('transaksi.detil', $id->ID_TRANSAKSI);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
