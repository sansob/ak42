<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\PassOrangBulananModel;
use App\TransaksiModel;
use Session;

class PassOrangBulananControl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('kendaraan-create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function aktivasideaktivasiorangbulan($id)
    {

        $passorgbln = PassOrangBulananModel::where('SERI_PASS_ORG_BLN', '=', $id)->first();

        if ($passorgbln->STATUS_ORG_BLN == "0") {
            DB::table('pass_orang_bulan')
                ->where('SERI_PASS_ORG_BLN', $id)
                ->update(['STATUS_ORG_BLN' => "1"]);
            // return redirect()->route('transaksi-detil');

            $transaksi = TransaksiModel::where('ID_TRANSAKSI', '=', Session::get('id'))->first();
            $pass_orang_bulan = DB::table('pass_orang_bulan')
                ->where('ID_TRANSAKSI', '=', Session::get('id'))
                ->get();
            $pass_orang_tahun = DB::table('pass_orang_tahun')
                ->where('ID_TRANSAKSI', '=', Session::get('id'))
                ->get();
            $pass_kend_bulan = DB::table('pass_kend_bulan')
                ->where('ID_TRANSAKSI', '=', Session::get('id'))
                ->get();
            $pass_kend_tahun = DB::table('pass_kend_tahun')
                ->where('ID_TRANSAKSI', '=', Session::get('id'))
                ->get();
            return view('transaksi-detil', compact('transaksi', 'pass_orang_bulan', 'pass_orang_tahun', 'pass_kend_bulan', 'pass_kend_tahun'));
        } else {
            DB::table('pass_orang_bulan')
                ->where('SERI_PASS_ORG_BLN', $id)
                ->update(['STATUS_ORG_BLN' => "0"]);
            //  return redirect()->route('transaksi-detil');

            $transaksi = TransaksiModel::where('ID_TRANSAKSI', '=', Session::get('id'))->first();
            $pass_orang_bulan = DB::table('pass_orang_bulan')
                ->where('ID_TRANSAKSI', '=', Session::get('id'))
                ->get();
            $pass_orang_tahun = DB::table('pass_orang_tahun')
                ->where('ID_TRANSAKSI', '=', Session::get('id'))
                ->get();
            $pass_kend_bulan = DB::table('pass_kend_bulan')
                ->where('ID_TRANSAKSI', '=', Session::get('id'))
                ->get();
            $pass_kend_tahun = DB::table('pass_kend_tahun')
                ->where('ID_TRANSAKSI', '=', Session::get('id'))
                ->get();
            return view('transaksi-detil', compact('transaksi', 'pass_orang_bulan', 'pass_orang_tahun', 'pass_kend_bulan', 'pass_kend_tahun'));
        }

    }

    public function aktivasideaktivasiorangtahun($id)
    {

        $passorgthn = pass_orang_tahun::where('SERI_PASS_ORG_THN', '=', $id)->first();
        if ($passorgthn->SERI_PASS_ORG_THN == "0") {
            DB::table('pass_orang_tahun')
                ->where('SERI_PASS_ORG_THN', $id)
                ->update(['STATUS_ORG_THN' => "1"]);
            return redirect()->route('transaksi-detil');
        } else {
            DB::table('pass_orang_tahun')
                ->where('SERI_PASS_ORG_THN', $id)
                ->update(['STATUS_ORG_THN' => "0"]);
            return redirect()->route('transaksi-detil');
        }

    }

    public function aktivasideaktivasikendbulan($id)
    {

        $passkendbln = pass_kend_bulan::where('SERI_PASS_KEND_BLN', '=', $id)->first();
        if ($passkendbln->SERI_PASS_KEND_BLN == "0") {
            DB::table('pass_kend_bulan')
                ->where('SERI_PASS_KEND_BLN', $id)
                ->update(['STATUS_KEND_BLN' => "1"]);
            return redirect()->route('transaksi-detil');
        } else {
            DB::table('pass_kend_bulan')
                ->where('SERI_PASS_KEND_BLN', $id)
                ->update(['STATUS_KEND_BLN' => "0"]);
            return redirect()->route('transaksi-detil');
        }

    }

    public function aktivasideaktivasikendtahun($id)
    {

        $passkendthn = pass_kend_tahun::where('SERI_PASS_KEND_THN', '=', $id)->first();
        if ($passkendthn->STATUS_KEND_THN == "0") {
            DB::table('pass_kend_tahun')
                ->where('SERI_PASS_KEND_THN', $id)
                ->update(['STATUS_KEND_THN' => "1"]);
            return redirect()->route('transaksi-detil');
        } else {
            DB::table('pass_kend_tahun')
                ->where('SERI_PASS_KEND_THN', $id)
                ->update(['STATUS_KEND_THN' => "0"]);
            return redirect()->route('transaksi-detil');
        }

    }

    public function store(Request $request)
    {
        //
        $user_data = $request->except('_token');
        KendaraanModel::create($user_data);
        return redirect()->route('kendaraan.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $user_data = $request->except('_token');
        $a = $user_data->A;
        $b = $user_data->B;
        $total = $a + ',' + $b;


        DB::table('pass_orang_bulan')
            ->where('SERI_PASS_ORG_BLN', $id)
            ->update(['ID_JENIS_PASS' => $user_data['ID_JENIS_PASS'], 'KET_ZONA_BLN' => $total]);
        return redirect()->route('transaksi-detil');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
