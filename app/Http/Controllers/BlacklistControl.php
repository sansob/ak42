<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BlacklistModel;
use App\PendaftarModel;
use DB;

class BlacklistControl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $blacklist = BlacklistModel::all();
        return view('data-blacklist', compact('blacklist'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    public function pilihBlacklist()
    {
        //
        return view('pilih-pendaftar');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $user_data = $request->except('_token');

        $blacklist = new BlacklistModel;
        $blacklist->ID_PENDAFTAR = $user_data['ID_PENDAFTAR'];
        $blacklist->TGL_BLACKLIST = $user_data['TGL_BLACKLIST'];
        $blacklist->ALASAN = $user_data['ALASAN'];
        $blacklist->FOTO = '';
        $blacklist->save();

        DB::table('data_pendaftar')->where('ID_PENDAFTAR', '=', $user_data['ID_PENDAFTAR'])->update(['STATUS_PENDAFTAR' => 'Blacklist']);
        return redirect()->route('datablacklist.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $pendaftar = PendaftarModel::where('ID_PENDAFTAR', '=', $id)->first();
        return view('input-blacklist', compact('pendaftar'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $peg)
    {
        //
        // $pendaf=DB::table('data_blacklist')->select('ID_PENDAFTAR')->where('ID_BLACKLIST','=',$id)->get();
        $pendaf = DB::table('data_blacklist')->where('ID_BLACKLIST', '=', $id)->get();
        DB::table('data_pendaftar')->where('ID_PENDAFTAR', '=', $peg)->update(['STATUS_PENDAFTAR' => 'Terdaftar']);
        DB::table('data_blacklist')->where('ID_BLACKLIST', '=', $id)->delete();
        return redirect()->route('datablacklist.index');
    }
}
