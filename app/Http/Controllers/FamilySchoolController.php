<?php

namespace App\Http\Controllers;

use App\KeluargaModel;
use App\PassOrangBulanModel;
use App\PassOrangTahunModel;
use App\PendidikanModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FamilySchoolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('form-front.familyschool');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


        $req = \request('id');
        $getpendaftar = PassOrangBulanModel::where('STATUS_ORG_BLN', 'Isikan')
            ->where('NO_IDENTITAS', session('trans_id'))
//            ['ID_TRANSAKSI', '==', session('trans_id')]

            ->first();

        if ($getpendaftar == null) {
            $getpendaftar = PassOrangTahunModel::where('STATUS_ORG_THN', 'Isikan')
                ->where('ID_TRANSAKSI', session('trans_id'))
                ->first();
        }

        if ($getpendaftar != null) {
            $getkel = KeluargaModel::where('ID_PENDAFTAR', $getpendaftar['ID_PENDAFTAR'])->first();
            $getpend = PendidikanModel::where('ID_PENDAFTAR', $getpendaftar['ID_PENDAFTAR'])->first();

            if ($getpend == null && $getkel == null) {
                $getmaxkel = DB::table('data_keluarga')->max('ID_DATA_KELUARGA');
                if ($getmaxkel == null) {
                    $nextkel = "0001";
                } else {
                    $angka = substr($getmaxkel, 3, 4);
                    $nextkel = str_repeat("0", 4 - strlen($angka + 1)) . ($angka + 1);
                }
                $id = "KG-" . $nextkel;

                $keluarga = new KeluargaModel;
                $keluarga->ID_DATA_KELUARGA = $id;
                $keluarga->ID_PENDAFTAR = $getpendaftar['ID_PENDAFTAR'];
                $keluarga->NAMA_BAPAK = "";
                $keluarga->NAMA_IBU = "";
                $keluarga->ALAMAT_ORTU = "";
                $keluarga->NAMA_PASANGAN = "";
                $keluarga->ALAMAT_PASANGAN = "";
                $keluarga->NO_TELP_PSG = "";
                $keluarga->save();

                $getmaxpn = DB::table('data_pendidikan')->max('ID_DATA_PEND');
                if ($getmaxpn == null) {
                    $nextpn = "0001";
                } else {
                    $angka = substr($getmaxpn, 3, 4);
                    $nextpn = str_repeat("0", 4 - strlen($angka + 1)) . ($angka + 1);
                }
                $idpn = "PN-" . $nextpn;

                $pendidikan = new PendidikanModel;
                $pendidikan->ID_DATA_PEND = $idpn;
                $pendidikan->ID_PENDAFTAR = $getpendaftar['ID_PENDAFTAR'];
                $pendidikan->SD = "";
                $pendidikan->LOKASI_SD = "";
                $pendidikan->THN_LULUS_SD = "";
                $pendidikan->SMP = "";
                $pendidikan->LOKASI_SMP = "";
                $pendidikan->THN_LULUS_SMP = "";
                $pendidikan->SMA = "";
                $pendidikan->LOKASI_SMA = "";
                $pendidikan->THN_LULUS_SMA = "";
                $pendidikan->PEND_TINGGI = "";
                $pendidikan->LOKASI_PEND_TINGGI = "";
                $pendidikan->THN_LULUS_PEND_TINGGI = "";

                $pendidikan->save();
            } else {
                $keluarga = $getkel;
                $pendidikan = $getpend;
            }
            return view('form-front.familyschool', compact('keluarga', 'pendidikan'));
        } else {
            alert()
                ->error(session('trans_id').'    Anda belum diizinkan untuk mengisi form Screening. Hubungi Security untuk info lebih lanjut', 'Error')
                ->persistent("Ok");;
            return view('front.dashboard');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $req = \request('id');
        $keluarga = KeluargaModel::where('ID_DATA_KELUARGA', $request->ID_DATA_KELUARGA)->first();
        $keluarga->NAMA_BAPAK = $request->NAMA_BAPAK;
        $keluarga->NAMA_IBU = $request->NAMA_IBU;
        $keluarga->ALAMAT_ORTU = $request->ALAMAT_ORTU;
        $keluarga->NAMA_PASANGAN = $request->NAMA_PASANGAN;
        $keluarga->ALAMAT_PASANGAN = $request->ALAMAT_PASANGAN;
        $keluarga->NO_TELP_PSG = $request->NO_TELP_PSG;
        $keluarga->save();

        $pendidikan = PendidikanModel::where('ID_DATA_PEND', $request->ID_DATA_PEND)->first();
        $pendidikan->SD = $request->SD;
        $pendidikan->LOKASI_SD = $request->LOKASI_SD;
        $pendidikan->THN_LULUS_SD = $request->THN_LULUS_SD;
        $pendidikan->SMP = $request->SMP;
        $pendidikan->LOKASI_SMP = $request->LOKASI_SMP;
        $pendidikan->THN_LULUS_SMP = $request->THN_LULUS_SMP;
        $pendidikan->SMA = $request->SMA;
        $pendidikan->LOKASI_SMA = $request->LOKASI_SMA;
        $pendidikan->THN_LULUS_SMA = $request->THN_LULUS_SMA;
        $pendidikan->PEND_TINGGI = $request->PEND_TINGGI;
        $pendidikan->LOKASI_PEND_TINGGI = $request->LOKASI_PEND_TINGGI;
        $pendidikan->THN_LULUS_PEND_TINGGI = $request->THN_LULUS_PEND_TINGGI;

        $pendidikan->save();
        alert()
            ->error("Screening Berhasil. Silakan menuju proses selanjutnya")
            ->persistent("Ok");;
        return redirect()->route('front.dashboard');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
