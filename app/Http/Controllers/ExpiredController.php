<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PassKendBulanModel;
use App\PassKendTahunModel;
use App\PassOrangBulanModel;
use App\PassOrangTahunModel;
use App\PendaftarModel;
use App\KendaraanModel;
use DB;

class ExpiredController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $now = date('Y-m-d');
        $end = strtotime($now);
        $end = date('Y-m-d', strtotime("+7 day", $end));

        DB::table('pass_orang_bulan')->where('TGL_SELESAI_ORG_BLN', '=', $now)->update(['STATUS_ORG_BLN' => 'Tidak Berlaku']);
        DB::table('pass_orang_tahun')->where('TGL_SELESAI_ORG_THN', '=', $now)->update(['STATUS_ORG_THN' => 'Tidak Berlaku']);
        DB::table('pass_kend_bulan')->where('TGL_SELESAI_KEND_BLN', '=', $now)->update(['STATUS_KEND_BLN' => 'Tidak Berlaku']);
        DB::table('pass_kend_tahun')->where('TGL_SELESAI_KEND_THN', '=', $now)->update(['STATUS_KEND_THN' => 'Tidak Berlaku']);

        $POBExp = PassOrangBulanModel::whereBetween('TGL_SELESAI_ORG_BLN', [$now, $end])->get();
        $POTExp = PassOrangTahunModel::whereBetween('TGL_SELESAI_ORG_THN', [$now, $end])->get();
        $PKBExp = PassKendBulanModel::whereBetween('TGL_SELESAI_KEND_BLN', [$now, $end])->get();
        $PKTExp = PassKendTahunModel::whereBetween('TGL_SELESAI_KEND_THN', [$now, $end])->get();


        return view('pass-expired', compact('POBExp', 'POTExp', 'PKBExp', 'PKTExp'));
    }


    public function dashboard($id){
        $shows = TransaksiControl::where('ID_TRANSAKSI', $id)->first();
        return view('front.dashboard')->with('editx', $shows);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
