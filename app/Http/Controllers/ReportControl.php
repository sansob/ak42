<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PassKendBulanModel;
use App\PassKendTahunModel;
use App\PassOrangBulanModel;
use App\PassOrangTahunModel;
use App\TransaksiModel;
use DB;
use Excel;
use File;

class ReportControl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('report-bulanan');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //
        $thn = $request['TAHUN'];
        $bln = $request['BULAN'];

        $gettrans = TransaksiModel::whereMonth('TGL_TRANSAKSI', '=', $request['BULAN'])->whereYear('TGL_TRANSAKSI', '=', $request['TAHUN'])->get();
        $getpob = PassOrangBulanModel::whereMonth('TGL_MULAI_ORG_BLN', '=', $request['BULAN'])->whereYear('TGL_MULAI_ORG_BLN', '=', $request['TAHUN'])->get();
        $getpot = PassOrangTahunModel::whereMonth('TGL_MULAI_ORG_THN', '=', $request['BULAN'])->whereYear('TGL_MULAI_ORG_THN', '=', $request['TAHUN'])->get();
        $getpkb = PassKendBulanModel::whereMonth('TGL_MULAI_KEND_BLN', '=', $request['BULAN'])->whereYear('TGL_MULAI_KEND_BLN', '=', $request['TAHUN'])->get();
        $getpkt = PassKendTahunModel::whereMonth('TGL_MULAI_KEND_THN', '=', $request['BULAN'])->whereYear('TGL_MULAI_KEND_THN', '=', $request['TAHUN'])->get();


        return view('report-show', compact('gettrans', 'getpob', 'getpot', 'getpkb', 'getpkt', 'bln', 'thn'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function cetakexcel(Request $request)
    {

        $bulan = $request['bulan'];
        $tahun = $request['tahunan'];

        $getpob = DB::table('pass_orang_bulan')->join('data_pendaftar', 'pass_orang_bulan.ID_PENDAFTAR', '=', 'data_pendaftar.ID_PENDAFTAR')
            ->join('jenis_pass', 'pass_orang_bulan.ID_JENIS_PASS', '=', 'jenis_pass.ID_JENIS_PASS')
            ->select('pass_orang_bulan.SERI_PASS_ORG_BLN', 'data_pendaftar.NAMA_PENDAFTAR', 'data_pendaftar.PEKERJAAN', 'data_pendaftar.INSTANSI', 'data_pendaftar.ALAMAT_KANTOR', 'data_pendaftar.NO_TELP_KANTOR', 'jenis_pass.NAMA_JENIS_PASS', 'pass_orang_bulan.KET_ZONA_BLN', 'pass_orang_bulan.TGL_MULAI_ORG_BLN', 'pass_orang_bulan.TGL_SELESAI_ORG_BLN')
            ->whereMonth('TGL_MULAI_ORG_BLN', '=', $bulan)->whereYear('TGL_MULAI_ORG_BLN', '=', $tahun)
            ->get();
        $getpot = DB::table('pass_orang_tahun')->join('data_pendaftar', 'pass_orang_tahun.ID_PENDAFTAR', '=', 'data_pendaftar.ID_PENDAFTAR')
            ->join('jenis_pass', 'pass_orang_tahun.ID_JENIS_PASS', '=', 'jenis_pass.ID_JENIS_PASS')
            ->select('pass_orang_tahun.SERI_PASS_ORG_THN', 'data_pendaftar.NAMA_PENDAFTAR', 'data_pendaftar.PEKERJAAN', 'data_pendaftar.INSTANSI', 'data_pendaftar.ALAMAT_KANTOR', 'data_pendaftar.NO_TELP_KANTOR', 'jenis_pass.NAMA_JENIS_PASS', 'pass_orang_tahun.KET_ZONA_THN', 'pass_orang_tahun.TGL_MULAI_ORG_THN', 'pass_orang_tahun.TGL_SELESAI_ORG_THN')
            ->whereMonth('TGL_MULAI_ORG_THN', '=', $bulan)->whereYear('TGL_MULAI_ORG_THN', '=', $tahun)
            ->get();
        $getpkb = DB::table('pass_kend_bulan')->join('data_kendaraan', 'pass_kend_bulan.ID_KENDARAAN', '=', 'data_kendaraan.ID_KENDARAAN')
            ->join('jenis_kendaraan', 'data_kendaraan.ID_JENIS_KEND', '=', 'jenis_kendaraan.ID_JENIS_KEND')
            ->select('pass_kend_bulan.SERI_PASS_KEND_BLN', 'data_kendaraan.NAMA_KENDARAAN', 'jenis_kendaraan.NAMA_JENIS', 'data_kendaraan.NO_POLISI', 'data_kendaraan.WARNA_PLAT', 'pass_kend_bulan.TGL_MULAI_KEND_BLN', 'pass_kend_bulan.TGL_SELESAI_KEND_BLN')
            ->whereMonth('TGL_MULAI_KEND_BLN', '=', $bulan)->whereYear('TGL_MULAI_KEND_BLN', '=', $tahun)
            ->get();
        $getpkt = DB::table('pass_kend_tahun')->join('data_kendaraan', 'pass_kend_tahun.ID_KENDARAAN', '=', 'data_kendaraan.ID_KENDARAAN')
            ->join('jenis_kendaraan', 'data_kendaraan.ID_JENIS_KEND', '=', 'jenis_kendaraan.ID_JENIS_KEND')
            ->select('pass_kend_tahun.SERI_PASS_KEND_THN', 'data_kendaraan.NAMA_KENDARAAN', 'jenis_kendaraan.NAMA_JENIS', 'data_kendaraan.NO_POLISI', 'data_kendaraan.WARNA_PLAT', 'pass_kend_tahun.TGL_MULAI_KEND_THN', 'pass_kend_tahun.TGL_SELESAI_KEND_THN')
            ->whereMonth('TGL_MULAI_KEND_THN', '=', $bulan)->whereYear('TGL_MULAI_KEND_THN', '=', $tahun)
            ->get();


        // Initialize the array which will be passed into the Excel
        // generator.
        $pobArray = [];
        $potArray = [];
        $pkbArray = [];
        $pktArray = [];

        // Define the Excel spreadsheet headers
        $pobArray[] = ['ID Pass', 'Nama', 'Pekerjaan', 'Instansi', 'Alamat Kantor', 'No Telp Kantor', 'Jenis Pass', 'Keterangan Zona', 'Tanggal Mulai', 'Tanggal Selesai'];
        $potArray[] = ['ID Pass', 'Nama', 'Pekerjaan', 'Instansi', 'Alamat Kantor', 'No Telp Kantor', 'Jenis Pass', 'Keterangan Zona', 'Tanggal Mulai', 'Tanggal Selesai'];
        $pkbArray[] = ['ID Pass', 'Nama Kendaraan', 'Jenis Kendaraan', 'Nomor Polisi', 'Warna Plat', 'Tanggal Mulai', 'Tanggal Selesai'];
        $pktArray[] = ['ID Pass', 'Nama Kendaraan', 'Jenis Kendaraan', 'Nomor Polisi', 'Warna Plat', 'Tanggal Mulai', 'Tanggal Selesai'];

        // Convert each member of the returned collection into an array,
        // and append it to the payments array.
        foreach ($getpob as $pob) {
            // $pobArray[] = $pob->toArray();
            $pobArray[] = (array)$pob;
        }
        foreach ($getpot as $pot) {
            // $potArray[] = $pot->toArray();
            $potArray[] = (array)$pot;
        }
        foreach ($getpkb as $pkb) {
            // $pkbArray[] = $pkb->toArray();
            $pkbArray[] = (array)$pkb;
        }
        foreach ($getpkt as $pkt) {
            // $pktArray[] = $pkt->toArray();
            $pktArray[] = (array)$pkt;
        }
        // Generate and return the spreadsheet

        Excel::create('Laporan Pass bulan ' . $bulan . ' tahunan ' . $tahun, function ($excel) use ($pobArray, $potArray, $pkbArray, $pktArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Laporan Pass bulan $bulan tahunan $tahunan');
            $excel->setCreator('Administrasi')->setCompany('PT.Angkasa Pura I');
            $excel->setDescription('Laporan Pass');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('Pass Orang Bulanan', function ($sheet) use ($pobArray) {
                $sheet->fromArray($pobArray, null, 'A1', false, false);
            });
            $excel->sheet('Pass Orang Tahunan', function ($sheet) use ($potArray) {
                $sheet->fromArray($potArray, null, 'A1', false, false);
            });
            $excel->sheet('Pass Kendaraan Bulanan', function ($sheet) use ($pkbArray) {
                $sheet->fromArray($pkbArray, null, 'A1', false, false);
            });
            $excel->sheet('Pass Kendaraan Tahunan', function ($sheet) use ($pktArray) {
                $sheet->fromArray($pktArray, null, 'A1', false, false);
            });

        })->download('xlsx');

    }
}
