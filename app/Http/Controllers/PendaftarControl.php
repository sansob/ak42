<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\PendaftarModel;

class PendaftarControl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('data-pendaftar');
    }

    public function detil($id)
    {
        //
        $pendaftar = PendaftarModel::where('ID_PENDAFTAR', '=', $id)->first();
        $pendaftardetil = DB::table('data_pendaftar')
            ->where('ID_PENDAFTAR', '=', $id)
            ->get();


        return view('pendaftar-profil', compact('pendaftar', 'pendaftardetil'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $user_data = $request->except('_token');
        PendaftarModel::create($user_data);
        return redirect()->route('pendaftar.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $pendaftar = PendaftarModel::where('ID_PENDAFTAR', '=', $id)->first();
        return view('pendaftar-update', compact('pendaftar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $user_data = $request->except('_token');
        DB::table('data_pendaftar')
            ->where('ID_PENDAFTAR', $user_data['ID_PENDAFTAR'])
            ->update(['NAMA_PENDAFTAR' => $user_data['NAMA_PENDAFTAR'], 'NO_IDENTITAS' => $user_data['NO_IDENTITAS'], 'NO_PEGAWAI' => $user_data['NO_PEGAWAI'], 'TEMPAT_LAHIR' => $user_data['TEMPAT_LAHIR'], 'TGL_LAHIR' => $user_data['TGL_LAHIR'], 'AGAMA' => $user_data['AGAMA'], 'INSTANSI' => $user_data['INSTANSI'], 'ALAMAT_KANTOR' => $user_data['ALAMAT_KANTOR'], 'ALAMAT_RUMAH' => $user_data['ALAMAT_RUMAH'], 'NO_TELP' => $user_data['NO_TELP'], 'NO_TELP_KANTOR' => $user_data['NO_TELP_KANTOR'], 'PEKERJAAN' => $user_data['PEKERJAAN'], 'STATUS_KERJA' => $user_data['STATUS_KERJA'], 'MASA_KERJA' => $user_data['MASA_KERJA'], 'TEMPAT_KERJA_SBLMNYA' => $user_data['TEMPAT_KERJA_SBLMNYA']]);
        return redirect()->route('pendaftar.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        DB::table('data_pendaftar')
            ->where('ID_PENDAFTAR', $id)
            ->delete();
        return redirect()->route('pendaftar.index');

    }
}
