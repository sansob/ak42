<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use DB;
use File;
use Illuminate\Support\Facades\Hash;
use Mail;
use App\Http\Requests;
use App\Role;
use App\UnitKerja;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        // $user = User::all();
        $id = Auth::user()->id_user;
        $user = User::where('id_user', '<>', $id)->get();
        return view('user-index', compact('user'));

    }

    protected $username = 'username';

    public function username()
    {
        return 'username';
    }
    //

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function login()
    {

        $pesan = '';
        return view('login', compact('pesan'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_data = $request->except('_token');

        User::create($user_data);
        return redirect()->route('User.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user = User::where('id_user', '=', $id)->first();
        return view('user-update', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $user_data = $request->except('_token');
        if ($user_data['PASSWORD'] != null or $user_data['PASSWORD'] != "") {
            $password = Hash::make($user_data['PASSWORD']);
            DB::table('user')
                ->where('id_user', $user_data['id_user'])
                ->update(['NAMA_USER' => $user_data['NAMA_USER'], 'password' => $password, 'id_role' => $user_data['id_role'], 'id_unit' => $user_data['id_unit']]);
        } else {
            DB::table('user')
                ->where('id_user', $user_data['id_user'])
                ->update(['NAMA_USER' => $user_data['NAMA_USER'], 'id_role' => $user_data['id_role'], 'id_unit' => $user_data['id_unit']]);
        }

        return redirect()->route('User.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        DB::table('user')
            ->where('id_user', $id)
            ->delete();
        return redirect()->route('User.index');

    }

    public function doLogin(Request $request)
    {

        $userdata = $request;
        $username = $userdata->username;
        $password = $userdata->password;

        // attempt to do the login
        if (Auth::attempt(['username' => $username, 'password' => $password], true)) {
            if (Auth::user()->id_role == '1' && Auth::user()->id_unit == '1')
                return redirect()->route('User.index');
            else
                return redirect()->route('transaksi.index');
            // validation successful!
            // redirect them to the secure section or whatever
            // return Redirect::to('secure');
            // for now we'll just echo success (even though echoing in a controller is bad)


        } else {

            // validation not successful, send back to form
            $pesan = 'id atau password salah';

            return view('login', compact('pesan', 'username', 'password'));

        }


    }

    public function doLogout()
    {
        Auth::logout();

        return redirect('/login');
    }

}
