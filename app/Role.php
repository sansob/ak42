<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //
	public $timestamps = false;
    protected $table = 'role_user';
		  protected $primaryKey = 'id_role';
     protected $fillable = [
        'id_role','NAMA_ROLE'
    ];


}
