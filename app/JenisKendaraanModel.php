<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisKendaraanModel extends Model
{
    //
    public $timestamps = false;
    protected $table = 'jenis_kendaraan';
        protected $primaryKey = 'ID_JENIS_KEND';
     protected $fillable = [
        'ID_JENIS_KEND','NAMA_JENIS'];
}
