<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $table = 'documents';
    protected $primaryKey='id';
    protected $fillable =['ID_PENDAFTAR','DOCUMENT_NAME','DOCUMENT_TYPE','ID_TRANSAKSI'];
}
