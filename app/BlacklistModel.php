<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlacklistModel extends Model
{
    //
    public $timestamps = false;
    protected $table = 'data_blacklist';
    protected $primaryKey='ID_BLACKLIST';

    protected $fillable = ['ID_PENDAFTAR','TGL_BLACKLIST','ALASAN','FOTO'];

    public function BlacklistPendaftar()
    {
        return $this->belongsTo('App\PendaftarModel','ID_PENDAFTAR');
    }
}
