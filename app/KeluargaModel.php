<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KeluargaModel extends Model
{
    //
    public $timestamps = false;
    protected $table = 'data_keluarga';
    protected $primaryKey='ID_DATA_KELUARGA';
    public $incrementing = false;
    protected $fillable = ['ID_DATA_KELUARGA','ID_PENDAFTAR','NAMA_BAPAK','NAMA_IBU','ALAMAT_ORTU','NAMA_PASANGAN','ALAMAT_PASANGAN','NO_TELP_PSG'];
}
