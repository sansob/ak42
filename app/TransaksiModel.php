<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransaksiModel extends Model
{
    //
    public $timestamps = false;
    protected $table = 'transaksi_pass';
    protected $fillable = [
        'ID_TRANSAKSI','NAMA_PEMOHON','NO_SURAT_PEMOHON','TGL_TRANSAKSI','STATUS_TRANSAKSI','NOTA_DINAS'];
}
