<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PassKendTahunModel extends Model
{
    //
    public $timestamps = false;
    protected $table = 'pass_kend_tahun';
    protected $primaryKey='SERI_PASS_KEND_THN';
    public $incrementing = false;

    protected $fillable = ['SERI_PASS_KEND_THN','ID_KENDARAAN','PEMEGANG_THN','ID_TRANSAKSI','TGL_MULAI_KEND_THN','TGL_SELESAI_KEND_THN','STATUS_KEND_THN'];
    
    public function KendaraanPassThn()
    {
        return $this->belongsTo('App\KendaraanModel','ID_KENDARAAN');
    }
}
