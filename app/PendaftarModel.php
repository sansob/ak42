<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PendaftarModel extends Model
{
    //
    public $timestamps = false;
    protected $table = 'data_pendaftar';
    protected $primaryKey = 'ID_PENDAFTAR';
    public $incrementing = false;
    protected $fillable = [
        'ID_PENDAFTAR','NAMA_PENDAFTAR','NO_IDENTITAS','NO_PEGAWAI','TEMPAT_LAHIR','TGL_LAHIR','AGAMA','INSTANSI','ALAMAT_KANTOR','ALAMAT_RUMAH','NO_TELP','NO_TELP_KANTOR','PEKERJAAN','STATUS_KERJA','MASA_KERJA','TEMPAT_KERJA_SBLMNYA','JABATAN'];
}
