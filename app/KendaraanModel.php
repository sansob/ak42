<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KendaraanModel extends Model
{
    //
    public $timestamps = false;
    protected $table = 'data_kendaraan';
    protected $primaryKey = 'ID_KENDARAAN';
    public $incrementing = false;
    protected $fillable = ['ID_KENDARAAN', 'ID_JENIS_KEND', 'NAMA_KENDARAAN', 'NO_POLISI', 'WARNA_PLAT'];

    public function JenisKend()
    {
        return $this->belongsTo('App\JenisKendaraanModel', 'ID_JENIS_KEND');
    }
}
