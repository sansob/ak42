<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PassKendaraanTahunanModel extends Model
{
    //
    public $timestamps = false;
    protected $table = 'pass_kend_tahunan';
    protected $fillable = ['SERI_PASS_KEND_THN', 'ID_TRANSAKSI', 'ID_KENDARAAN', 'PEMEGANG_THN', 'TGL_MULAI_KEND_THN', 'TGL_SELESAI_KEND_THN', 'STATUS_KEND_THN'];

    protected $primaryKey = 'SERI_PASS_KEND_THN';

    public function TransaksiIDTHN()
    {
        return $this->belongsTo('App\TransaksiModel', 'ID_TRANSAKSI');
    }

    public function KendaraanIDTHN()
    {
        return $this->belongsTo('App\KendaraanModel', 'ID_KENDARAAN');
    }
}
