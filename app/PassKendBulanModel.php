<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PassKendBulanModel extends Model
{
    //
     public $timestamps = false;
    protected $table = 'pass_kend_bulan';
    protected $primaryKey='SERI_PASS_KEND_BLN';
    public $incrementing = false;
    protected $fillable = ['SERI_PASS_KEND_BLN','ID_KENDARAAN','PEMEGANG_BLN','ID_TRANSAKSI','TGL_MULAI_KEND_BLN','TGL_SELESAI_KEND_BLN','STATUS_KEND_BLN'];
    
    public function KendaraanPassBln()
    {
        return $this->belongsTo('App\KendaraanModel','ID_KENDARAAN');
    }
}
