<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PassKendaraanBulananModel extends Model
{
    //
    public $timestamps = false;
    protected $table = 'pass_kend_bulan';
    protected $fillable = ['SERI_PASS_KEND_BLN','ID_TRANSAKSI','ID_KENDARAAN','PEMEGANG_BLN','TGL_MULAI_KEND_BLN','TGL_SELESAI_KEND_BLN','STATUS_KEND_BLN'];

    protected $primaryKey = 'SERI_PASS_KEND_BLN';
    public function TransaksiIDBLN()
        {
            return $this->belongsTo('App\TransaksiModel','ID_TRANSAKSI');
        }
    public function KendaraanIDBLN()
    		{
    		    return $this->belongsTo('App\KendaraanModel','ID_KENDARAAN');
    		}

}
