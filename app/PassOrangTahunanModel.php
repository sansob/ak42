<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PassOrangTahunanModel extends Model
{
    //
    public $timestamps = false;
    protected $table = 'pass_orang_tahun';
    protected $fillable = ['SERI_PASS_ORG_THN', 'ID_PENDAFTAR', 'ID_JENIS_PASS', 'ID_TRANSAKSI', 'TGL_MULAI_ORG_THN', 'TGL_SELESAI_ORG_THN', 'STATUS_ORG_THN', 'KET_ZONA_THN'];

    protected $primaryKey = 'SERI_PASS_ORG_THN';

    public function TransaksiIDTHN1()
    {
        return $this->belongsTo('App\TransaksiModel', 'ID_TRANSAKSI');
    }

    public function PendaftarIDTHN1()
    {
        return $this->belongsTo('App\PendaftarModel', 'ID_PENDAFTAR');
    }

}
