<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnitKerja extends Model
{
    //
    public $timestamps = false;
    protected $primaryKey = 'id_unit';
    protected $table = 'unit_kerja';
    protected $fillable = [
        'id_unit', 'NAMA_UNIT'
    ];


}
