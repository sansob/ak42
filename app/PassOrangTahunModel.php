<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PassOrangTahunModel extends Model
{
    //
    public $timestamps = false;
    protected $table = 'pass_orang_tahun';

    protected $fillable = ['SERI_PASS_ORG_THN','ID_PENDAFTAR','ID_JENIS_PASS','ID_TRANSAKSI','TGL_MULAI_ORG_THN','TGL_SELESAI_ORG_THN','STATUS_ORG_THN','KET_ZONA_THN'];
    
    public function JenisPassThn()
    {
        return $this->belongsTo('App\JenisPassModel','ID_JENIS_PASS');
    }
    public function PendaftarPassThn()
    {
        return $this->belongsTo('App\PendaftarModel','ID_PENDAFTAR');
    }
}
