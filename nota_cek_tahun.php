<style>
    body {
        font-family: Calibri;
    }

    tr.border_bottom td {
        border-bottom: 1pt solid black;
    }

    .style1 {
        font-size: 14px
    }

    .style2 {
        font-size: 13px
    }
</style>
<?php

function indonesian_date($timestamp = '', $date_format = ' j F Y', $suffix = '')
{
    if (trim($timestamp) == '') {
        $timestamp = time();
    } elseif (!ctype_digit($timestamp)) {
        $timestamp = strtotime($timestamp);
    }
    # remove S (st,nd,rd,th) there are no such things in indonesia :p
    $date_format = preg_replace("/S/", "", $date_format);
    $pattern = array(
        '/Mon[^day]/', '/Tue[^sday]/', '/Wed[^nesday]/', '/Thu[^rsday]/',
        '/Fri[^day]/', '/Sat[^urday]/', '/Sun[^day]/', '/Monday/', '/Tuesday/',
        '/Wednesday/', '/Thursday/', '/Friday/', '/Saturday/', '/Sunday/',
        '/Jan[^uary]/', '/Feb[^ruary]/', '/Mar[^ch]/', '/Apr[^il]/', '/May/',
        '/Jun[^e]/', '/Jul[^y]/', '/Aug[^ust]/', '/Sep[^tember]/', '/Oct[^ober]/',
        '/Nov[^ember]/', '/Dec[^ember]/', '/January/', '/February/', '/March/',
        '/April/', '/June/', '/July/', '/August/', '/September/', '/October/',
        '/November/', '/December/',
    );
    $replace = array('Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab', 'Min',
        'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu',
        'Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des',
        'Januari', 'Februari', 'Maret', 'April', 'Juni', 'Juli', 'Agustus', 'Sepember',
        'Oktober', 'November', 'Desember',
    );
    $date = date($date_format, $timestamp);
    $date = preg_replace($pattern, $replace, $date);
    $date = "{$date} {$suffix}";
    return $date;
}


date_default_timezone_set('Asia/Jakarta');
$id = $_GET['id'];
$mysqli = mysqli_connect('localhost', 'root', '', 'ak41');
$sql1 = "SELECT * FROM transaksi_pass,data_pendaftar, pass_orang_tahun WHERE data_pendaftar.ID_PENDAFTAR = pass_orang_tahun.ID_PENDAFTAR AND transaksi_pass.ID_TRANSAKSI  = pass_orang_tahun.ID_TRANSAKSI  AND transaksi_pass.ID_TRANSAKSI = '$id'";
$result = $mysqli->query($sql1);
$data = $result->fetch_array();
$id_transaksi = $data['ID_TRANSAKSI'];
$nama_pemohon = $data['INSTANSI'];
$no_surat_pemohon = $data['NO_SURAT_PEMOHON'];
?>
<body>
<br><br><br><br><br>
<CENTER>
    <table width="250" border="0">
        <tr class="border_bottom" valign="bottom">
            <td width="186" valign="bottom">
                <div align="center"><b><font size="+2">N O T A - D I N A S </font></b></div>
            </td>
        </tr>
        <tr>
            <td></td>
        </tr>
    </table>
    NOMOR: <?php echo $data['NOTA_DINAS']; ?>
</CENTER>
<br>
<table width="100%" border="0">
    <tr>
        <td width="15%">KEPADA YTH</td>
        <td width="1%">:</td>
        <td width="84%">PROPERTI &amp; ADVERTISING SALES SECTION HEAD</td>
    </tr>
    <tr>
        <td>DARI</td>
        <td>:</td>
        <td>AIRPORT LANDSIDE &amp; TERMINAL SECTION HEAD</td>
    </tr>
    <tr>
        <td>PERIHAL</td>
        <td>:</td>
        <td>PEMBAYARAN PAS BANDARA</td>
    </tr>
</table>

<HR>
Diharapkan menerima dan membuatkan kwitansi pembayaran Pas Bandara sesuai daftar terlampir dengan perincian sebagai
berikut : <BR>
<table width="100%" border="0">
    <tr>
        <td width="35%">Nama Perusahaan / Instansi</td>
        <td width="0%">:</td>
        <td width="65%"><?php echo "$nama_pemohon"; ?></td>
    </tr>
    <tr>
        <td>Sesuai surat permohonan Pas No.</td>
        <td>:</td>
        <td><?php echo "$no_surat_pemohon"; ?></td>
    </tr>
    <tr>
        <td>Periode</td>
        <td>:</td>

        <td>TAHUNAN <?php echo date("Y"); ?></td>
    </tr>
</table>

<BR>
<span class="style1"><strong>PAS ORANG / KONTRAK / PAS TAHUNAN <?php echo date("Y"); ?> sbb :</strong><br>
</span>
<table cellspacing="0" cellpadding="0" border="1" style="border-collapse:collapse">
    <col width="26"/>
    <col width="125"/>
    <col width="124"/>
    <col width="133"/>
    <col width="43"/>
    <col width="39"/>
    <tr height="18" valign="top">
        <td width="26" class="style1">
            <div align="center"><strong>NO</strong></div>
        </td>
        <td width="110" class="style1">
            <div align="center"><strong>NO. URUT<br>
                    PAS / STICKER</strong></div>
        </td>
        <td width="260" class="style1">
            <div align="center"><strong>Jenis Pas<br>
                    Kategori - NPA/RPA</strong></div>
        </td>
        <td width="233" class="style1">
            <div align="center"><strong>Jangka Waktu<br/>
                    Bln/Thn&nbsp;</strong></div>
        </td>
        <td width="60" class="style1">
            <div align="center"><strong>Jumlah<br>
                    P A S</strong></div>
        </td>
        <td width="68" class="style1">
            <div align="center"><strong>Jumlah<br>
                    Total</strong></div>
        </td>
    </tr>
    <tr height="18">
        <td height="18" class="style1">&nbsp;</td>
        <td colspan="5" class="style1"><strong>* TAGIHAN PAS ORANG</strong></td>
    </tr>
    <?php


    $sql2 = "SELECT * , count(TGL_MULAI_ORG_THN) AS R FROM pass_orang_tahun,jenis_pass WHERE pass_orang_tahun.ID_JENIS_PASS=jenis_pass.ID_JENIS_PASS and ID_TRANSAKSI = '$id_transaksi' GROUP BY TGL_MULAI_ORG_THN,	TGL_SELESAI_ORG_THN";
    $result2 = $mysqli->query($sql2);
    $no = 0;
    while ($data2 = $result2->fetch_array()) {
        $no++;
        $ID_JENIS_PASSW = $data2['ID_JENIS_PASS'];
        $NAMA_JENIS_PASS = $data2['NAMA_JENIS_PASS'];
        $SERI_PASS_ORG_BLN = $data2['SERI_PASS_ORG_BLN'];
        $R = $data2['R'];
        $TGL_MULAI_ORG_BLN = $data2['TGL_MULAI_ORG_BLN'];
        $array1 = explode("-", $TGL_MULAI_ORG_BLN);
        $bulan1 = $array1[1];
        $TGL_SELESAI_ORG_BLN = $data2['TGL_SELESAI_ORG_BLN'];
        $array2 = explode("-", $TGL_SELESAI_ORG_BLN);
        $bulan2 = $array2[1];
        $SELMONTH = $bulan2 - $bulan1;
        $perkalianc = $R * $SELMONTH;


        ?>
        <tr height="18">
            <td height="18" class="style1">
                <div align="center"><?php echo "$no"; ?></div>
            </td>
            <td class="style1"> <?php echo "$SERI_PASS_ORG_BLN"; ?> </td>
            <td class="style1"><?php echo "$NAMA_JENIS_PASS"; ?></td>
            <td width="233" class="style1"> JANUARI - DESEMBER</td>
            <td class="style1"> <?php echo "$R x $SELMONTH"; ?> </td>
            <td class="style1">  <?php echo "$perkalianc"; ?> </td>
        </tr>

        <?php

    }
    ?>
    <tr height="18">
        <td height="18" class="style1">
            <div align="center"></div>
        </td>
        <td colspan="5" class="style1"><strong>** TAGIHAN KENDARAAN</strong></td>
    </tr>
    <?php


    $sql2p = "SELECT * FROM jenis_kendaraan ";
    $result2p = $mysqli->query($sql2p);
    $nos = 0;
    while ($data2p = $result2p->fetch_array()) {
        $nos++;
        $ID_JENIS_KEND = $data2p['ID_JENIS_KEND'];
        $NAMA_JENIS = $data2p['NAMA_JENIS'];

        ?>
        <tr height="18">
            <td height="18" class="style1">
                <div align="center"><?php echo "$nos"; ?></div>
            </td>
            <td class="style1"></td>
            <td class="style1"><?php echo "$NAMA_JENIS"; ?></td>
            <td width="233" class="style1"></td>
            <td class="style1"></td>
            <td class="style1"></td>
        </tr>

        <?php

    }
    ?>
</table>
<br>
Demikian disampaikan atas perhatian dan kerjasamanya diucapkan terima kasih.
<br><br>
<table width="100%" border="0">
    <tr>
        <td width="49%">&nbsp;</td>
        <td width="1%">&nbsp;</td>
        <td width="50%"><p align="center">Semarang, <?php echo indonesian_date(date('d - M - Y')); ?><br/>
                AIRPORT OPS. LANDSIDE &amp; TERMINAL<br/>
                SECTION HEAD
                <br>
                <br><br><br><br>
                <strong>SUKIMAN </strong></p></td>
    </tr>
</table>
<span class="style2">Tembusan  Yth. :<br>
1. Airport Operation &amp; Services Departement Head<br>
2. Properti  &amp; Advertising Sales Section  Head (  untuk penerbitan tagihan dan pajak ) </span>
</body>
