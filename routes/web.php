<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
//
Route::get('/', function () {
    return view('front.index');
});

Route::get('home', function () {
    return view('homepage');
})->name('home');


Route::get('/logoff', function () {
    Session::forget('trans_id');
    if (!Session::has('trans_id')) {
        return view('front.index');
    }
});

Route::get('login', 'UserController@login')->name('login');
Route::post('dologin', 'UserController@doLogin')->name('dologin');
Route::get('logout', 'UserController@doLogout')->name('logout');


Route::group(['middleware' => 'auth'], function () {


    /// start user route
    Route::get('users', 'UserController@index')->name('User.index');
    Route::post('users/store', 'UserController@store')->name('User.store');
    Route::get('users/edit/{id}', 'UserController@edit')->name('User.edit');
    Route::get('users/delete/{ID_USER}', 'UserController@destroy')->name('User.delete');
    Route::patch('users/update', 'UserController@update')->name('User.update');

    // end user route


    // start transaction route
    Route::get('transaction/backs', 'TransaksiControl@back1')->name('transaksi.back1');
    Route::get('transaction', 'TransaksiControl@index')->name('transaksi.index');
    Route::get('transaction/printing?id={id}', 'TransaksiControl@cetak')->name('transaksi.cetak');
    Route::get('transaction/print/{id}', 'TransaksiControl@detil')->name('transaksi.detil');
    Route::get('transaction/confirmation/{id}', 'TransaksiControl@konfirmasi')->name('transaksi.konfirmasi');

    //Route::get('transaction/fill/{id}', 'TransaksiControl@konfirmasi')->name('transaksi.fill');

    Route::get('transaction/goto/{id}', 'TransaksiControl@menujuscrn')->name('transaksi.menujuscrn');
    Route::get('transaction/activation/{id}', 'TransaksiControl@aktifasi')->name('transaksi.aktifasi');

    Route::get('transaction/fillform/year/{id}', 'TransaksiControl@isikonfirmasiorgtahun')->name('transaksi.isikonfirmasiorgtahun');
    Route::get('transaction/fillform/month/{id}', 'TransaksiControl@isikonfirmasiorgbulan')->name('transaksi.isikonfirmasiorgbulan');


    Route::get('transaction/deactivate/people/month/{id}', 'TransaksiControl@aktivasideaktivasiorangbulan')->name('transaksi.aktivasideaktivasiorangbulan');
    Route::get('transaction/deactivate/people/year/{id}', 'TransaksiControl@aktivasideaktivasiorangtahun')->name('transaksi.aktivasideaktivasiorangtahun');

    Route::get('transaction/deactivate/vehicle/month/{id}', 'TransaksiControl@aktivasideaktivasikendbulan')->name('transaksi.aktivasideaktivasikendbulan');
    Route::get('transaction/deactivate/vehicle/year/{id}', 'TransaksiControl@aktivasideaktivasikendtahun')->name('transaksi.aktivasideaktivasikendtahun');

    Route::get('transaction/log/{id}', 'TransaksiControl@log')->name('transaksi.log');
    Route::get('transaction/edit/{id}', 'TransaksiControl@edit')->name('transaksi.edit');
    Route::patch('transaction/update', 'TransaksiControl@update')->name('transaksi.update');
    Route::get('transaction/edit/people/year/{id}', 'TransaksiControl@editorangtahun')->name('transaksi.editorangtahun');
    Route::patch('transaction/update/people/year', 'TransaksiControl@updateorangtahun')->name('transaksi.updateorangtahun');


    Route::patch('transaction/node/update', 'TransaksiControl@updatenodis')->name('transaksi.updatenodis');
    Route::get('transaction/node/edit/{id}', 'TransaksiControl@editnodis')->name('transaksi.editnodis');

    //end transaction route

    //start log route
    Route::get('log', 'TransaksiControl@logtampil')->name('transaksi.logtampil');

    //end log route

    //start registrant route
    Route::get('registrant', 'PendaftarControl@index')->name('pendaftar.index');
    Route::get('registrant/detail/{id}', 'PendaftarControl@detil')->name('pendaftar.detil');
    Route::post('registrant/store', 'PendaftarControl@store')->name('pendaftar.store');
    Route::get('registrant/edit/{id}', 'PendaftarControl@edit')->name('pendaftar.edit');
    Route::patch('registrant/update', 'PendaftarControl@update')->name('pendaftar.update');
    Route::get('registrant/delete/{id}', 'PendaftarControl@destroy')->name('pendaftar.delete');


    //end registrant route


    //start vehicle route

    Route::get('vehicle', 'KendaraanControl@index')->name('kendaraan.index');
    Route::post('vehicle/store', 'KendaraanControl@store')->name('kendaraan.store');
    Route::get('vehicle/edit/{id}', 'KendaraanControl@edit')->name('kendaraan.edit');
    Route::patch('vehicle/update', 'KendaraanControl@update')->name('kendaraan.update');
    Route::get('vehicle/delete/{id}', 'KendaraanControl@destroy')->name('kendaraan.delete');

    //end vehicle route

    //start pass route
    Route::get('pass/people/delete/{seri}/{id}/{periode}', 'PassOrangControl@destroy')->name('passorang.delete');

    Route::get('pass/vehicle', 'PassKendaraanControl@index')->name('passkendaraan.index');
    Route::post('pass/vehicle/store', 'PassKendaraanControl@store')->name('passkendaraan.store');
    Route::get('pass/vehicle/delete/{seri}/{id}/{periode}', 'PassKendaraanControl@destroy')->name('passkendaraan.delete');

    Route::get('pass/expired', 'ExpiredController@index')->name('expiredpass.index');

    //end pass route
    Route::get('report', 'ReportControl@index')->name('report.index');
    Route::post('report/show', 'ReportControl@show')->name('report.show');
    Route::post('report/excel', 'ReportControl@cetakexcel')->name('report.cetakexcel');

    //start blacklist route
    Route::get('blacklist', 'BlacklistControl@index')->name('datablacklist.index');
    Route::get('blacklist/select', 'BlacklistControl@pilihBlacklist')->name('pilihblacklist');
    Route::get('blacklist/detail/{id}', 'BlacklistControl@show')->name('blacklist.show');
    Route::post('blacklist/store', 'BlacklistControl@store')->name('blacklist.store');
    Route::get('blacklist/delete/{id}/{peg}', 'BlacklistControl@destroy')->name('blacklist.delete');

    //end blacklist route

    //print pass




});

Route::get('print/notadinas', 'FrontController@printNota')->name('front.print');

//begin front controller

Route::get('front/index', 'FrontController@index')->name('front.index');
Route::get('front/status', 'FrontController@status')->name('front.status');
Route::post('front/status/cek', 'FrontController@cek')->name('front.status.cek');
Route::get('front/dashboard', 'FrontController@dashboard')->name('front.dashboard');
Route::get('front/newpass', 'NewPassController@index')->name('front.newpass');
Route::get('front/pass', 'NewPassController@pass')->name('front.pass');
Route::get('front/register', 'NewPassController@register')->name('front.register');
Route::post('front/register/go', 'NewPassController@registergo')->name('front.registergo');

Route::post('front/newpass/store', 'NewPassController@store')->name('front.storepassperson');

Route::get('front/pass/renew', 'NewPassController@perpanjang')->name('front.perpanjang');
//fungsi keluar status


//security

Route::get('front/security', 'FamilySchoolController@create')->name('front.security');
Route::patch('front/security/store', 'FamilySchoolController@store')->name('front.security.store');

//end front controller
Route::get('pass/people', 'PassOrangControl@index')->name('passorang.index');
Route::post('pass/people/store', 'PassOrangControl@store')->name('passorang.store');
Route::get('pass/people/detail/{id}', 'PassOrangControl@show')->name('passorang.show');
Route::get('pass/people/edit/{seri}/{id}', 'PassOrangControl@edit')->name('passorang.edit');

Route::get('pass/vehicle', 'PassKendaraanControl@index')->name('passkendaraan.index');
Route::post('pass/vehicle/store', 'PassKendaraanControl@store')->name('passkendaraan.store');
Route::get('pass/vehicle/delete/{seri}/{id}/{periode}', 'PassKendaraanControl@destroy')->name('passkendaraan.delete');
Route::get('pass/vehicle/get/{id}/{idtrans}', 'PassKendaraanControl@getKend')->name('passkendaraan.getkend');
Route::patch('pass/vehicle/renew', 'PassKendaraanControl@perpanjangPassKend')->name('passkendaraan.perpanjang');

Route::get('transaction/index', 'TransaksiControl@index2')->name('transaksi.index2');
Route::post('transaction/store', 'TransaksiControl@store')->name('transaksi.store');

Route::get('inputscreening/', 'InputScreeningControl@index')->name('inputscreening.index');
Route::get('inputscreening/create', 'InputScreeningControl@create')->name('inputscreening.create');
Route::patch('inputscreening/store', 'InputScreeningControl@store')->name('inputscreening.store');

Route::get('pass/people/getpendaf/{id}/{idtrans}', 'PassOrangControl@getPendaf')->name('passorang.getpendaf');
Route::patch('pass/people/renew', 'PassOrangControl@perpanjangPass')->name('passorang.perpanjang');


Route::get('send_test_email', function () {
    Mail::raw('Sending emails with Mailgun and Laravel is easy!', function ($message) {
        $message->subject('Mailgun and Laravel are awesome!');
        $message->to('hasan.shobri@outlook.com');
    });
});