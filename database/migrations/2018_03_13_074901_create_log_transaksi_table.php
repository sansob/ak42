<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLogTransaksiTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('log_transaksi', function(Blueprint $table)
		{
			$table->integer('ID_LOG', true);
			$table->string('ID_TRANSAKSI', 20);
			$table->dateTime('TGL_LOG');
			$table->string('STATUS_TERAKHIR', 50);
			$table->text('PJ_TRANSAKSI', 65535);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('log_transaksi');
	}

}
