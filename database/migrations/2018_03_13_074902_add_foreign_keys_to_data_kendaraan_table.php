<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDataKendaraanTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('data_kendaraan', function(Blueprint $table)
		{
			$table->foreign('ID_JENIS_KEND', 'FK_MEMILIKI_JENIS_KENDARAAN')->references('ID_JENIS_KEND')->on('jenis_kendaraan')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('data_kendaraan', function(Blueprint $table)
		{
			$table->dropForeign('FK_MEMILIKI_JENIS_KENDARAAN');
		});
	}

}
