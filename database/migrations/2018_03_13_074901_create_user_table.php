<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user', function(Blueprint $table)
		{
			$table->integer('id_user', true);
			$table->integer('id_unit')->nullable()->index('FK_MEMILIKI_UNIT');
			$table->integer('id_role')->nullable()->index('FK_MEMILIKI_ROLE');
			$table->string('NAMA_USER', 50)->nullable();
			$table->string('username', 20)->nullable();
			$table->string('password', 1024)->nullable();
			$table->string('remember_token', 100)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user');
	}

}
