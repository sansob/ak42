<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPassKendTahunTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pass_kend_tahun', function(Blueprint $table)
		{
			$table->foreign('ID_TRANSAKSI', 'FK_DETIL_PASS_KEND_THN')->references('ID_TRANSAKSI')->on('transaksi_pass')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('ID_KENDARAAN', 'FK_MEMILIKI_KEND_THN')->references('ID_KENDARAAN')->on('data_kendaraan')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pass_kend_tahun', function(Blueprint $table)
		{
			$table->dropForeign('FK_DETIL_PASS_KEND_THN');
			$table->dropForeign('FK_MEMILIKI_KEND_THN');
		});
	}

}
