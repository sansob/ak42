<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDataPendaftarTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('data_pendaftar', function(Blueprint $table)
		{
			$table->string('ID_PENDAFTAR', 20)->primary();
			$table->string('NAMA_PENDAFTAR', 1024)->nullable();
			$table->string('NO_IDENTITAS', 50)->nullable();
			$table->string('NO_PEGAWAI', 50)->nullable();
			$table->string('TEMPAT_LAHIR', 50)->nullable();
			$table->date('TGL_LAHIR')->nullable();
			$table->string('AGAMA', 50)->nullable();
			$table->string('INSTANSI', 1024)->nullable();
			$table->string('ALAMAT_KANTOR', 1024)->nullable();
			$table->string('ALAMAT_RUMAH', 1024)->nullable();
			$table->string('NO_TELP', 20)->nullable();
			$table->string('NO_TELP_KANTOR', 20)->nullable();
			$table->string('PEKERJAAN', 1024)->nullable();
			$table->string('STATUS_KERJA', 20)->nullable();
			$table->string('MASA_KERJA', 100)->nullable();
			$table->string('TEMPAT_KERJA_SBLMNYA', 1024)->nullable();
			$table->text('STATUS_PENDAFTAR', 65535);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('data_pendaftar');
	}

}
