<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDataPendidikanTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('data_pendidikan', function(Blueprint $table)
		{
			$table->foreign('ID_PENDAFTAR', 'FK_MEMILIKI_PENDIDIKAN')->references('ID_PENDAFTAR')->on('data_pendaftar')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('data_pendidikan', function(Blueprint $table)
		{
			$table->dropForeign('FK_MEMILIKI_PENDIDIKAN');
		});
	}

}
