<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDataPendidikanTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('data_pendidikan', function(Blueprint $table)
		{
			$table->string('ID_DATA_PEND', 20)->primary();
			$table->string('ID_PENDAFTAR', 20)->nullable()->index('FK_MEMILIKI_PENDIDIKAN');
			$table->string('SD', 1024)->nullable();
			$table->string('LOKASI_SD', 1024)->nullable();
			$table->string('THN_LULUS_SD', 4)->nullable();
			$table->string('SMP', 1024)->nullable();
			$table->string('LOKASI_SMP', 1024)->nullable();
			$table->string('THN_LULUS_SMP', 4)->nullable();
			$table->string('SMA', 1024)->nullable();
			$table->string('LOKASI_SMA', 1024)->nullable();
			$table->string('THN_LULUS_SMA', 4)->nullable();
			$table->string('PEND_TINGGI', 1024)->nullable();
			$table->string('LOKASI_PEND_TINGGI', 1024)->nullable();
			$table->string('THN_LULUS_PEND_TINGGI', 4)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('data_pendidikan');
	}

}
