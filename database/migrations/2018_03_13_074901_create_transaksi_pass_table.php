<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTransaksiPassTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transaksi_pass', function(Blueprint $table)
		{
			$table->string('ID_TRANSAKSI', 20)->primary();
			$table->string('NAMA_PEMOHON', 1024)->nullable();
			$table->string('NO_SURAT_PEMOHON', 100)->nullable();
			$table->dateTime('TGL_TRANSAKSI')->nullable();
			$table->string('STATUS_TRANSAKSI', 1024)->nullable();
			$table->string('NOTA_DINAS', 1024)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transaksi_pass');
	}

}
