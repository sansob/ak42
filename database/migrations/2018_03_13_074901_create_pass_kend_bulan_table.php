<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePassKendBulanTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pass_kend_bulan', function(Blueprint $table)
		{
			$table->string('SERI_PASS_KEND_BLN', 20)->primary();
			$table->string('ID_TRANSAKSI', 20)->nullable()->index('FK_DETIL_PASS_KEND_BLN');
			$table->string('ID_KENDARAAN', 20)->nullable()->index('FK_MEMILIKI_KEND_BLN');
			$table->string('PEMEGANG_BLN', 1024)->nullable();
			$table->date('TGL_MULAI_KEND_BLN')->nullable();
			$table->date('TGL_SELESAI_KEND_BLN')->nullable();
			$table->string('STATUS_KEND_BLN', 100)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pass_kend_bulan');
	}

}
