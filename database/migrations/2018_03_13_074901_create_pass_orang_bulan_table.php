<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePassOrangBulanTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pass_orang_bulan', function(Blueprint $table)
		{
			$table->string('SERI_PASS_ORG_BLN', 20)->primary();
			$table->string('ID_PENDAFTAR', 20)->nullable()->index('FK_MEMILIKI_PEND_BLN');
			$table->string('ID_JENIS_PASS', 20)->nullable()->index('FK_MEMILIKI_JENIS_PASS');
			$table->string('ID_TRANSAKSI', 20)->nullable()->index('FK_DETIL_PASS_ORG_BLN');
			$table->date('TGL_MULAI_ORG_BLN')->nullable();
			$table->date('TGL_SELESAI_ORG_BLN')->nullable();
			$table->string('STATUS_ORG_BLN', 100)->nullable();
			$table->string('KET_ZONA_BLN', 30)->nullable();
			$table->text('CATATAN_ORG_BLN', 65535);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pass_orang_bulan');
	}

}
