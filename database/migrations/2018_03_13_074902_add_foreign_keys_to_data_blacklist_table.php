<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDataBlacklistTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('data_blacklist', function(Blueprint $table)
		{
			$table->foreign('ID_PENDAFTAR', 'fk_blacklist_pendaf')->references('ID_PENDAFTAR')->on('data_pendaftar')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('data_blacklist', function(Blueprint $table)
		{
			$table->dropForeign('fk_blacklist_pendaf');
		});
	}

}
