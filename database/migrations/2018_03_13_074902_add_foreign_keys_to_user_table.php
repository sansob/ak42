<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user', function(Blueprint $table)
		{
			$table->foreign('id_role', 'FK_MEMILIKI_ROLE')->references('id_role')->on('role_user')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('id_unit', 'FK_MEMILIKI_UNIT')->references('id_unit')->on('unit_kerja')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user', function(Blueprint $table)
		{
			$table->dropForeign('FK_MEMILIKI_ROLE');
			$table->dropForeign('FK_MEMILIKI_UNIT');
		});
	}

}
