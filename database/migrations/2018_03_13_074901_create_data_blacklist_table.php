<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDataBlacklistTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('data_blacklist', function(Blueprint $table)
		{
			$table->integer('ID_BLACKLIST', true);
			$table->string('ID_PENDAFTAR', 20)->index('fk_blacklist_pendaf');
			$table->date('TGL_BLACKLIST');
			$table->text('ALASAN', 65535);
			$table->text('FOTO', 65535);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('data_blacklist');
	}

}
