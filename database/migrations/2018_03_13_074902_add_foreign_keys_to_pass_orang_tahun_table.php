<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPassOrangTahunTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pass_orang_tahun', function(Blueprint $table)
		{
			$table->foreign('ID_TRANSAKSI', 'FK_DETIL_PASS_ORG_THN')->references('ID_TRANSAKSI')->on('transaksi_pass')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('ID_JENIS_PASS', 'FK_MEMILIKI_JENIS')->references('ID_JENIS_PASS')->on('jenis_pass')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('ID_PENDAFTAR', 'FK_MEMILIKI_PENDAFTAR')->references('ID_PENDAFTAR')->on('data_pendaftar')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pass_orang_tahun', function(Blueprint $table)
		{
			$table->dropForeign('FK_DETIL_PASS_ORG_THN');
			$table->dropForeign('FK_MEMILIKI_JENIS');
			$table->dropForeign('FK_MEMILIKI_PENDAFTAR');
		});
	}

}
