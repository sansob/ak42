<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDataKendaraanTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('data_kendaraan', function(Blueprint $table)
		{
			$table->string('ID_KENDARAAN', 20)->primary();
			$table->string('ID_JENIS_KEND', 20)->nullable()->index('FK_MEMILIKI_JENIS_KENDARAAN');
			$table->string('NAMA_KENDARAAN', 1024)->nullable();
			$table->string('NO_POLISI', 15)->nullable();
			$table->string('WARNA_PLAT', 20)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('data_kendaraan');
	}

}
