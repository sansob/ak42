<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePassOrangTahunTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pass_orang_tahun', function(Blueprint $table)
		{
			$table->string('SERI_PASS_ORG_THN', 20)->primary();
			$table->string('ID_JENIS_PASS', 20)->nullable()->index('FK_MEMILIKI_JENIS');
			$table->string('ID_TRANSAKSI', 20)->nullable()->index('FK_DETIL_PASS_ORG_THN');
			$table->string('ID_PENDAFTAR', 20)->nullable()->index('FK_MEMILIKI_PENDAFTAR');
			$table->date('TGL_MULAI_ORG_THN')->nullable();
			$table->date('TGL_SELESAI_ORG_THN')->nullable();
			$table->string('STATUS_ORG_THN', 100)->nullable();
			$table->string('KET_ZONA_THN', 30)->nullable();
			$table->text('CATATAN_ORG_THN', 65535);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pass_orang_tahun');
	}

}
