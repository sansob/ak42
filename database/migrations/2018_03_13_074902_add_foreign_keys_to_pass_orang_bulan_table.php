<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPassOrangBulanTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pass_orang_bulan', function(Blueprint $table)
		{
			$table->foreign('ID_TRANSAKSI', 'FK_DETIL_PASS_ORG_BLN')->references('ID_TRANSAKSI')->on('transaksi_pass')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('ID_JENIS_PASS', 'FK_MEMILIKI_JENIS_PASS')->references('ID_JENIS_PASS')->on('jenis_pass')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('ID_PENDAFTAR', 'FK_MEMILIKI_PEND_BLN')->references('ID_PENDAFTAR')->on('data_pendaftar')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pass_orang_bulan', function(Blueprint $table)
		{
			$table->dropForeign('FK_DETIL_PASS_ORG_BLN');
			$table->dropForeign('FK_MEMILIKI_JENIS_PASS');
			$table->dropForeign('FK_MEMILIKI_PEND_BLN');
		});
	}

}
