<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDataKeluargaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('data_keluarga', function(Blueprint $table)
		{
			$table->string('ID_DATA_KELUARGA', 20)->primary();
			$table->string('ID_PENDAFTAR', 20)->nullable()->index('FK_MEMILIKI_KELUARGA');
			$table->string('NAMA_BAPAK', 1024)->nullable();
			$table->string('NAMA_IBU', 1024)->nullable();
			$table->string('ALAMAT_ORTU', 1024)->nullable();
			$table->string('NAMA_PASANGAN', 1024)->nullable();
			$table->string('ALAMAT_PASANGAN', 1024)->nullable();
			$table->string('NO_TELP_PSG', 1024)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('data_keluarga');
	}

}
