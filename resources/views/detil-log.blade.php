@extends('template')
@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Transaksi</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>

                    <th>ID Transaksi</th>
                    <th>Tanggal Log</th>
                    <th>Status Transaksi</th>
                    <th>PJ Transaksi</th>


                </tr>
                </thead>
                <?php
                $role = Auth::user()->id_role;
                $unit = Auth::user()->id_unit;
                $logs = DB::table('log_transaksi')
                    ->where('ID_TRANSAKSI', '=', $lempar)->get();
                ?>

                @if( $unit != 4)
                    <tbody>

                    @foreach($logs as $T)
                        <tr>

                            <td>{{$T->ID_TRANSAKSI}}</td>
                            <td>{{ $T->TGL_LOG}}</td>
                            <td>{{ $T->STATUS_TERAKHIR}}</td>
                            <td>{{ $T->PJ_TRANSAKSI}}</td>


                        </tr>
                    @endforeach


                    </tbody>
                @endif

                <tfoot>

                </tfoot>
            </table>
        </div>
        <!-- /.box-body -->
    </div>

    <!-- /.box -->
    </div>
    <!-- /.col -->
    </div>
    <!-- /.row -->




    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
        <!-- <div class="modal-content">


    </div>
  </div>







@endsection
        @section('custom_script')
            <!-- jQuery 2.2.3 -->
                <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
                <!-- Bootstrap 3.3.6 -->
                <script src="bootstrap/js/bootstrap.min.js"></script>
                <!-- DataTables -->
                <script src="plugins/datatables/jquery.dataTables.min.js"></script>
                <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
                <!-- SlimScroll -->
                <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
                <!-- FastClick -->
                <script src="plugins/fastclick/fastclick.js"></script>
                <!-- AdminLTE App -->
                <script src="dist/js/app.min.js"></script>
                <!-- AdminLTE for demo purposes -->
                <script src="dist/js/demo.js"></script>
                <!-- page script -->
                <script type="text/javascript"
                        src="{{URL::asset('plugins/jQuery/bootstrap-confirmation.min.js')}}"></script>
                <script>
                    $(document).find('[data-toggle="confirmation"]').confirmation();
                </script>
                <script>
                    $(function () {
                        $("#example1").DataTable();
                        $('#example2').DataTable({
                            "paging": true,
                            "lengthChange": false,
                            "searching": false,
                            "ordering": true,
                            "info": true,
                            "autoWidth": false
                        });
                    });
                </script>

                <script type="text/javascript">
                    $(document).on("click", '.edit_button', function (e) {

                        var id = $(this).data('id');
                        $(".id").val(id);

                    });
                </script>
                <script type="text/javascript">
                    function autoRefresh_div() {
                        $("#links").load('coba #links', function () {
                            setTimeout(autoRefresh_div, 60000);
                        });
                    }

                    autoRefresh_div();
                </script>
@endsection
