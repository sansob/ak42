@extends('templateprofil')
@section('content')
    <div class="row">
        <!-- /.col-lg-12 -->
        <div class="col-lg-12">
            <h2>Laporan Bulanan</h2>
        </div>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div id="inputtrans" class="panel-body">
                    {{ csrf_field() }}
                    <?php
                    $jumlahtrans = count($gettrans);
                    $jumlahpassorang = count($getpob) + count($getpot);
                    $jumlahpasskend = count($getpkb) + count($getpkt);
                    $bulan = $bln;
                    $tahun = $thn;

                    ?>

                    <div class="row">
                        <div class="col-md-4" >
                            <div id="laporan1">
                                <h3>{{$jumlahtrans}}</h3>
                                <p>Jumlah Transaksi</p>

                            </div>

                        </div>
                        <div class="col-md-4">
                            <div>
                                <h3>{{$jumlahpassorang}}</h3>
                                <p>Jumlah Pass Orang</p>

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div>
                                <h3>{{$jumlahpasskend}}</h3>
                                <p>Jumlah Pass Kendaraan</p>

                            </div>
                        </div>
                    </div>

                    <form action="{{ route('report.cetakexcel') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="tahun" value="<?= $tahun ?>">
                        <input type="hidden" name="bulan" value="<?= $bulan ?>">



                        <button type="submit" name="cetak" value="cetak" class="btn btn-success btn-lg">Cetak Laporan
                        </button>

                    </form>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#pendaftar" data-toggle="tab">Data Pendaftar</a></li>
                        <li><a href="#kendaraan" data-toggle="tab">Data Kendaraan</a></li>
                    </ul>
                    <div class="box-body">
                        <div class="tab-content">
                            <div class="active tab-pane" id="pendaftar">
                                <div class="tab-content">
                                    <label for="title">Periode Bulanan</label>
                                    <table id="pendaftarbulan" class="table table-bordered table-striped">
                                        <thead>
                                        <th>ID Pass</th>
                                        <th>Nama</th>
                                        <th>Pekerjaan</th>
                                        <th>Instansi</th>
                                        <th>Alamat Kantor</th>
                                        <th>Nomor Telepon Kantor</th>
                                        <th>Jenis Pass</th>
                                        <th>Keterangan Zona</th>
                                        <th>Tanggal Mulai Berlaku</th>
                                        <th>Tanggal Selesai Berlaku</th>

                                        </thead>
                                        <tbody>
                                        <?php $PassOrangBulan = $getpob?>
                                        @foreach($PassOrangBulan as $POB)
                                            <tr>
                                                <td>{{ $POB->SERI_PASS_ORG_BLN }}</td>
                                                <td>{{ $POB->PendaftarPassBln->NAMA_PENDAFTAR }}</td>
                                                <td>{{ $POB->PendaftarPassBln->PEKERJAAN }}</td>
                                                <td>{{ $POB->PendaftarPassBln->INSTANSI}}</td>
                                                <td>{{ $POB->PendaftarPassBln->ALAMAT_KANTOR}}</td>
                                                <td>{{ $POB->PendaftarPassBln->NO_TELP_KANTOR}}</td>
                                                <td>{{ $POB->JenisPassBln->NAMA_JENIS_PASS}}</td>
                                                <td>{{ $POB->KET_ZONA_BLN }}</td>
                                                <td>{{ date('d-m-Y',strtotime($POB->TGL_MULAI_ORG_BLN))}}</td>
                                                <td>{{date('d-m-Y',strtotime($POB->TGL_SELESAI_ORG_BLN))}}</td>


                                            </tr>
                                        @endforeach
                                        </tbody>
                                        <tfoot>
                                        </tfoot>
                                    </table>
                                    <label for="title">Periode Tahunan</label>
                                    <table id="pendaftartahun" class="table table-bordered table-striped">
                                        <thead>
                                        <th>ID Pass</th>
                                        <th>Nama</th>
                                        <th>Pekerjaan</th>
                                        <th>Instansi</th>
                                        <th>Alamat Kantor</th>
                                        <th>Nomor Telepon Kantor</th>
                                        <th>Jenis Pass</th>
                                        <th>Keterangan Zona</th>
                                        <th>Tanggal Mulai Berlaku</th>
                                        <th>Tanggal Selesai Berlaku</th>

                                        </thead>
                                        <tbody>
                                        <?php $PassOrangTahun = $getpot;?>
                                        @foreach($PassOrangTahun as $POT)
                                            <tr>
                                                <td>{{ $POT->SERI_PASS_ORG_THN }}</td>
                                                <td>{{ $POT->PendaftarPassThn->NAMA_PENDAFTAR }}</td>
                                                <td>{{ $POT->PendaftarPassThn->PEKERJAAN }}</td>
                                                <td>{{ $POT->PendaftarPassThn->INSTANSI}}</td>
                                                <td>{{ $POT->PendaftarPassThn->ALAMAT_KANTOR}}</td>
                                                <td>{{ $POT->PendaftarPassThn->NO_TELP_KANTOR}}</td>
                                                <td>{{ $POT->NAMA_JENIS_PASS}}</td>
                                                <td>{{ $POT->KET_ZONA_THN }}</td>
                                                <td>{{ date('d-m-Y',strtotime($POT->TGL_MULAI_ORG_THN))}}</td>
                                                <td>{{date('d-m-Y',strtotime($POT->TGL_SELESAI_ORG_THN))}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                        <tfoot>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            <!-- /.tab-pane -->


                            <div class="tab-pane" id="kendaraan">
                                <div class="tab-content">
                                    <label for="title">Periode Bulanan</label>
                                    <?php
                                    $kendaraan = $getpkb;
                                    $kendaraantahun = $getpkt;?>

                                    <table id="kendaraanbulan" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>ID Pass</th>
                                            <th>Nama Kendaraan</th>
                                            <th>Jenis Kendaraan</th>
                                            <th>Nomor Polisi</th>
                                            <th>Warna Plat</th>
                                            <th>Tanggal Mulai Berlaku</th>
                                            <th>Tanggal Selesai Berlaku</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($kendaraan as $kend)
                                            <tr>
                                                <td>{{ $kend->SERI_PASS_KEND_BLN}}</td>
                                                <td>{{ $kend->KendaraanPassBln->NAMA_KENDARAAN}}</td>
                                                <td>{{ $kend->KendaraanPassBln->JenisKend->NAMA_JENIS}}</td>
                                                <td>{{ $kend->KendaraanPassBln->NO_POLISI}}</td>
                                                <td>{{ $kend->KendaraanPassBln->WARNA_PLAT}}</td>
                                                <td>{{ date('d-m-Y',strtotime($kend->TGL_MULAI_KEND_BLN))}}</td>
                                                <td>{{ date('d-m-Y',strtotime($kend->TGL_SELESAI_KEND_BLN))}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>

                                    </table>


                                    <label for="title">Periode Tahunan</label>
                                    <table id="kendaraantahun" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>ID Pass</th>
                                            <th>Nama Kendaraan</th>
                                            <th>Jenis Kendaraan</th>
                                            <th>Nomor Polisi</th>
                                            <th>Warna Plat</th>
                                            <th>Tanggal Mulai Berlaku</th>
                                            <th>Tanggal Selesai Berlaku</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($kendaraantahun as $kendthn)
                                            <tr>
                                                <td>{{ $kendthn->SERI_PASS_KEND_THN}}</td>
                                                <td>{{ $kendthn->KendaraanPassThn->NAMA_KENDARAAN}}</td>
                                                <td>{{ $kendthn->KendaraanPassThn->JenisKend->NAMA_JENIS}}</td>
                                                <td>{{ $kendthn->KendaraanPassThn->NO_POLISI}}</td>
                                                <td>{{ $kendthn->KendaraanPassThn->WARNA_PLAT}}</td>
                                                <td>{{ date('d-m-Y',strtotime($kendthn->TGL_MULAI_KEND_THN))}}</td>
                                                <td>{{ date('d-m-Y',strtotime($kendthn->TGL_SELESAI_KEND_THN))}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>


                                </div>

                            </div>

                            <div class="form-group">
                                <a href="{{route('report.index')}}" type="submit" class="btn btn-success btn-lg">Kembali</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>



        @endsection
        @section('custom_script')
            <!-- jQuery 2.2.3 -->
                <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
                <!-- Bootstrap 3.3.6 -->
                <script src="bootstrap/js/bootstrap.min.js"></script>
                <!-- DataTables -->
                <script src="plugins/datatables/jquery.dataTables.min.js"></script>
                <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
                <!-- SlimScroll -->
                <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
                <!-- FastClick -->
                <script src="plugins/fastclick/fastclick.js"></script>
                <!-- AdminLTE App -->
                <script src="dist/js/app.min.js"></script>
                <!-- AdminLTE for demo purposes -->
                <script src="dist/js/demo.js"></script>
                <!-- page script -->
                <script>

                    $(function () {
                        $("#kendaraanbulan").DataTable({
                            "paging": true,
                            "lengthChange": false,
                            "searching": true,
                            "ordering": false,
                            "info": false,
                            "autoWidth": true
                        });
                        $("#kendaraantahun").DataTable({
                            "paging": true,
                            "lengthChange": false,
                            "searching": true,
                            "ordering": false,
                            "info": false,
                            "autoWidth": true
                        });
                        $("#pendaftarbulan").DataTable({
                            "paging": true,
                            "lengthChange": false,
                            "searching": true,
                            "ordering": false,
                            "info": false,
                            "autoWidth": true
                        });
                        $("#pendaftartahun").DataTable({
                            "paging": true,
                            "lengthChange": false,
                            "searching": true,
                            "ordering": false,
                            "info": false,
                            "autoWidth": true
                        });
                    });
                </script>
                </body>










                <script type="text/javascript">

                    $(document).on("click", '.edit_button', function (e) {

                        var id = $(this).data('id');
                        $(".id").val(id);
                        var nama = $(this).data('nama');
                        $(".nama").val(nama);

                        var identitas = $(this).data('identitas');
                        $(".identitas").val(identitas);

                        var alamatrmh = $(this).data('alamatrmh');
                        $(".alamatrmh").val(alamatrmh);
                        var telprmh = $(this).data('telprmh');
                        $(".telprmh").val(telprmh);
                        var nopeg = $(this).data('nopeg');
                        $(".nopeg").val(nopeg);
                        var pkrjn = $(this).data('pkrjn');
                        $(".pkrjn").val(pkrjn);
                        var instansi = $(this).data('instansi');
                        $(".instansi").val(instansi);
                        var alamatkntr = $(this).data('alamatkntr');
                        $(".alamatkntr").val(alamatkntr);
                        var telpkntr = $(this).data('telpkntr');
                        $(".telpkntr").val(telpkntr);
                        var nopeg = $(this).data('nopeg');
                        $(".nopeg").val(nopeg);
                        var tmptlhr = $(this).data('tmptlhr');
                        $(".tmptlhr").val(tmptlhr);

                        var tgllhr = $(this).data('tgllhr');
                        $(".tgllhr").val(tgllhr);

                        var masa = $(this).data('masa');
                        $(".masa").val(masa);

                        var tmptkerjasblm = $(this).data('tmptkerjasblm');
                        $(".tmptkerjasblm").val(tmptkerjasblm);


                        var status = $(this).data('status');
                        if (status == 'Tetap')
                            $(".status option[value=Tetap]").attr('selected', 'selected');
                        else
                            $(".status option[value=Kontrak]").attr('selected', 'selected');

                        var agama = $(this).data('agama');
                        if (agama == 'Islam')
                            $(".agama option[value=Islam]").attr('selected', 'selected');
                        else if (agama == 'Katholik')
                            $(".agama option[value=Katholik]").attr('selected', 'selected');
                        else if (status == 'Kristen')
                            $(".agama option[value=Kristen]").attr('selected', 'selected');
                        else if (status == 'Hindu')
                            $(".agama option[value=Hindu]").attr('selected', 'selected');
                        else if (status == 'Buddha')
                            $(".agama option[value=Buddha]").attr('selected', 'selected');
                        else
                            $(".agama option[value=Lain-lain]").attr('selected', 'selected');


                    });
                    $(document).on("click", '.edit_kend', function (e) {

                        var idkend = $(this).data('idkend');
                        $(".idkend").val(idkend);
                        var namakend = $(this).data('namakend');
                        $(".namakend").val(namakend);

                        var nopol = $(this).data('nopol');
                        $(".nopol").val(nopol);


                        var warna = $(this).data('warna');
                        if (warna == 'Hitam')
                            $(".warna option[value=Hitam]").attr('selected', 'selected');
                        else
                            $(".warna option[value=Merah]").attr('selected', 'selected');

                        var jeniskend = $(this).data('jeniskend');
                        if (jeniskend == 'JK-01')
                            $(".jeniskend option[value=JK-01]").attr('selected', 'selected');
                        else if (jeniskend == 'JK-02')
                            $(".jeniskend option[value=JK-02]").attr('selected', 'selected');
                        else if (jeniskend == 'JK-03')
                            $(".jeniskend option[value=JK-03]").attr('selected', 'selected');
                        else if (jeniskend == 'JK-04')
                            $(".jeniskend option[value=JK-04]").attr('selected', 'selected');
                        else if (jeniskend == 'JK-05')
                            $(".jeniskend option[value=JK-05]").attr('selected', 'selected');
                        else
                            $(".jeniskend option[value=JK-06]").attr('selected', 'selected');

                    });

                    }
                </script>

@endsection
