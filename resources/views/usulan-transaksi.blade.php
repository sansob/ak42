@extends('template')
@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Transaksi</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>

                    <th>ID Transaksi</th>
                    <th>Nomor Surat</th>
                    <th>Nama Pemohon</th>
                    <th>Tanggal</th>
                    <th>Status</th>
                    <th>Aksi</th>
                </tr>
                </thead>
                <?php
                $role = Auth::user()->id_role;
                $unit = Auth::user()->id_unit;
                ?>
                @if($role == 3 && $unit == 2)
                    <tbody>

                    @foreach($transaksi as $T)
                        <tr>

                            <td>{{$T->ID_TRANSAKSI}}</td>
                            <td>{{ $T->NO_SURAT_PEMOHON}}</td>
                            <td>{{ $T->NAMA_PEMOHON}}</td>
                            <td>{{ $T->TGL_TRANSAKSI}}</td>
                            <td>{{ $T->STATUS_TRANSAKSI}}</td>

                            <td>
                                <?php

                                if ($T->STATUS_TRANSAKSI == "Belum Ajukan Konfirmasi") {

                                    $a = "Screening";
                                    $b = "'btn btn-xs btn-success'";
                                }
                                if ($T->STATUS_TRANSAKSI == "Perpanjang") {

                                    $a = "Ajukan Konfirmasi";
                                    $b = "'btn btn-xs btn-success'";
                                }
                                //  else
                                //  {
                                //   $a="Belum Ajukan Konfirmasi";
                                //   $b="'btn btn-xs btn-danger'";
                                //  }
                                ?>

                                @if($T->STATUS_TRANSAKSI=="Belum Ajukan Konfirmasi")
                                    <a href="{{ route('transaksi.menujuscrn',$T->ID_TRANSAKSI) }}"
                                       data-toggle="confirmation" data-title="Anda yakin?" data-btn-ok-label="Ya"
                                       data-btn-cancel-label="Batal" class=<?php echo "$b"; ?>><?php echo "$a"; ?></a>
                                @endif
                                @if($T->STATUS_TRANSAKSI=="Perpanjang")
                                    <a href="{{ route('transaksi.konfirmasi',$T->ID_TRANSAKSI) }}"
                                       data-toggle="confirmation" data-title="Anda yakin?" data-btn-ok-label="Ya"
                                       data-btn-cancel-label="Batal" class=<?php echo "$b"; ?>><?php echo "$a"; ?></a>
                                @endif
                                <a href="{{ route('transaksi.detil', $T->ID_TRANSAKSI) }}"
                                   class="btn fa fa-eye btn-xs btn-primary"></a>
                                <a href="{{ route('transaksi.editnodis', $T->ID_TRANSAKSI) }}"
                                   class="btn btn-xs btn-primary edit_button fa  fa-edit" data-toggle="modal"
                                   data-notadinas="{{$T->NOTA_DINAS}}"
                                   data-name="{{$T->ID_TRANSAKSI}}"
                                   data-target="#modaledit"></a>
                                </button>
                            </td>
                        </tr>
                    @endforeach


                    </tbody>
                @endif
                @if($role == 2 && $unit == 2 )
                    <tbody>

                    @foreach($transaksiboss as $T)
                        <tr>

                            <td>{{$T->ID_TRANSAKSI}}</td>
                            <td>{{ $T->NO_SURAT_PEMOHON}}</td>
                            <td>{{ $T->NAMA_PEMOHON}}</td>
                            <td>{{ $T->TGL_TRANSAKSI}}</td>
                            <td>{{ $T->STATUS_TRANSAKSI}}</td>

                            <td>
                                <?php

                                if ($T->STATUS_TRANSAKSI == "Belum Ajukan Konfirmasi" && $role != 2 && $unit != 2) {

                                    $a = "Ajukan Konfirmasi";
                                    $b = "'btn btn-xs btn-success'";
                                } else if ($T->STATUS_TRANSAKSI == "Sudah Ajukan Konfirmasi" && $role == 2 && $unit == 2) {

                                    $a = "Konfirmasi";
                                    $b = "'btn btn-xs btn-success'";
                                } else {
                                    $a = "";
                                    $b = "";
                                }

                                ?>
                                @if($T->STATUS_TRANSAKSI=="Belum Ajukan Konfirmasi")
                                    <a href="{{ route('transaksi.konfirmasi',$T->ID_TRANSAKSI) }}"
                                       class=<?php echo "$b"; ?>><?php echo "$a"; ?></a>
                                @endif
                                @if($T->STATUS_TRANSAKSI=="Sudah Ajukan Konfirmasi" && $role == 2 && $unit== 2)
                                    <a href="{{ route('transaksi.aktifasi',$T->ID_TRANSAKSI) }}"
                                       class=<?php echo "$b"; ?>><?php echo "$a"; ?></a>
                                @endif
                                <a href="{{ route('transaksi.detil', $T->ID_TRANSAKSI) }}"
                                   class="btn fa fa-eye btn-xs btn-primary"></a>

                                </button>
                            </td>
                        </tr>
                    @endforeach


                    </tbody>
                @endif
                @if($role == 3 && $unit == 3)
                    <tbody>

                    @foreach($transaksi as $T)
                        <tr>

                            <td>{{$T->ID_TRANSAKSI}}</td>
                            <td>{{ $T->NO_SURAT_PEMOHON}}</td>
                            <td>{{ $T->NAMA_PEMOHON}}</td>
                            <td>{{ $T->TGL_TRANSAKSI}}</td>
                            <td>{{ $T->STATUS_TRANSAKSI}}</td>

                            <td>
                                <?php

                                if ($T->STATUS_TRANSAKSI == "Belum Ajukan Konfirmasi") {

                                    $a = "Screening";
                                    $b = "'btn btn-xs btn-success'";
                                }
                                if ($T->STATUS_TRANSAKSI == "Menuju Screening") {

                                    $a = "Ajukan Konfirmasi";
                                    $b = "'btn btn-xs btn-success'";
                                } else {
                                    $a = "Belum Ajukan Konfirmasi";
                                    $b = "'btn btn-xs btn-danger'";
                                }
                                ?>

                                @if($T->STATUS_TRANSAKSI=="Menuju Screening")
                                    <a href="{{ route('transaksi.konfirmasi',$T->ID_TRANSAKSI) }}"
                                       class=<?php echo "$b"; ?>><?php echo "$a"; ?></a>
                                @endif
                                <a href="{{ route('transaksi.detil', $T->ID_TRANSAKSI) }}"
                                   class="btn fa fa-eye btn-xs btn-primary"></a>

                                </button>
                            </td>
                        </tr>
                    @endforeach


                    </tbody>
                @endif
                @if($role == 3 && $unit == 4)
                    <tbody>

                    @foreach($transaksisales as $T)
                        <tr>

                            <td>{{$T->ID_TRANSAKSI}}</td>
                            <td>{{ $T->NO_SURAT_PEMOHON}}</td>
                            <td>{{ $T->NAMA_PEMOHON}}</td>
                            <td>{{ $T->TGL_TRANSAKSI}}</td>
                            <td>{{ $T->STATUS_TRANSAKSI}}</td>

                            <td>
                                <?php

                                if ($T->STATUS_TRANSAKSI == "Belum Ajukan Konfirmasi") {

                                    $a = "Screening";
                                    $b = "'btn btn-xs btn-success'";
                                } else if ($T->STATUS_TRANSAKSI == "Menuju Screening") {

                                    $a = "Ajukan Konfirmasi";
                                    $b = "'btn btn-xs btn-success'";
                                } else if ($T->STATUS_TRANSAKSI == "Sudah Konfirmasi") {
                                    $a = "PDF";
                                    $b = "'btn btn-xs btn-success'";
                                }
                                ?>
                                @if($T->STATUS_TRANSAKSI=="Belum Ajukan Konfirmasi")
                                    <a href="{{ route('transaksi.menujuscrn',$T->ID_TRANSAKSI) }}"
                                       class=<?php echo "$b"; ?>><?php echo "$a"; ?></a>
                                @endif
                                @if($T->STATUS_TRANSAKSI=="Menuju Screening")
                                    <a href="{{ route('transaksi.konfirmasi',$T->ID_TRANSAKSI) }}"
                                       class=<?php echo "$b"; ?>><?php echo "$a"; ?></a>
                                @endif
                                @if($T->STATUS_TRANSAKSI=="Sudah Konfirmasi")
                                    <a target="_blank" href="http://localhost/ak42/nota_cetak.php?id={{$T->ID_TRANSAKSI}}"
                                       class=<?php echo "$b"; ?>><?php echo "$a"; ?></a>

                                @endif
                                <a href="{{ route('transaksi.detil', $T->ID_TRANSAKSI) }}"
                                   class="btn fa fa-eye btn-xs btn-primary"></a>

                                </button>
                            </td>
                        </tr>
                    @endforeach


                    </tbody>
                @endif
                @if($role == 1  && $unit == 1)
                    <tbody>

                    @foreach($transaksi as $T)
                        <tr>

                            <td>{{$T->ID_TRANSAKSI}}</td>
                            <td>{{ $T->NO_SURAT_PEMOHON}}</td>
                            <td>{{ $T->NAMA_PEMOHON}}</td>
                            <td>{{ $T->TGL_TRANSAKSI}}</td>
                            <td>{{ $T->STATUS_TRANSAKSI}}</td>

                            <td>
                                <?php

                                if ($T->STATUS_TRANSAKSI == "Belum Ajukan Konfirmasi") {

                                    $a = "Screening";
                                    $b = "'btn btn-xs btn-success'";
                                } else if ($T->STATUS_TRANSAKSI == "Menuju Screening") {

                                    $a = "Ajukan Konfirmasi";
                                    $b = "'btn btn-xs btn-success'";
                                } else {
                                    $a = "Belum Ajukan Konfirmasi";
                                    $b = "'btn btn-xs btn-danger'";
                                }
                                ?>
                                @if($T->STATUS_TRANSAKSI=="Belum Ajukan Konfirmasi")
                                    <a href="{{ route('transaksi.menujuscrn',$T->ID_TRANSAKSI) }}"
                                       class=<?php echo "$b"; ?>><?php echo "$a"; ?></a>
                                @endif
                                @if($T->STATUS_TRANSAKSI=="Menuju Screening")
                                    <a href="{{ route('transaksi.konfirmasi',$T->ID_TRANSAKSI) }}"
                                       class=<?php echo "$b"; ?>><?php echo "$a"; ?></a>
                                @endif
                                <a href="{{ route('transaksi.detil', $T->ID_TRANSAKSI) }}"
                                   class="btn fa fa-eye btn-xs btn-primary"></a>

                                </button>
                            </td>
                        </tr>
                    @endforeach


                    </tbody>
                @endif

                <tfoot>

                </tfoot>
            </table>
        </div>
        <!-- /.box-body -->
    </div>

    <!-- /.box -->
    </div>
    <!-- /.col -->
    </div>
    <!-- /.row -->



    <div class="modal fade" id="modaledit" tabindex="-1" role="dialog" aria- labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Nomor Nota Dinas</h4>
                </div>
                <div class="modal-body">
                    <form action="{{ route('transaksi.updatenodis') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PATCH">
                        <input type="hidden" name="ID_TRANSAKSI" class="name">
                        <!-- <div class="form-group">
                            <label for="title">ID transaksi</label>
                          <input type="text" name="ID_TRANSAKSI" class="form-control name" required>
                        </div> -->
                        <div class="form-group">
                            <label for="title">Nota Dinas</label>
                            <input type="text" name="NOTA_DINAS" class="form-control notadinas" required>
                            <label for="title">Nota Dinas Jenis</label><br>
                            <select name="tipenota" id="tipenota">
                                <option value="bulanan">Bulanan</option>
                                <option value="tahunan">Tahunan</option>
                            </select>
                        </div>

                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <button formtarget="_blank" type="submit" class="btn btn-success"><i class="glyphicon glyphicon-floppy-saved"></i>Simpan dan Cetak
                        </button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i
                                    class="glyphicon glyphicon-floppy-remove"></i> Tutup
                        </button>
                        </button>
                    </div>
                    </form>
                </div>
                </form>
            </div>
        </div>
    </div>







@endsection
@section('custom_script')
    <!-- jQuery 2.2.3 -->
    <script src="pass/js/jquery.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <!-- page script -->
    <script type="text/javascript" src="{{URL::asset('plugins/jQuery/bootstrap-confirmation.min.js')}}"></script>
    <script>
        $(document).find('[data-toggle="confirmation"]').confirmation();
    </script>
    <script>
        $(function () {
            // $("#example1").DataTable();
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });
    </script>


    <script type="text/javascript">
        function autoRefresh_div() {
            $("#links").load('coba #links', function () {
                setTimeout(autoRefresh_div, 60000);
            });
        }

        autoRefresh_div();


        $(document).ready(function () {
            $('#example1').DataTable({
                "iDisplayLength": 100,
                "bFilter": false,
                "searching": true,
                "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    if (aData[4] == "Sudah Konfirmasi") {
                        $('td', nRow).css('background-color', '#ff7675');
                    }
                    else if (aData[4] == "Belum Ajukan Konfirmasi") {
                        $('td', nRow).css('background-color', '#ffbe76');
                    }
                    else if (aData[4] == "Sudah Ajukan Konfirmasi") {
                        $('td', nRow).css('background-color', '#f6e58d');
                    }
                }
            });
        })

    </script>

    <script type="text/javascript">
        $(document).on("click", '.edit_button', function (e) {

            var name = $(this).data('name');
            $(".name").val(name);
            var notadinas = $(this).data('notadinas');
            $(".notadinas").val(notadinas);


        });
    </script>


@endsection

