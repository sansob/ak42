@extends('template-front')
@section('content')

    <div id="inputpass" class="box">
        <!-- /.box-header -->
        <div class="panel-body">
            <?php
            $getmaxtrans = DB::table('transaksi_pass')->max('ID_TRANSAKSI');
            $pemohon = DB::table('transaksi_pass')->select('NAMA_PEMOHON')->where('ID_TRANSAKSI', '=', $getmaxtrans)->get();
            ?>
            <div class="form-group">
                <label for="title">ID Transaksi:</label>
                <input type="text" name="idtrans" class="form-control" value="{{$getmaxtrans}}" disabled>
            </div>
            <div class="form-group">
                <label for="title">Pemohon:</label>
                <input type="text" name="pemohon" class="form-control" value='{{$pemohon}}' disabled>
            </div>
            <div class="form-group">
                <a href="{{ route('transaksi.index') }}" class="btn btn-success btn-lg">Selesai</a>
            </div>

        </div>




        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#pendaftar" data-toggle="tab">Data Pendaftar</a></li>
                <li><a href="#kendaraan" data-toggle="tab">Data Kendaraan</a></li>
            </ul>
            <div class="box-body">
                <div class="tab-content">
                    <div class="active tab-pane" id="pendaftar">
                        <div class="tab-content">
                            <label for="title">Periode Bulanan</label>
                            <table id="pendaftarbulan" class="table table-bordered table-striped">
                                <thead>
                                <th>ID Pass</th>
                                <th>Nama</th>
                                <th>Alamat Rumah</th>
                                <th>Nomor Telepon</th>
                                <th>Nomor Pegawai</th>
                                <th>Pekerjaan</th>
                                <th>Instansi</th>
                                <th>Alamat Kantor</th>
                                <th>Nomor Telepon Kantor</th>
                                <th>Aksi</th>

                                </thead>
                                <tbody>
                                <?php $PassOrangBulan = \App\PassOrangBulanModel::where('ID_TRANSAKSI', $getmaxtrans)->get();?>
                                @foreach($PassOrangBulan as $POB)
                                    <tr>
                                        <td>{{ $POB->SERI_PASS_ORG_BLN }}</td>
                                        <td>{{ $POB->PendaftarPassBln->NAMA_PENDAFTAR }}</td>
                                        <td>{{ $POB->PendaftarPassBln->ALAMAT_RUMAH}}</td>
                                        <td>{{ $POB->PendaftarPassBln->NO_TELP}}</td>
                                        <td>{{ $POB->PendaftarPassBln->NO_PEGAWAI }}</td>
                                        <td>{{ $POB->PendaftarPassBln->PEKERJAAN }}</td>
                                        <td>{{ $POB->PendaftarPassBln->INSTANSI}}</td>
                                        <td>{{ $POB->PendaftarPassBln->ALAMAT_KANTOR}}</td>
                                        <td>{{ $POB->PendaftarPassBln->NO_TELP_KANTOR}}</td>
                                        <td>

                                            <a href="{{ route('passorang.edit', [$POB->SERI_PASS_ORG_BLN, $POB->ID_PENDAFTAR]) }}"
                                               class="btn btn-xs btn-primary edit_button fa fa-edit" data-toggle="modal"
                                               data-id="{{$POB->ID_PENDAFTAR}}"
                                               data-nama="{{$POB->PendaftarPassBln->NAMA_PENDAFTAR }}"
                                               data-alamatrmh="{{$POB->PendaftarPassBln->ALAMAT_RUMAH}}"
                                               data-telprmh="{{$POB->PendaftarPassBln->NO_TELP}}"
                                               data-nopeg="{{$POB->PendaftarPassBln->NO_PEGAWAI}}"
                                               data-pkrjn="{{$POB->PendaftarPassBln->PEKERJAAN}}"
                                               data-instansi="{{$POB->PendaftarPassBln->INSTANSI}}"
                                               data-alamatkntr="{{$POB->PendaftarPassBln->ALAMAT_KANTOR}}"
                                               data-telpkntr="{{$POB->PendaftarPassBln->NO_TELP_KANTOR}}"

                                               data-identitas="{{$POB->PendaftarPassBln->NO_IDENTITAS}}"
                                               data-tmptlhr="{{$POB->PendaftarPassBln->TEMPAT_LAHIR}}"
                                               data-tgllhr="{{$POB->PendaftarPassBln->TGL_LAHIR}}"
                                               data-agama="{{$POB->PendaftarPassBln->AGAMA}}"
                                               data-status="{{$POB->PendaftarPassBln->STATUS_KERJA}}"
                                               data-masa="{{$POB->PendaftarPassBln->MASA_KERJA}}"
                                               data-tmptkerjasblm="{{$POB->PendaftarPassBln->TEMPAT_KERJA_SBLMNYA}}"

                                               data-periode="Bulanan"
                                               data-tglmulai="{{$POB->TGL_MULAI_ORG_BLN}}"
                                               data-tglselesai="{{$POB->TGL_SELESAI_ORG_BLN}}"

                                               data-target="#modaledit"></a>
                                            <a href="{{ route('passorang.delete', [$POB->SERI_PASS_ORG_BLN, $POB->ID_PENDAFTAR,'Bulanan']) }}"
                                               class="fa fa-trash btn btn-danger btn-xs" data-toggle="confirmation"
                                               data-title="Hapus data pendaftar?"></a>

                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                </tfoot>
                            </table>
                            <label for="title">Periode Tahunan</label>
                            <table id="pendaftartahun" class="table table-bordered table-striped">
                                <thead>
                                <th>ID Pass</th>
                                <th>Nama</th>
                                <th>Alamat Rumah</th>
                                <th>Nomor Telepon</th>
                                <th>Nomor Pegawai</th>
                                <th>Pekerjaan</th>
                                <th>Instansi</th>
                                <th>Alamat Kantor</th>
                                <th>Nomor Telepon Kantor</th>
                                <th>Aksi</th>

                                </thead>
                                <tbody>
                                <?php $PassOrangTahun = \App\PassOrangTahunModel::where('ID_TRANSAKSI', $getmaxtrans)->get();?>
                                @foreach($PassOrangTahun as $POT)
                                    <tr>
                                        <td>{{ $POT->SERI_PASS_ORG_THN }}</td>
                                        <td>{{ $POT->PendaftarPassThn->NAMA_PENDAFTAR }}</td>
                                        <td>{{ $POT->PendaftarPassThn->ALAMAT_RUMAH}}</td>
                                        <td>{{ $POT->PendaftarPassThn->NO_TELP}}</td>
                                        <td>{{ $POT->PendaftarPassThn->NO_PEGAWAI }}</td>
                                        <td>{{ $POT->PendaftarPassThn->PEKERJAAN }}</td>
                                        <td>{{ $POT->PendaftarPassThn->INSTANSI}}</td>
                                        <td>{{ $POT->PendaftarPassThn->ALAMAT_KANTOR}}</td>
                                        <td>{{ $POT->PendaftarPassThn->NO_TELP_KANTOR}}</td>
                                        <td>

                                            <a href="{{ route('passorang.edit', [$POT->SERI_PASS_ORG_THN, $POT->ID_PENDAFTAR]) }}"
                                               class="btn btn-xs btn-primary edit_button fa fa-edit" data-toggle="modal"
                                               data-id="{{$POT->ID_PENDAFTAR}}"
                                               data-nama="{{$POT->PendaftarPassThn->NAMA_PENDAFTAR }}"
                                               data-alamatrmh="{{$POT->PendaftarPassThn->ALAMAT_RUMAH}}"
                                               data-telprmh="{{$POT->PendaftarPassThn->NO_TELP}}"
                                               data-nopeg="{{$POT->PendaftarPassThn->NO_PEGAWAI}}"
                                               data-pkrjn="{{$POT->PendaftarPassThn->PEKERJAAN}}"
                                               data-instansi="{{$POT->PendaftarPassThn->INSTANSI}}"
                                               data-alamatkntr="{{$POT->PendaftarPassThn->ALAMAT_KANTOR}}"
                                               data-telpkntr="{{$POT->PendaftarPassThn->NO_TELP_KANTOR}}"

                                               data-identitas="{{$POT->PendaftarPassThn->NO_IDENTITAS}}"
                                               data-tmptlhr="{{$POT->PendaftarPassThn->TEMPAT_LAHIR}}"
                                               data-tgllhr="{{$POT->PendaftarPassThn->TGL_LAHIR}}"
                                               data-agama="{{$POT->PendaftarPassThn->AGAMA}}"
                                               data-status="{{$POT->PendaftarPassThn->STATUS_KERJA}}"
                                               data-masa="{{$POT->PendaftarPassThn->MASA_KERJA}}"
                                               data-tmptkerjasblm="{{$POT->PendaftarPassThn->TEMPAT_KERJA_SBLMNYA}}"

                                               data-periode="Tahunan"
                                               data-tglmulai="{{$POT->TGL_MULAI_ORG_THN}}"
                                               data-tglselesai="{{$POT->TGL_SELESAI_ORG_THN}}"

                                               data-target="#modaledit"></a>
                                            <a href="{{ route('passorang.delete', [$POT->SERI_PASS_ORG_THN, $POT->ID_PENDAFTAR,'Tahunan']) }}"
                                               class="fa fa-trash btn btn-danger btn-xs" data-toggle="confirmation"
                                               data-title="Hapus data pendaftar?"></a>

                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                </tfoot>
                            </table>
                            <button type="button" class="btn btn-success btn-xs  fa fa-plus" data-toggle="modal"
                                    data-target="#myModal"> Tambah Pass
                            </button>
                        </div>
                    </div>
                    <!-- /.tab-pane -->


                    <div class="tab-pane" id="kendaraan">
                        <div class="tab-content">
                            <label for="title">Periode Bulanan</label>
                            <?php
                            $kendaraan = \App\PassKendBulanModel::where('ID_TRANSAKSI', $getmaxtrans)->get();
                            $kendaraantahun = \App\PassKendTahunModel::where('ID_TRANSAKSI', $getmaxtrans)->get();?>

                            <table id="kendaraanbulan" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID Pass</th>
                                    <th>Nama Kendaraan</th>
                                    <th>Jenis Kendaraan</th>
                                    <th>Nomor Polisi</th>
                                    <th>Warna Plat</th>
                                    <th>Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($kendaraan as $kend)
                                    <tr>
                                        <td>{{ $kend->SERI_PASS_KEND_BLN}}</td>
                                        <td>{{ $kend->KendaraanPassBln->NAMA_KENDARAAN}}</td>
                                        <td>{{ $kend->KendaraanPassBln->JenisKend->NAMA_JENIS}}</td>
                                        <td>{{ $kend->KendaraanPassBln->NO_POLISI}}</td>
                                        <td>{{ $kend->KendaraanPassBln->WARNA_PLAT}}</td>
                                        <td>
                                            <a href="{{ route('kendaraan.edit', $kend->ID_KENDARAAN) }}"
                                               class="btn btn-xs btn-primary edit_button fa  fa-edit edit_kend"
                                               data-toggle="modal"
                                               data-idkend="{{$kend->ID_KENDARAAN}}"
                                               data-namakend="{{$kend->KendaraanPassBln->NAMA_KENDARAAN}}"
                                               data-jeniskend="{{$kend->ID_JENIS_KEND}}"
                                               data-nopol="{{$kend->KendaraanPassBln->NO_POLISI}}"
                                               data-warna="{{$kend->KendaraanPassBln->WARNA_PLAT}}"

                                               data-target="#ModalEditKend"></a>
                                            <a href="{{ route('passkendaraan.delete', [$kend->SERI_PASS_KEND_BLN, $kend->KendaraanPassBln->ID_KENDARAAN,'Bulanan']) }}"
                                               class="fa fa-trash btn btn-danger btn-xs" data-toggle="confirmation"
                                               data-title="Hapus data kendaraan?"></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>


                            <label for="title">Periode Tahunan</label>
                            <table id="kendaraantahun" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID Pass</th>
                                    <th>Nama Kendaraan</th>
                                    <th>Jenis Kendaraan</th>
                                    <th>Nomor Polisi</th>
                                    <th>Warna Plat</th>
                                    <th>Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($kendaraantahun as $kendthn)
                                    <tr>
                                        <td>{{ $kendthn->SERI_PASS_KEND_THN}}</td>
                                        <td>{{ $kendthn->KendaraanPassThn->NAMA_KENDARAAN}}</td>
                                        <td>{{ $kendthn->KendaraanPassThn->JenisKend->NAMA_JENIS}}</td>
                                        <td>{{ $kendthn->KendaraanPassThn->NO_POLISI}}</td>
                                        <td>{{ $kendthn->KendaraanPassThn->WARNA_PLAT}}</td>
                                        <td>
                                            <a href="{{ route('kendaraan.edit', $kend->ID_KENDARAAN) }}"
                                               class="btn btn-xs btn-primary edit_button fa  fa-edit edit_kend"
                                               data-toggle="modal"
                                               data-idkend="{{$kend->ID_KENDARAAN}}"
                                               data-namakend="{{$kend->NAMA_KENDARAAN}}"
                                               data-jeniskend="{{$kend->ID_JENIS_KEND}}"
                                               data-nopol="{{$kend->NO_POLISI}}"
                                               data-warna="{{$kend->WARNA_PLAT}}"

                                               data-target="#ModalEditKend"></a>
                                            <a href="{{ route('passkendaraan.delete', [$kend->SERI_PASS_KEND_BLN, $kend->KendaraanPassBln->ID_KENDARAAN,'Tahunan']) }}"
                                               class="fa fa-trash btn btn-danger btn-xs" data-toggle="confirmation"
                                               data-title="Hapus data kendaraan?"></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <button type="button" class="btn btn-success btn-xs  fa fa-plus" data-toggle="modal"
                                    data-target="#ModalKend"> Tambah Pass
                            </button>

                        </div>

                    </div>

                </div>

                <!-- /.tab-pane -->

            </div>
            <!-- /.tab-content -->

        </div>
        <!-- /.nav-tabs-custom -->

    </div>
    </div>

    <!-- /.box-body -->
    </div>

    <!-- /.box -->
    </div>


    <!-- /.col -->
    </div>
    <!-- /.row -->

    <!--/.Modal-->
    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Data Pendaftar</h4>
                </div>
                <div class="modal-body">
                    <form action="{{ route('passorang.store') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <?php
                        $getmax = DB::table('data_pendaftar')->max('ID_PENDAFTAR');
                        $max = substr($getmax, 3);
                        $next = $max + 1;
                        $id = "PD-" . $next;
                        ?>

                        <input type="hidden" name="ID_PENDAFTAR" value="<?= $id ?>">
                        <input type="hidden" name="ID_TRANSAKSI" value="<?= $getmaxtrans ?>">
                        <div class="form-group">
                            <label for="title">ID Transaksi:</label>
                            <input type="text" id="idtrans" name="ID_TRANSAKSI" class="form-control"
                                   value="{{$getmaxtrans}}" required disabled>
                        </div>
                        <div class="form-group">
                            <label for="title">ID Pendaftar:</label>
                            <input type="text" class="form-control" value="{{$id}}" required disabled>
                        </div>
                        <div class="form-group">
                            <label for="title">Nama Pendaftar</label>
                            <input type="text" name="NAMA_PENDAFTAR" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="title">Nomor Identitas (KTP)</label>
                            <input type="number" name="NO_IDENTITAS" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="title">Tempat Lahir</label>
                            <input type="text" name="TEMPAT_LAHIR" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="title">Tanggal Lahir</label>
                            <input type="date" name="TGL_LAHIR" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="title">Agama</label>
                            <select class="form-control" name="AGAMA">
                                <option value="Islam">Islam</option>
                                <option value="Katholik">Katholik</option>
                                <option value="Kristen">Kristen</option>
                                <option value="Hindu">Hindu</option>
                                <option value="Buddha">Buddha</option>
                                <option value="Lain-lain">Lain-lain</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="title">Alamat Rumah</label>
                            <input type="text" name="ALAMAT_RUMAH" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="title">Nomor Telepon</label>
                            <input type="number" name="NO_TELP" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="title">Instansi</label>
                            <input type="text" name="INSTANSI" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="title">Alamat Kantor</label>
                            <input type="text" name="ALAMAT_KANTOR" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="title">Nomor Telepon Kantor</label>
                            <input type="number" name="NO_TELP_KANTOR" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="title">Nomor Pegawai</label>
                            <input type="text" name="NO_PEGAWAI" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="title">Pekerjaan</label>
                            <input type="text" name="PEKERJAAN" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="title">Status Pekerjaan</label>
                            <select class="form-control" name="STATUS_KERJA">
                                <option value="Tetap">Tetap</option>
                                <option value="Kontrak">Kontrak</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="title">Masa Kerja</label>
                            <input type="text" name="MASA_KERJA" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="title">Tempat Kerja Sebelumnya</label>
                            <input type="text" name="TEMPAT_KERJA_SBLMNYA" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="title">Periode</label>
                            <select class="form-control" name="periode">
                                <option value="Bulanan">Bulanan</option>
                                <option value="Tahunan">Tahunan</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="title">Tanggal Mulai</label>
                            <input type="date" name="TGL_MULAI" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="title">Tanggal Selesai</label>
                            <input type="date" name="TGL_SELESAI" class="form-control" required>
                        </div>
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">
                            <i class="glyphicon glyphicon-floppy-saved"></i>Simpan
                        </button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i
                                    class="glyphicon glyphicon-floppy-remove"></i> Batal
                        </button>
                        </button>
                    </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

    <div class="modal fade" id="ModalKend" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Data Kendaraan</h4>
                </div>
                <div class="modal-body">
                    <form action="{{ route('passkendaraan.store') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <?php
                        $getmax = DB::table('data_kendaraan')->max('ID_KENDARAAN');
                        $max = substr($getmax, 3);
                        $next = $max + 1;
                        $id = "KD-" . $next;
                        ?>

                        <input type="hidden" name="ID_KENDARAAN" value="<?= $id ?>">
                        <input type="hidden" name="ID_TRANSAKSI" value="<?= $getmaxtrans ?>">
                        <div class="form-group">
                            <label for="title">ID Transaksi:</label>
                            <input type="text" id="idtrans" name="ID_TRANSAKSI" class="form-control"
                                   value="{{$getmaxtrans}}" required disabled>
                        </div>
                        <div class="form-group">
                            <label for="title">ID Kendaraan:</label>
                            <input type="text" class="form-control" value="{{$id}}" required disabled>
                        </div>
                        <div class="form-group">
                            <label for="title">Nama Kendaraan</label>
                            <input type="text" name="NAMA_KENDARAAN" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="title">Nomor Polisi</label>
                            <input type="text" name="NO_POLISI" class="form-control" required>
                        </div>


                        <div class="form-group">
                            <label for="title">Jenis Kendaraan</label>
                            <select class="form-control" name="ID_JENIS_KEND">
                                <option value="JK-01">Mobil Airlines s/d 2 ton (APRON)</option>
                                <option value="JK-02">R4 / Mobil Mitra s/d 2 ton</option>
                                <option value="JK-03">ATN / T. Refuel < 10 ton</option>
                                <option value="JK-04">Tangki Refuel > 10 ton</option>
                                <option value="JK-05">BTT s/d > 2 ton</option>
                                <option value="JK-06">Roda 4 minibus / pelataran</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="title">Warna Plat</label>
                            <select class="form-control" name="WARNA_PLAT">
                                <option value="Merah">Merah</option>
                                <option value="Hitam">Hitam</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="title">Nama Pemegang Pass</label>
                            <input type="text" name="PEMEGANG" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="title">Periode</label>
                            <select class="form-control" name="periode">
                                <option value="Bulanan">Bulanan</option>
                                <option value="Tahunan">Tahunan</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="title">Tanggal Mulai</label>
                            <input type="date" name="TGL_MULAI" class="form-control"
                                   value="<?php echo date("Y/m/d"); ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="title">Tanggal Selesai</label>
                            <input type="date" name="TGL_SELESAI" class="form-control" required>
                        </div>

                        <div class="modal-footer">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success">
                                    <i class="glyphicon glyphicon-floppy-saved"></i>Simpan
                                </button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal"><i
                                            class="glyphicon glyphicon-floppy-remove"></i>Batal
                                </button>
                                </button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>



    <!-- Modal for Edit button -->
    <div class="modal fade" id="modaledit" tabindex="-1" role="dialog" aria- labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Edit Pendaftar</h4>
                </div>
                <div class="modal-body">
                    <form action="" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PATCH">
                        <input type="hidden" name="ID" class="id">
                        <div class="form-group">
                            <label for="title">ID Pendaftar:</label>
                            <input type="text" name="ID_PENDAFTAR" class="form-control id" required disabled>
                        </div>
                        <div class="form-group">
                            <label for="title">Nama Pendaftar</label>
                            <input type="text" name="NAMA_PENDAFTAR" class="form-control nama" required>
                        </div>
                        <div class="form-group">
                            <label for="title">Nomor Identitas (KTP)</label>
                            <input type="text" name="NO_IDENTITAS" class="form-control identitas" required>
                        </div>
                        <div class="form-group">
                            <label for="title">Tempat Lahir</label>
                            <input type="text" name="TEMPAT_LAHIR" class="form-control tmptlhr" required>
                        </div>
                        <div class="form-group">
                            <label for="title">Tanggal Lahir</label>
                            <input type="date" name="TGL_LAHIR" class="form-control tgllhr" required>
                        </div>
                        <div class="form-group">
                            <label for="title">Agama</label>
                            <select class="form-control agama" name="AGAMA">
                                <option value="Islam">Islam</option>
                                <option value="Katholik">Katholik</option>
                                <option value="Kristen">Kristen</option>
                                <option value="Hindu">Hindu</option>
                                <option value="Buddha">Buddha</option>
                                <option value="Lain-lain">Lain-lain</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="title">Alamat Rumah</label>
                            <input type="text" name="ALAMAT_RUMAH" class="form-control alamatrmh" required>
                        </div>
                        <div class="form-group">
                            <label for="title">Nomor Telepon</label>
                            <input type="text" name="NO_TELP" class="form-control telprmh" required>
                        </div>
                        <div class="form-group">
                            <label for="title">Instansi</label>
                            <input type="text" name="INSTANSI" class="form-control instansi" required>
                        </div>
                        <div class="form-group">
                            <label for="title">Alamat Kantor</label>
                            <input type="text" name="ALAMAT_KANTOR" class="form-control alamatkntr" required>
                        </div>
                        <div class="form-group">
                            <label for="title">Nomor Telepon Kantor</label>
                            <input type="text" name="NO_TELP_KANTOR" class="form-control telpkntr" required>
                        </div>
                        <div class="form-group">
                            <label for="title">Nomor Pegawai</label>
                            <input type="text" name="NO_PEGAWAI" class="form-control nopeg" required>
                        </div>
                        <div class="form-group">
                            <label for="title">Pekerjaan</label>
                            <input type="text" name="PEKERJAAN" class="form-control pkrjn" required>
                        </div>
                        <div class="form-group">
                            <label for="title">Status Pekerjaan</label>
                            <select class="form-control status" name="STATUS_KERJA">
                                <option value="Tetap">Tetap</option>
                                <option value="Kontrak">Kontrak</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="title">Masa Kerja</label>
                            <input type="text" name="MASA_KERJA" class="form-control masa" required>
                        </div>
                        <div class="form-group">
                            <label for="title">Tempat Kerja Sebelumnya</label>
                            <input type="text" name="TEMPAT_KERJA_SBLMNYA" class="form-control tmptkerjasblm">
                        </div>
                        <div class="form-group">
                            <label for="title">Periode</label>
                            <select class="form-control" name="periode">
                                <option value="Bulanan">Bulanan</option>
                                <option value="Tahunan">Tahunan</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="title">Tanggal Mulai</label>
                            <input type="date" name="TGL_MULAI" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="title">Tanggal Selesai</label>
                            <input type="date" name="TGL_SELESAI" class="form-control" required>
                        </div>
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-floppy-saved"></i>Simpan
                        </button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i
                                    class="glyphicon glyphicon-floppy-remove"></i> Batal
                        </button>
                        </button>
                    </div>
                    </form>
                </div>
                </form>
            </div>
        </div>
    </div>
    </div>


    <div class="modal fade" id="ModalEditKend" tabindex="-1" role="dialog" aria- labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Edit Kendaraan</h4>
                </div>
                <div class="modal-body">
                    <form action="{{ route('kendaraan.update') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PATCH">
                        <input type="hidden" name="ID" class="idkend">

                        <div class="form-group">
                            <label for="title">ID Kendaraan:</label>
                            <input type="text" name="ID_PENDAFTAR" class="form-control idkend" required disabled>
                        </div>
                        <div class="form-group">
                            <label for="title">Nama Kendaraan</label>
                            <input type="text" name="NAMA_KENDARAAN" class="form-control namakend" required>
                        </div>
                        <div class="form-group">
                            <label for="title">Nomor Polisi</label>
                            <input type="text" name="NO_POLISI" class="form-control nopol" required>
                        </div>

                        <div class="form-group">
                            <label for="title">Jenis Kendaraan</label>
                            <select class="form-control jeniskend" name="ID_JENIS_KEND">
                                <option value="JK-01">Mobil Airlines s/d 2 ton (APRON)</option>
                                <option value="JK-02">R4 / Mobil Mitra s/d 2 ton</option>
                                <option value="JK-03">ATN / T. Refuel < 10 ton</option>
                                <option value="JK-04">Tangki Refuel > 10 ton</option>
                                <option value="JK-05">BTT s/d > 2 ton</option>
                                <option value="JK-06">Roda 4 minibus / pelataran</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="title">Warna Plat</label>
                            <select class="form-control warna" name="WARNA_PLAT">
                                <option value="Merah">Merah</option>
                                <option value="Hitam">Hitam</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="title">Periode</label>
                            <select class="form-control" name="periode">
                                <option value="Bulanan">Bulanan</option>
                                <option value="Tahunan">Tahunan</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="title">Tanggal Mulai</label>
                            <input type="date" name="TGL_MULAI" class="form-control"
                                   value="<?php echo date("Y/m/d"); ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="title">Tanggal Selesai</label>
                            <input type="date" name="TGL_SELESAI" class="form-control" required>
                        </div>

                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-floppy-saved"></i>Simpan
                        </button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i
                                    class="glyphicon glyphicon-floppy-remove"></i> Batal
                        </button>
                        </button>
                    </div>
                    </form>
                </div>
                </form>
            </div>
        </div>
    </div>
    </div>
    </div>

@endsection
@section('custom_script')
    <!-- jQuery 2.2.3 -->
    <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <!-- page script -->
    <script>

        $(function () {
            $("#kendaraanbulan").DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": false,
                "autoWidth": true
            });
            $("#kendaraantahun").DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": false,
                "autoWidth": true
            });
            $("#pendaftarbulan").DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": false,
                "autoWidth": true
            });
            $("#pendaftartahun").DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": false,
                "autoWidth": true
            });
        });
    </script>
    </body>










    <script type="text/javascript">

        $(document).on("click", '.edit_button', function (e) {

            var id = $(this).data('id');
            $(".id").val(id);
            var nama = $(this).data('nama');
            $(".nama").val(nama);

            var identitas = $(this).data('identitas');
            $(".identitas").val(identitas);

            var alamatrmh = $(this).data('alamatrmh');
            $(".alamatrmh").val(alamatrmh);
            var telprmh = $(this).data('telprmh');
            $(".telprmh").val(telprmh);
            var nopeg = $(this).data('nopeg');
            $(".nopeg").val(nopeg);
            var pkrjn = $(this).data('pkrjn');
            $(".pkrjn").val(pkrjn);
            var instansi = $(this).data('instansi');
            $(".instansi").val(instansi);
            var alamatkntr = $(this).data('alamatkntr');
            $(".alamatkntr").val(alamatkntr);
            var telpkntr = $(this).data('telpkntr');
            $(".telpkntr").val(telpkntr);
            var nopeg = $(this).data('nopeg');
            $(".nopeg").val(nopeg);
            var tmptlhr = $(this).data('tmptlhr');
            $(".tmptlhr").val(tmptlhr);

            var tgllhr = $(this).data('tgllhr');
            $(".tgllhr").val(tgllhr);

            var masa = $(this).data('masa');
            $(".masa").val(masa);

            var tmptkerjasblm = $(this).data('tmptkerjasblm');
            $(".tmptkerjasblm").val(tmptkerjasblm);


            var status = $(this).data('status');
            if (status == 'Tetap')
                $(".status option[value=Tetap]").attr('selected', 'selected');
            else
                $(".status option[value=Kontrak]").attr('selected', 'selected');

            var agama = $(this).data('agama');
            if (agama == 'Islam')
                $(".agama option[value=Islam]").attr('selected', 'selected');
            else if (agama == 'Katholik')
                $(".agama option[value=Katholik]").attr('selected', 'selected');
            else if (status == 'Kristen')
                $(".agama option[value=Kristen]").attr('selected', 'selected');
            else if (status == 'Hindu')
                $(".agama option[value=Hindu]").attr('selected', 'selected');
            else if (status == 'Buddha')
                $(".agama option[value=Buddha]").attr('selected', 'selected');
            else
                $(".agama option[value=Lain-lain]").attr('selected', 'selected');


        });
        $(document).on("click", '.edit_kend', function (e) {

            var idkend = $(this).data('idkend');
            $(".idkend").val(idkend);
            var namakend = $(this).data('namakend');
            $(".namakend").val(namakend);

            var nopol = $(this).data('nopol');
            $(".nopol").val(nopol);


            var warna = $(this).data('warna');
            if (warna == 'Hitam')
                $(".warna option[value=Hitam]").attr('selected', 'selected');
            else
                $(".warna option[value=Merah]").attr('selected', 'selected');

            var jeniskend = $(this).data('jeniskend');
            if (jeniskend == 'JK-01')
                $(".jeniskend option[value=JK-01]").attr('selected', 'selected');
            else if (jeniskend == 'JK-02')
                $(".jeniskend option[value=JK-02]").attr('selected', 'selected');
            else if (jeniskend == 'JK-03')
                $(".jeniskend option[value=JK-03]").attr('selected', 'selected');
            else if (jeniskend == 'JK-04')
                $(".jeniskend option[value=JK-04]").attr('selected', 'selected');
            else if (jeniskend == 'JK-05')
                $(".jeniskend option[value=JK-05]").attr('selected', 'selected');
            else
                $(".jeniskend option[value=JK-06]").attr('selected', 'selected');

        });

        }
    </script>

@endsection
