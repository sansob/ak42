<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>SIPPABAN</title>
    <link href="{{URL::asset('front/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('front/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('front/css/prettyPhoto.css')}}" rel="stylesheet">
    <link href="{{URL::asset('front/css/main.css')}}" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="images/ico/favicon.ico">

</head><!--/head-->

<body data-spy="scroll" data-target="#navbar" data-offset="0">
<header id="header" role="banner">
    <div class="container">
        <div id="navbar" class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navbar-brand"><img src="front/images/logoap1.png"></div>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#main-slider"><i class="icon-home"></i></a></li>
                    <li><a href="#menuakses">Menu Akses</a></li>
                    <li><a href="#contact">Kontak</a></li>
                </ul>
            </div>
        </div>
    </div>
</header><!--/#header-->

<section id="main-slider" class="carousel">
    <div class="carousel-inner">
        <div class="item active">
            <div class="container">
                <div class="carousel-content">
                    <h1>Selamat Datang</h1>
                    <p class="lead">di Bandara Ahmad Yani Semarang</p>
                </div>
            </div>
        </div><!--/.item-->
        <div class="item">
            <div class="container">
                <div class="carousel-content">
                    <h1>SIPPABAN</h1>
                    <p class="lead">Sistem Informasi Pembuatan Pass Bandara</p>
                </div>
            </div>
        </div><!--/.item-->
    </div><!--/.carousel-inner-->
    <a class="prev" href="#main-slider" data-slide="prev"><i class="icon-angle-left"></i></a>
    <a class="next" href="#main-slider" data-slide="next"><i class="icon-angle-right"></i></a>
</section><!--/#main-slider-->

<section id="menuakses">
    <div class="container">
        <div class="box">
            <div class="center gap">
                <h2>Menu Akses</h2>
                <p class="lead">Pada sistem informasi ini, terdapat menu sebagai berikut:</p>
            </div><!--/.center-->
            <ul class="portfolio-items col-4">
                <li class="portfolio-item apps">
                    <div class="item-inner">
                        <div class="portfolio-image">
                            <img src="front/images/portfolio/thumb/pass.jpg" alt="">
                        </div>
                        <a href="{{route('transaksi.index2')}}"><h5>Pendaftaran Pass</h5></a>
                    </div>
                </li><!--/.portfolio-item-->
                <li class="portfolio-item apps">
                    <div class="item-inner">
                        <div class="portfolio-image">
                            <img src="front/images/portfolio/thumb/admin.jpg" alt="">
                        </div>
                        <a href="{{route('login')}}"><h5>Pelayanan Administrasi</h5></a>
                    </div>
                </li>
                <li class="portfolio-item apps">
                    <div class="item-inner">
                        <div class="portfolio-image">
                            <img src="front/images/portfolio/thumb/security.jpg" alt="">
                        </div>
                        <a href="{{route('login')}}"><h5>Airport Security</h5></a>
                    </div>
                </li>
                <li class="portfolio-item apps">
                    <div class="item-inner">
                        <div class="portfolio-image">
                            <img src="front/images/portfolio/thumb/sales.jpg" alt="">
                        </div>
                        <a href="{{route('login')}}"><h5>Sales</h5></a>
                    </div>
                </li>
            </ul>
        </div><!--/.box-->
    </div><!--/.container-->
</section><!--/#portfolio-->


<section id="contact">
    <div class="container">
        <div class="box last">
            <div class="row">
                <div class="col-sm-12">
                    <h1>Alamat Kami</h1>
                    <div class="row">
                        <div class="col-md-12">
                            <address>
                                <strong>Achmad Yani International Airport</strong><br>
                                Jalan Puad Ahmad Yani<br>
                                Semarang 50145<br>
                                Indonesia<br>
                                <abbr title="Telepon">Telepon:</abbr> +62 24 7607596<br>
                                <abbr title="Fax">Fax:</abbr> +62 24 7603506<br>
                                <abbr title="Email">Email:</abbr> humas.srg@ap1.co.id<br>
                            </address>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h1>Hubungi Kami</h1>
                            <ul class="social">
                                <li><a href="https://web.facebook.com/ahmadyani.airport?_rdr"><i
                                                class="icon-facebook icon-social"></i> Facebook</a></li>
                                <li><a href="https://plus.google.com/"><i class="icon-google-plus icon-social"></i>
                                        Google Plus</a></li>
                                <li><a href="https://twitter.com/srg_ap1"><i class="icon-twitter icon-social"></i>
                                        Twitter</a></li>
                                <li><a href="https://www.youtube.com/channel/UCVCAyzX9Tl5VR6KExojRNqA"><i
                                                class="icon-youtube icon-social"></i> Youtube</a></li>
                            </ul>
                        </div>
                    </div>

                </div>

            </div><!--/.col-sm-6-->
        </div><!--/.row-->
    </div><!--/.box-->
    </div><!--/.container-->
</section><!--/#contact-->

<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                &copy; 2013 <a target="_blank" href="http://shapebootstrap.net/"
                               title="Free Twitter Bootstrap WordPress Themes and HTML templates">ShapeBootstrap</a>.
                All Rights Reserved.
            </div>
            <div class="col-sm-6">
                <img class="pull-right" src="front/images/shapebootstrap.png" alt="ShapeBootstrap"
                     title="ShapeBootstrap">
            </div>
        </div>
    </div>
</footer><!--/#footer-->

<script src="{{URL::asset('front/js/jquery.js')}}"></script>
<script src="{{URL::asset('front/js/bootstrap.min.js')}}"></script>
<script src="{{URL::asset('front/js/jquery.isotope.min.js')}}"></script>
<script src="{{URL::asset('front/js/jquery.prettyPhoto.js')}}"></script>
<script src="{{URL::asset('front/js/main.js')}}"></script>
</body>
</html>
