<?php
/**
 * Created by PhpStorm.
 * User: sansob
 * Date: 18/02/18
 * Time: 20.48
 */
?>

@extends('form-front.template')
@section('content')

    <div class="row">
        <div class="form-box">
            <form role="form" action="{{route('front.security.store')}}" method="POST" enctype="multipart/form-data"
                  class="f1">

                <h3>Security Form</h3>
                <p>Data Keluarga</p>
                <div class="alert alert-danger" role="alert">Pastikan semua data diisi. Isikan "-" Jika ingin
                    mengosongkan data
                </div>
                <div class="f1-steps">
                    <div class="f1-progress">
                        <div class="f1-progress-line" data-now-value="16.66" data-number-of-steps="4"
                             style="width: 16.66%;"></div>
                    </div>
                    <div class="f1-step active">
                        <div class="f1-step-icon"><i class="fa fa-key"></i></div>
                        <p>Data Keluarga </p>
                    </div>
                    <div class="f1-step">
                        <div class="f1-step-icon"><i class="fa fa-user"></i></div>
                        <p>Data Pendidikan SD</p>
                    </div>
                    <div class="f1-step">
                        <div class="f1-step-icon"><i class="fa fa-user"></i></div>
                        <p>Data Pendidikan SMP</p>
                    </div>
                    <div class="f1-step">
                        <div class="f1-step-icon"><i class="fa fa-user"></i></div>
                        <p>Data Pendidikan SMA/SMK</p>
                    </div>
                    <div class="f1-step">
                        <div class="f1-step-icon"><i class="fa fa-user"></i></div>
                        <p>Data Pendidikan Sekolah Tinggi</p>
                    </div>

                </div>
                {{ csrf_field() }}
                <?php
                $kelg = $keluarga;
                $idpendaftar = $kelg['ID_PENDAFTAR'];
                $pend = $pendidikan;

                ?>
                <fieldset>

                    <input type="hidden" name="_method" value="PATCH">


                    <input type="hidden" name="ID_PENDAFTAR" placeholder="{{$idpendaftar}}"
                           value="{{$idpendaftar}}">
                    <input type="hidden" name="ID_DATA_KELUARGA" value="<?= $kelg['ID_DATA_KELUARGA'] ?>">

                    <h4>Nama Bapak</h4>
                    <div class="form-group">
                        <input type="text" name="NAMA_BAPAK"
                               class="f1-last-name form-control" id="NAMA_BAPAK">
                    </div>
                    <h4>Nama Ibu</h4>
                    <div class="form-group">
                        <input type="text" name="NAMA_IBU"
                               class="f1-last-name form-control" id="NAMA_IBU">
                    </div>


                    <h4>Alamat Orang tua</h4>
                    <div class="form-group">
                        <textarea name="ALAMAT_ORTU"
                                  class="f1-about-yourself form-control" id="ALAMAT_RUMAH"></textarea>
                    </div>

                    <h4>Nama Pasangan</h4>
                    <div class="form-group">
                        <input type="text" name="NAMA_PASANGAN"
                               class="f1-last-name form-control" id="NAMA_IBU">
                    </div>

                    <h4>Alamat Pasangan</h4>
                    <div class="form-group">
                        <textarea name="ALAMAT_PASANGAN"
                                  class="f1-about-yourself form-control" id="ALAMAT_RUMAH"></textarea>
                    </div>

                    <h4>Nomor Telepon Pasangan</h4>
                    <div class="form-group">
                        <input type="text" name="NO_TELP_PSG"
                               class="f1-last-name form-control" id="NAMA_IBU">
                    </div>

                    <div class="f1-buttons">
                        <button type="button" class="btn btn-next">Selanjutnya</button>
                    </div>
                </fieldset>

                <fieldset>
                    <input type="hidden" name="ID_DATA_PEND" value="<?= $pend['ID_DATA_PEND'] ?>">
                    <h4>Nama SD</h4>
                    <div class="form-group">
                        <input type="text" name="SD"
                               class="f1-last-name form-control" id="SD">
                    </div>
                    <h4>Alamat SD</h4>
                    <div class="form-group">
                        <textarea name="LOKASI_SD"
                                  class="f1-about-yourself form-control" id="LOKASI_SD"></textarea>
                    </div>
                    <h4>Tahun Lulus SD</h4>
                    <div class="form-group">
                        <div>
                            <select id="year" name="THN_LULUS_SD" class="form-control">
                                <option value="0">Pilih Tahun</option>
                                <script>

                                    var myDate = new Date();
                                    var year = myDate.getFullYear();
                                    for (var i = 1960; i < year + 1; i++) {
                                        document.write('<option value="' + i + '">' + i + '</option>');
                                    }
                                </script>
                            </select>
                        </div>
                    </div>


                    <div class="f1-buttons">
                        <button type="button" class="btn btn-previous">Sebelumnya</button>
                        <button type="button" class="btn btn-next">Selanjutnya</button>
                    </div>
                </fieldset>

                <fieldset>
                    <h4>Nama SMP</h4>
                    <div class="form-group">
                        <input type="text" name="SMP"
                               class="f1-last-name form-control" id="SD">
                    </div>
                    <h4>Alamat SMP</h4>
                    <div class="form-group">
                        <textarea name="LOKASI_SMP"
                                  class="f1-about-yourself form-control" id="LOKASI_SMP"></textarea>
                    </div>
                    <h4>Tahun Lulus SMP</h4>
                    <div class="form-group">
                        <div>
                            <select id="THN_LULUS_PEND_TINGGI" name="THN_LULUS_SMP" class="form-control">
                                <option value="0">Pilih Tahun</option>
                                <script>
                                    var myDate = new Date();
                                    var year = myDate.getFullYear();
                                    for (var i = 1960; i < year + 1; i++) {
                                        document.write('<option value="' + i + '">' + i + '</option>');
                                    }
                                </script>
                            </select>
                        </div>
                    </div>


                    <div class="f1-buttons">
                        <button type="button" class="btn btn-previous">Sebelumnya</button>
                        <button type="button" class="btn btn-next">Selanjutnya</button>
                    </div>
                </fieldset>

                <fieldset>
                    <h4>Nama SMA/SMK</h4>
                    <div class="form-group">
                        <input type="text" name="SMA"
                               class="f1-last-name form-control" id="SMA">
                    </div>
                    <h4>Alamat SMA/SMK</h4>
                    <div class="form-group">
                        <textarea name="LOKASI_SMA"
                                  class="f1-about-yourself form-control" id="LOKASI_SMA"></textarea>
                    </div>
                    <h4>Tahun Lulus SMA/SMK</h4>
                    <div class="form-group">
                        <div>
                            <select id="THN_LULUS_PEND_TINGGI" name="THN_LULUS_SMA" class="form-control">
                                <option value="0">Pilih Tahun</option>
                                <script>
                                    var myDate = new Date();
                                    var year = myDate.getFullYear();
                                    for (var i = 1960; i < year + 1; i++) {
                                        document.write('<option value="' + i + '">' + i + '</option>');
                                    }
                                </script>
                            </select>
                        </div>
                    </div>


                    <div class="f1-buttons">
                        <button type="button" class="btn btn-previous">Sebelumnya</button>
                        <button type="button" class="btn btn-next">Selanjutnya</button>
                    </div>
                </fieldset>

                <fieldset>
                    <h4>Nama Pendidikan Tinggi</h4>
                    <div class="form-group">
                        <input type="text" name="PEND_TINGGI"
                               class="f1-last-name form-control" id="PEND_TINGGI">
                    </div>
                    <h4>Alamat Pendidikan Tinggi</h4>
                    <div class="form-group">
                        <textarea name="LOKASI_PEND_TINGGI"
                                  class="f1-about-yourself form-control" id="LOKASI_PEND_TINGGI"></textarea>
                    </div>
                    <h4>Tahun Lulus Pendidikan Tinggi</h4>
                    <div class="form-group">
                        <div>
                            <select id="THN_LULUS_PEND_TINGGI" name="THN_LULUS_PEND_TINGGI" class="form-control">
                                <option value="0">Pilih Tahun</option>
                                <script>
                                    var myDate = new Date();
                                    var year = myDate.getFullYear();
                                    for (var i = 1960; i < year + 1; i++) {
                                        document.write('<option value="' + i + '">' + i + '</option>');
                                    }
                                </script>
                            </select>
                        </div>
                    </div>


                    <div class="f1-buttons">
                        <button type="button" class="btn btn-previous">Sebelumnya</button>
                        <button type="submit" class="btn btn-submit">Proses</button>
                    </div>
                </fieldset>


            </form>
        </div>
    </div>

@endsection
