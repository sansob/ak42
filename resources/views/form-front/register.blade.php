<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Airport Pass Wizard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <!-- App favicon -->
    <link rel="shortcut icon" href="{{asset('pass/images/favicon.ico')}}">

    <!--Form Wizard-->
    <link rel="stylesheet" type="text/css" href="{{asset('pass/plugins/jquery.steps/css/jquery.steps.css')}}"/>

    <!-- App css -->
    <link href="{{asset('pass/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('pass/css/icons.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('pass/css/style.css')}}" rel="stylesheet" type="text/css"/>

    <script src="{{asset('pass/js/modernizr.min.js')}}"></script>

</head>

<body>

<!-- Begin page -->
<div class="accountbg"
     style="background: url('https://ahmadyani-airport.com/frontend/uploads/defaults/9Xs5Jt20170308092941.jpg');background-size: cover;"></div>

<div class="wrapper-page account-page-full">

    <div class="card">
        <div class="card-block">

            <div class="account-box">

                <div class="card-box p-5">
                    <h2 class="text-uppercase text-center pb-4">
                        <a href="###" class="text-success">
                            <span><img src="https://ahmadyani-airport.com/frontend/uploads/defaults/F32O8A20170306111123.png"
                                       alt="" height="76"></span>
                        </a>
                    </h2>

                    <form class="form-horizontal" method="POST" action="{{route('front.registergo')}}">

                        {{ csrf_field() }}
                        <div class="form-group row m-b-20">
                            <div class="col-12">
                                <label for="username">ID Transaksi</label>
                                <input class="form-control" type="text" id="ID_TRANSAKSI" name="ID_TRANSAKSI"
                                       value="{{$idpendaftar}}" placeholder="" readonly>
                            </div>
                        </div>

                        <div class="form-group row m-b-20">
                            <div class="col-12">
                                <label for="username">Nama Pemohon</label>
                                <input class="form-control" type="text" id="NAMA_PEMOHON" name="NAMA_PEMOHON"
                                       required="" placeholder="Nama Pemohon">
                            </div>
                        </div>

                        <div class="form-group row m-b-20">
                            <div class="col-12">
                                <label for="username">Nomor Surat Pengajuan</label>
                                <input class="form-control" type="text" name="NO_SURAT_PEMOHON" id="NO_SURAT_PEMOHON"
                                       required="" placeholder="Nomor Surat Pengajuan">
                            </div>
                        </div>


                        <div class="form-group row text-center m-t-10">
                            <div class="col-12">
                                <button class="btn btn-block btn-custom waves-effect waves-light" type="submit">Proses
                                </button>
                            </div>
                        </div>

                    </form>

                    <div class="row m-t-50">
                        <div class="col-sm-12 text-center">
                            <p class="text-muted">Sudah pernah mendaftar Pass Bandara? <a
                                        href="{{route('front.status')}}" class="text-dark m-l-5"><b>Check Status Pass
                                        Bandara</b></a></p>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>


</div>


<!-- jQuery  -->
<script src="{{asset('pass/js/jquery.min.js')}}"></script>
<script src="{{asset('pass/js/popper.min.js')}}"></script>
<script src="{{asset('pass/js/bootstrap.min.js')}}"></script>
<script src="{{asset('pass/js/waves.js')}}"></script>
<script src="{{asset('pass/js/jquery.slimscroll.js')}}"></script>


<!--Form Wizard-->
<script src="{{asset('pass/plugins/jquery.steps/js/jquery.steps.min.js')}}"></script>

<!--wizard initialization-->
<script src="{{asset('pass/pages/jquery.wizard-init.js')}}"></script>

<!-- App js -->
<script src="{{asset('pass/js/jquery.core.js')}}"></script>
<script src="{{asset('pass/js/jquery.app.js')}}"></script>

</body>
</html>