<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Airport Pass Wizard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <!-- App favicon -->
    <link rel="shortcut icon" href="{{asset('pass/images/favicon.ico')}}">

    <!--Form Wizard-->
    <link rel="stylesheet" type="text/css" href="{{asset('pass/plugins/jquery.steps/css/jquery.steps.css')}}"/>

    <!-- App css -->
    <link href="{{asset('pass/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('pass/css/icons.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('pass/css/style.css')}}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@7.16.0/dist/sweetalert2.css"/>

    <script src="{{asset('pass/js/modernizr.min.js')}}"></script>


</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    <div class="topbar-main">
        <div class="container-fluid">

            <!-- Logo container-->
            <div class="logo">
                <!-- Text Logo -->
                <!-- <a href="index.html" class="logo">
                    <span class="logo-small"><i class="mdi mdi-radar"></i></span>
                    <span class="logo-large"><i class="mdi mdi-radar"></i> Highdmin</span>
                </a> -->
                <!-- Image Logo -->
                <a href="#" class="logo">
                    <img src="https://ahmadyani-airport.com/frontend/uploads/defaults/F32O8A20170306111123.png" alt=""
                         height="76" class="logo-small">
                    <img src="https://ahmadyani-airport.com/frontend/uploads/defaults/F32O8A20170306111123.png" alt=""
                         height="72" class="logo-large">
                </a>

            </div>
            <!-- End Logo container-->


            <div class="menu-extras topbar-custom">

                <ul class="list-unstyled topbar-right-menu float-right mb-0">

                    <li class="menu-item">


                        <a class="navbar-toggle nav-link">
                            <div class="lines">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </a>
                        <!-- End mobile menu toggle-->
                    </li>
                </ul>
            </div>
            <!-- end menu-extras -->

            <div class="clearfix"></div>

        </div> <!-- end container -->
    </div>
    <!-- end topbar-main -->

    <div class="navbar-custom">
        <div class="container-fluid">
            <div id="navigation">
                <!-- Navigation Menu-->
                <ul class="navigation-menu">

                    <li class="has-submenu">
                        <a href="{{route('front.index')}}"><i class="icon-speedometer"></i>Home</a>
                    </li>

                    <li class="has-submenu">
                        <a href="#"><i class="icon-layers"></i>Status Pass</a>
                    </li>

                </ul>
                <!-- End navigation menu -->
            </div> <!-- end #navigation -->
        </div> <!-- end container -->
    </div> <!-- end navbar-custom -->
</header>
<!-- End Navigation Bar-->


<div class="wrapper">
    <div class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">

                    </div>
                    <h4 class="page-title">>New Pass Wizard </h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->

        <!-- Basic Form Wizard -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">
                    <h4 class="m-t-0 header-title"><b>Form Pass Bandara Baru.</b></h4>

                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <strong>Holy guacamole!</strong>

                        * Dibutuhkan. Wajib di isi. <br>
                        Silakan isikan semua data, tekan "Finish" jika semua data selesai
                        diisikan. <br>
                        Form akan mengalihkan kembali untuk pengisian data orang ke dua dan seterusnya. Tap "Keluar"
                        untuk selesai dan keluar dari aplikasi.

                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="float-right">
                        <button type="button" class="btn btn-danger btn-lg waves-light waves-effect"
                                onclick="r = confirm('Apakah anda yakin untuk selesai dan keluar? Pastikan semua data telah tersubmit dengan klik FInish sebelum Keluar dari aplikasi');
                                if (r == true) {
                                    window.location.href='http://localhost:8000/front/index';
                                } else {
                                   //
                                }">Keluar
                        </button>
                    </div>
                    <p class="text-muted m-b-30 font-13">


                        <br>
                        <br>

                    </p>


                    @if ($errors->any())

                        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show"
                             role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </div>
                    @endif


                    <div class="pull-in">
                        <form id="basic-form" enctype="multipart/form-data" action="{{route('front.storepassperson')}}"
                              method="post"
                              onSubmit="if(!confirm('Apakah data sudah benar? Data yang sudah diinput tidak dapat di ubah!')){return false;}">
                            <input type="hidden" name="ID_TRANSAKSI"
                                   value="{{session("trans_id")}}"
                                   class="f1-first-name form-control" id="ID-TRANSAKSI">
                            <input type="hidden" name="ID_PENDAFTAR" value="{{$idpendaftar}}">


                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div>
                                <h3>Identitas</h3>
                                <section>
                                    <div class="form-group clearfix">
                                        <label class="control-label " for="NAMA_PENDAFTAR">Nama Pendaftar *</label>
                                        <div class="">
                                            <input class="form-control required" id="NAMA_PENDAFTAR"
                                                   name="NAMA_PENDAFTAR" value="{{ old('NAMA_PENDAFTAR') }}"
                                                   type="text" required>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <label class="control-label " for="NO_IDENTITAS">Nomor Identitas (KTP/SIM)
                                            *</label>
                                        <div class="">
                                            <input class="form-control required" id="NO_IDENTITAS" name="NO_IDENTITAS"
                                                   type="number" value="{{ old('NO_IDENTITAS') }}" required>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <label class="control-label " for="TEMPAT_LAHIR">Tempat Lahir *</label>
                                        <div class="">
                                            <input class="form-control required" id="TEMPAT_LAHIR" name="TEMPAT_LAHIR"
                                                   type="text" required value="{{ old('TEMPAT_LAHIR') }}">
                                        </div>
                                    </div>


                                    <div class="form-group clearfix">
                                        <label class="control-label " for="TGL_LAHIR">Tanggal Lahir
                                            * (HH/BB/TT)</label>
                                        <div class="">
                                            <input class="form-control required" id="TGL_LAHIR" name="TGL_LAHIR"
                                                   type="date" value="{{ old('TGL_LAHIR') }}" required>
                                        </div>
                                    </div>


                                    <div class="form-group clearfix">
                                        <label class="control-label " for="NO_TELP">Nomor Telepon *</label>
                                        <div class="">
                                            <input class="form-control required" id="NO_TELP" name="NO_TELP"
                                                   type="number" required value="{{ old('NO_TELP') }}">
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <label class="control-label " for="ALAMAT_RUMAH">Alamat Rumah *</label>
                                        <div class="">
                                            <textarea name="ALAMAT_RUMAH" required class="form-control"
                                                      rows="5">{{ old('ALAMAT_RUMAH') }}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <label class="control-label " for="AGAMA">Agama *</label>
                                        <div class="">
                                            <select class="form-control" name="AGAMA" id="AGAMA">
                                                <option value="-">Pilih Agama</option>
                                                <option value="Islam">Islam</option>
                                                <option value="Protestan">Protestan</option>
                                                <option value="Katholik">Katholik</option>
                                                <option value="Hindu">Hindu</option>
                                                <option value="Budha">Budha</option>
                                                <option value="Konghucu">Konghucu</option>
                                            </select>
                                        </div>
                                    </div>


                                </section>


                                <h3>Instansi</h3>
                                <section>
                                    <div class="form-group clearfix">
                                        <label class="control-label " for="INSTANSI">Instansi *</label>
                                        <div class="">
                                            <input class="form-control required" id="INSTANSI"
                                                   name="INSTANSI"
                                                   type="text" required value="{{ old('INSTANSI') }}">
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <label class="control-label " for="NO_TELP_KANTOR">Nomor Telepon Kantor
                                            *</label>
                                        <div class="">
                                            <input class="form-control required" id="NO_TELP_KANTOR"
                                                   name="NO_TELP_KANTOR" value="{{ old('NO_TELP_KANTOR') }}"
                                                   type="number" required>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <label class="control-label " for="ALAMAT_KANTOR">Alamat Kantor *</label>
                                        <div class="">
                                            <textarea name="ALAMAT_KANTOR" required class="form-control"
                                                      rows="5">{{ old('ALAMAT_KANTOR') }}</textarea>
                                        </div>
                                    </div>


                                </section>
                                <h3>Pekerjaan</h3>
                                <section>
                                    <div class="form-group clearfix">
                                        <label class="control-label " for="NO_PEGAWAI">Nomor Pegawai </label>
                                        <div class="">
                                            <input class="form-control required" id="NO_PEGAWAI"
                                                   name="NO_PEGAWAI" value="{{ old('NO_PEGAWAI') }}"
                                                   type="text">
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <label class="control-label " for="PEKERJAAN">Pekerjaan * </label>
                                        <div class="">
                                            <input class="form-control required" id="PEKERJAAN" required
                                                   name="PEKERJAAN" value="{{ old('PEKERJAAN') }}"
                                                   type="text">
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <label class="control-label " for="PEKERJAAN">Status Pekerjaan *</label>
                                        <div class="">
                                            <select class="form-control" name="STATUS_KERJA" id="STATUS_KERJA">
                                                <option value="Bulanan">Bulanan</option>
                                                <option value="Tahunan">Tahunan</option>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="form-group clearfix">
                                        <label class="control-label " for="Masa Kerja">Masa Kerja
                                            *</label>
                                        <div class="row">

                                            <div class="col-sm-1">
                                                <input class="form-control required" id="MASA_KERJA_tahun"
                                                       name="MASA_KERJA_tahun" value="0"
                                                       type="number" required>
                                            </div>
                                            <div class="col-sm-1">Tahun
                                            </div>
                                            <div class="col-md-1"><select name="MASA_KERJA_bulan" class="form-control"
                                                                          id="MASA_KERJA_bulan">
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                            <div class="col-md-1">Bulan</div>

                                        </div>

                                    </div>

                                    <div class="form-group clearfix">
                                        <label class="control-label " for="TEMPAT_KERJA_SBLMNYA">Tempat Kerja
                                            Sebelumnya</label>
                                        <div class="">
                                            <input class="form-control required" id="TEMPAT_KERJA_SBLMNYA"
                                                   name="TEMPAT_KERJA_SBLMNYA" value="{{ old('TEMPAT_KERJA_SBLMNYA') }}"
                                                   type="text">
                                        </div>
                                    </div>


                                </section>

                                <h3>Periode Pass & Berkas</h3>
                                <section>
                                    <div class="form-group clearfix">
                                        <label class="control-label " for="PERIODE">Periode Pass * </label>
                                        <div class="">
                                            <select class="form-control" name="PERIODE" id="PERIODE">
                                                <option value="Bulanan">Bulanan</option>
                                                <option value="Tahunan">Tahunan</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <label class="control-label " for="TGL_MULAI">Tanggal Mulai
                                            * (HH/BB/TT)</label>
                                        <div class="">
                                            <input class="form-control required" id="TGL_MULAI" name="TGL_MULAI"
                                                   type="date" value="{{ old('TGL_MULAI') }}" required>
                                        </div>
                                    </div>

                                    <div class="form-group clearfix">
                                        <label class="control-label " for="TGL_SELESAI">Tanggal Selesai
                                            * (HH/BB/TT)</label>
                                        <div class="">
                                            <input class="form-control required" id="TGL_SELESAI" name="TGL_SELESAI"
                                                   type="date" value="{{ old('TGL_SELESAI') }}" required>
                                        </div>
                                    </div>

                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group clearfix">
                                                    <label class="control-label " for="ktp">Unggah KTP</label>
                                                    <div class="">
                                                        <input class="default" id="ktp" name="ktp" type="file"
                                                               accept="application/pdf, image/*">
                                                        <p>*Support file jpg, png dan pdf</p>
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix">
                                                    <label class="control-label " for="ktp">Unggah NPWP
                                                        (Opsional)</label>
                                                    <div class="">
                                                        <input class="default" id="npwp" name="npwp" type="file"
                                                               accept="application/pdf, image/*">
                                                        <p>*Support file jpg, png dan pdf</p>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group clearfix">
                                                    <label class="control-label " for="ktp">Unggah SKCK</label>
                                                    <div class="">
                                                        <input class="default" id="skck" name="skck" type="file"
                                                               accept="application/pdf, image/*">
                                                        <p>*Support file jpg, png dan pdf</p>
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix">
                                                    <label class="control-label " for="ktp">Unggah Dokumen Lain
                                                        (Opsional)</label>
                                                    <div class="">
                                                        <input class="default" id="dokumen" name="dokumen" type="file"
                                                               accept="application/pdf, image/*">
                                                        <p>*Support file jpg, png dan pdf</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group clearfix">
                                                    <label class="control-label " for="cv">Unggah CV </label>
                                                    <div class="">
                                                        <input class="default" id="cv" name="cv" type="file"
                                                               accept="application/pdf, image/*">
                                                        <p>*Support file jpg, png dan pdf</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </section>


                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>


    </div> <!-- end container -->
</div>

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                2018 Angkasa Pura Airport
            </div>
        </div>
    </div>
</footer>


<script src="{{asset('pass/js/jquery.min.js')}}"></script>
<script src="{{asset('pass/js/popper.min.js')}}"></script>
<script src="{{asset('pass/js/bootstrap.min.js')}}"></script>
<script src="{{asset('pass/js/waves.js')}}"></script>
<script src="{{asset('pass/js/jquery.slimscroll.js')}}"></script>


<!--Form Wizard-->
<script src="{{asset('pass/plugins/jquery.steps/js/jquery.steps.min.js')}}"></script>

<!--wizard initialization-->
<script src="{{asset('pass/pages/jquery.wizard-init.js')}}"></script>

<!-- plugin js -->
<script src="{{asset('pass/plugins/moment/moment.js')}}"></script>


<!-- App js -->
<script src="{{asset('pass/js/jquery.core.js')}}"></script>
<script src="{{asset('pass/js/jquery.app.js')}}"></script>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@include('sweet::alert')


</body>
</html>