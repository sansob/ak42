<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Pass Baru</title>

    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="{{URL::asset('form-front/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('form-front/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('form-front/css/form-elements.css')}}">
    <link rel="stylesheet" href="{{URL::asset('form-front/css/style.css')}}">
    <link rel="stylesheet" href="{{URL::asset('form-front/css/default.css')}}">
    <link rel="stylesheet" href="{{URL::asset('form-front/css/default.date.css')}}">
    <script src="{{URL::asset('form-front/js/jquery.2.0.0.js')}}"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/2.27.5/css/uikit.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/2.27.5/css/components/datepicker.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/2.27.5/js/uikit.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/2.27.5/js/components/datepicker.min.js"></script>
    <script src="{{URL::asset('https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js')}}"
            defer></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css"/>

    <!-- Latest compiled and minified JavaScript -->
    <!-- (Optional) Latest compiled and minified JavaScript translation files -->


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Favicon and touch icons -->
    <link rel="shortcut icon" href="{{URL::asset('form-front/assets/ico/favicon.png')}}">


</head>

<body>
@include('sweet::alert')
<!-- Top content -->
<div class="top-content">
    <div class="container">

        @yield('content')

    </div>
</div>


<!-- Javascript -->

<script>
    var datepicker = UIkit.datepicker(element, {/* options */});
</script>

<script src="{{URL::asset('form-front/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{URL::asset('form-front/js/jquery.backstretch.min.js')}}"></script>
<script src="{{URL::asset('form-front/js/retina-1.1.0.min.js')}}"></script>
<script src="{{URL::asset('form-front/js/scripts.js')}}"></script>
<script src="{{URL::asset('form-front/js/placeholder.js')}}"></script>

<![endif]-->


</body>

</html>