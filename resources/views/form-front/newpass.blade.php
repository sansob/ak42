<?php
/**
 * Created by PhpStorm.
 * User: sansob
 * Date: 18/02/18
 * Time: 20.48
 */
?>

@extends('form-front.template')
@section('content')

    <div class="row">
        <div class="form-box">
            <form role="form" enctype="multipart/form-data" action="{{route('front.storepassperson')}}" method="post"
                  class="f1">

                <h3>PASS BARU</h3>
                <p>Formulir 1</p>
                <div class="f1-steps">
                    <div class="f1-progress">
                        <div class="f1-progress-line" data-now-value="16.66" data-number-of-steps="4"
                             style="width: 16.66%;"></div>
                    </div>
                    <div class="f1-step active">
                        <div class="f1-step-icon"><i class="fa fa-key"></i></div>
                        <p>Registrasi </p>
                    </div>
                    <div class="f1-step">
                        <div class="f1-step-icon"><i class="fa fa-user"></i></div>
                        <p>Data Diri</p>
                    </div>
                    <div class="f1-step">
                        <div class="f1-step-icon"><i class="fa fa-keyboard-o"></i></div>
                        <p>Instansi</p>
                    </div>
                    <div class="f1-step">
                        <div class="f1-step-icon"><i class="fa fa-briefcase"></i></div>
                        <p>Pekerjaan</p>
                    </div>
                    <div class="f1-step">
                        <div class="f1-step-icon"><i class="fa fa-file-pdf-o"></i></div>
                        <p>Berkas</p>
                    </div>

                </div>
                {{ csrf_field() }}
                <fieldset>

                    <div class="row">

                        <div class="col-md-11">
                            <div class="form-group">
                                <input type="hidden" name="ID_TRANSAKSI" placeholder="{{$randoms}}" readonly
                                       value="{{$randoms}}"
                                       class="f1-first-name form-control" id="ID-TRANSAKSI">
                                <input type="hidden" name="ID_PENDAFTAR" value="{{$idpendaftar}}">
                            </div>
                        </div>

                    </div>
                    <h4>Nomor Surat Permohonan</h4>
                    <div class="form-group">
                        <input type="text" name="NO_SURAT_PEMOHON"
                               class="f1-last-name form-control" id="no-surat">
                    </div>


                    <div class="f1-buttons">
                        <button type="button" class="btn btn-next">Selanjutnya</button>
                    </div>
                </fieldset>

                <fieldset>

                    <h4>Nomor Identitas</h4>
                    <div class="form-group">
                        <input type="number" name="NO_IDENTITAS"
                               class="f1-last-name form-control" id="NO_IDENTITAS" maxlength="16">
                    </div>
                    <h4>Nama Pemohon</h4>
                    <div class="form-group">
                        <input type="text" name="NAMA_PENDAFTAR" value="XXX"
                               class="f1-last-name form-control" id="NAMA_PENDAFTAR" maxlength="50">
                    </div>

                    <h4>Tempat Lahir</h4>
                    <div class="form-group">
                        <input type="text" name="TEMPAT_LAHIR" class="f1-email form-control"
                               id="TEMPAT_LAHIR" maxlength="25">
                    </div>
                    <h4>Tanggal Lahir</h4>
                    <div class="form-group">
                        <input type="text" name="TGL_LAHIR" id="TGL_LAHIR" data-uk-datepicker="{format:'YYYY/MM/DD'}">

                    </div>
                    <h4>Alamat Rumah</h4>
                    <div class="form-group">
                        <textarea name="ALAMAT_RUMAH"
                                  class="f1-about-yourself form-control" id="ALAMAT_RUMAH"></textarea>
                    </div>

                    <h4>No Telepon</h4>
                    <div class="form-group">
                        <input type="number" name="NO_TELP"
                               class="f1-last-name form-control" id="NO_TELP" maxlength="15">
                    </div>

                    <h4>Agama</h4>
                    <div class="form-group">
                        <select class="form-control" name="AGAMA" id="AGAMA">
                            <option value="Islam">Islam</option>
                            <option value="Protestan">Protestan</option>
                            <option value="Katholik">Katholik</option>
                            <option value="Hindu">Hindu</option>
                            <option value="Budha">Budha</option>
                        </select>

                    </div>


                    <div class="f1-buttons">
                        <button type="button" class="btn btn-previous">Sebelumnya</button>
                        <button type="button" class="btn btn-next">Selanjutnya</button>
                    </div>
                </fieldset>

                <fieldset>
                    <div class="form-group">
                        <h4>Nama Perusahaan</h4>
                        <div class="form-group">
                            <input type="text" name="INSTANSI"
                                   class="f1-password form-control" id="INSTANSI" maxlength="45">
                        </div>

                        <h4>Nomor Telepon Kantor</h4>
                        <div class="form-group">
                            <input type="number" name="NO_TELP_KANTOR"
                                   class="f1-password form-control" id="NO_TELP_KANTOR" maxlength="15">
                        </div>

                        <h4>Alamat Kantor</h4>
                        <div class="form-group">
                        <textarea name="ALAMAT_KANTOR"
                                  class="f1-about-yourself form-control" id="ALAMAT_KANTOR"></textarea>
                        </div>


                        <div class="f1-buttons">
                            <button type="button" class="btn btn-previous">Sebelumnya</button>
                            <button type="button" class="btn btn-next">Selanjutnya</button>
                        </div>

                    </div>


                </fieldset>
                <fieldset>

                    <h4>Nomor Pegawai</h4>
                    <div class="form-group">
                        <input type="text" name="NO_PEGAWAI"
                               class="f1-password form-control" id="NO_PEGAWAI" maxlength="50">
                    </div>
                    <h4>Pekerjaan</h4>
                    <div class="form-group">
                        <input type="text" name="PEKERJAAN"
                               class="f1-password form-control" id="PEKERJAAN" maxlength="30">
                    </div>
                    <h4>Status Pekerjaan</h4>
                    <div class="form-group">
                        <select class="form-control" name="STATUS_KERJA" id="STATUS_KERJA">
                            <option value="Tetap">Tetap</option>
                            <option value="Kontrak">Kontrak</option>
                        </select>

                    </div>
                    <h4>Masa Kerja</h4>

                    <div class="row">
                        <div class="col-md-1">
                            <div class="form-group">
                                <input type="text" name="MASA_KERJA_tahun"
                                       class="f1-password form-control" id="MASA_KERJA_tahun" placeholder="0">
                            </div>
                        </div>
                        <div class="col-md-1">Tahun</div>
                        <div class="col-md-1"><select name="MASA_KERJA_bulan" class="form-control"
                                                      id="MASA_KERJA_bulan">
                                <script>
                                    for (var i = 1; i <= 11; i++) {
                                        document.write('<option value="' + i + '">' + i + '</option>');
                                    }
                                </script>
                            </select></div>
                        <div class="col-md-1">Bulan</div>
                    </div>

                    <div class="form-group">
                        <h4>Tempat Kerja Sebelumnya</h4>
                        <div class="form-group">
                            <input type="text" name="TEMPAT_KERJA_SBLMNYA"
                                   class="f1-password form-control" id="TEMPAT_KERJA_SBLMNYA" maxlength="25">
                        </div>

                        <h4>Periode</h4>
                        <div class="form-group">
                            <select class="form-control" name="PERIODE" id="PERIODE">
                                <option value="Bulanan">Bulanan</option>
                                <option value="Tahunan">Tahunan</option>
                            </select>
                        </div>
                        <h4>Tanggal Mulai</h4>
                        <div class="form-group">
                            <input type="text" name="TGL_MULAI" id="TGL_MULAI"
                                   data-uk-datepicker="{format:'YYYY/MM/DD'}">
                        </div>
                        <h4>Tanggal Selesai</h4>
                        <div class="form-group">
                            <input type="text" name="TGL_SELESAI" id="TGL_SELESAI"
                                   data-uk-datepicker="{format:'YYYY/MM/DD'}">
                        </div>

                        <div class="f1-buttons">
                            <button type="button" class="btn btn-previous">Sebelumnya</button>
                            <button type="button" class="btn btn-next">Selanjutnya</button>
                        </div>


                    </div>


                </fieldset>

                <fieldset>
                    <h4>Unggah Scan KTP</h4>
                    <div class="form-group">
                        <input id="ktp" name="ktp" type="file">
                    </div>
                    <h4>Unggah Scan SKCK</h4>
                    <div class="form-group">
                        <input id="skck" name="skck" type="file">
                    </div>
                    <h4>Unggah Dokumen Perusahaan</h4>
                    <div class="form-group">
                        <input id="dokumen" name="dokumen" type="file">
                    </div>

                    <div class="f1-buttons">
                        <button type="button" class="btn btn-previous">Sebelumnya</button>
                        <button type="submit" class="btn btn-submit">Proses</button>
                    </div>

                </fieldset>
            </form>
        </div>
    </div>

@endsection
