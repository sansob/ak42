<?php
/**
 * Created by PhpStorm.
 * User: sansob
 * Date: 18/02/18
 * Time: 20.48
 */
?>

@extends('front.template')
@section('content')

    <section class="tm-margin-large-top">
        <div class="uk-container uk-text-center">

            <p class="uk-margin-remove-bottom tm-overtitle uk-margin-top uk-text-uppercase letter-spacing-l">Angkasa
                Pura
                1</p>
            <h1 class="uk-margin-small-top uk-heading-hero">AIRPORT PASS</h1>

            <div class="uk-grid-medium uk-child-width-1-3@m uk-grid-small uk-grid-match tm-margin-large-top uk-text-center"
                 data-uk-grid>
                <div>
                    <a href="{{route('front.register')}}" class="uk-card uk-card-box uk-card-body uk-border-rounded">
                    <span class="uk-icon-forder uk-border-rounded"><span
                                data-uk-icon="icon: plus; ratio: 2"></span></span>
                        <p>Pass Baru</p>
                    </a>
                </div>
                <div>
                    <a onclick="return false;" href="category.html" class="uk-card uk-card-box uk-card-body uk-border-rounded">
                    <span class="uk-icon-forder uk-border-rounded"><span
                                data-uk-icon="icon: calendar; ratio: 2"></span></span>
                        <p>Perpanjangan Pass</p>
                    </a>
                </div>
                <div>
                    <a href="{{route('front.status')}}" class="uk-card uk-card-box uk-card-body uk-border-rounded">
                    <span class="uk-icon-forder uk-border-rounded"><span
                                data-uk-icon="icon: check; ratio: 2"></span></span>
                        <p>Status Pass</p>
                    </a>
                </div>

            </div>

        </div>
    </section>

@endsection