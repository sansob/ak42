<?php
/**
 * Created by PhpStorm.
 * User: sansob
 * Date: 18/02/18
 * Time: 20.48
 */
?>

@extends('front.template')
@section('content')



    <section class="tm-margin-large-top">
        <div class="uk-container uk-text-center">

            <p class="uk-margin-remove-bottom tm-overtitle uk-margin-top uk-text-uppercase letter-spacing-l">Angkasa
                Pura
                1</p>
            <h1 class="uk-margin-small-top uk-heading-hero">Status PASS</h1>


            <form method="POST" action="{{route('front.status.cek')}}" class="uk-form-stacked uk-margin-xlarge-top uk-margin-xlarge-bottom tm-form-contact">
                {{ csrf_field() }}
                <label class="uk-form-label uk-margin-small-bottom" for="form-stacked-text">No Identitas</label>
                <div class="uk-form-controls">
                    <input id="ID_TRANSAKSI" class="uk-input uk-form-large tm-form-xlarge uk-border-rounded" name="ID_TRANSAKSI"
                           type="text" placeholder="Masukkan No KTP/SIM">
                </div>

                <br>
                <div class="uk-text-center">
                    <input class="tm-button tm-button-xlarge" id="form-submit" type="submit" name="send" value="Cek Status Pass">
                </div>
            </form>


        </div>
    </section>

@endsection