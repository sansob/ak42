<?php
/**
 * Created by PhpStorm.
 * User: sansob
 * Date: 18/02/18
 * Time: 20.29
 */
?>


        <!DOCTYPE html>
<html lang="en-gb" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Angkasa Pura Airport Support || Pass Creation</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,800%7CWork+Sans:200,300%7CJosefin+Sans:100"
          rel="stylesheet">
    <link rel="stylesheet" href="{{URL::asset('minimal/css/main.css')}}"/>
    <script src="{{URL::asset('minimal/js/uikit.min.js')}}" defer></script>
    <script src="{{URL::asset('minimal/js/uikit-icons.min.js')}}" defer></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@7.16.0/dist/sweetalert2.css"/>


    <style>

        #ID_TRANSAKSI {
            text-align: center;
            font-weight: bold;
            font-size: 50px;

        }
    </style>

</head>

<body class="uk-background-primary uk-light">


<div data-uk-sticky="sel-target: .uk-navbar-container; cls-active: uk-navbar-sticky">


    <nav class="uk-navbar-container uk-margin uk-light">
        <div class="uk-container">
            <div data-uk-navbar>
                <div class="uk-navbar-left">
                    <a class="uk-navbar-item uk-logo uk-text-small" href="index.html">AP1 PASS</a>
                </div>
                <div class="uk-navbar-center uk-visible@m">


                </div>
                <div class="uk-navbar-right">
                    <a class="uk-navbar-toggle uk-hidden@m" href="#offcanvas" data-uk-navbar-toggle-icon
                       data-uk-toggle></a>
                </div>
            </div>
        </div>
    </nav>


</div>


@yield('content')


<div class="uk-margin-large-top uk-section-primary">
    <div class="uk-container">
        <hr class="uk-margin-remove">
        <div class="uk-grid-large uk-flex-middle uk-margin-xlarge-top uk-margin-xlarge-bottom" data-uk-grid
             data-uk-scrollspy="cls: uk-animation-slide-bottom; repeat: true; delay: 100">
        </div>
    </div>
</div>


<footer class="uk-section tm-primary-color-dark uk-light uk-text-center">
    <div class="uk-container">

        <div class="uk-margin-medium">
            <div data-uk-grid class="uk-child-width-auto uk-grid-small uk-flex-center uk-grid">
                <div class="uk-first-column">
                    <a href="https://github.com/" data-uk-icon="icon: github" class="uk-icon-link uk-icon"></a>
                </div>
                <div>
                    <a href="https://twitter.com/" data-uk-icon="icon: twitter" class="uk-icon-link uk-icon"></a>
                </div>
                <div>
                    <a href="https://www.instagram.com/" data-uk-icon="icon: instagram"
                       class="uk-icon-link uk-icon"></a>
                </div>
                <div>
                    <a href="https://www.facebook.com/" data-uk-icon="icon: facebook" class="uk-icon-link uk-icon"></a>
                </div>
            </div>
        </div>
        <div class="uk-margin-medium tm-text-xsmall uk-text-meta copyright">Made by Hasan Shobri <a
                    href="https://qampus.us/">& Qampus</a>
        </div>

    </div>
</footer>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@include('sweet::alert')
</body>

</html>
