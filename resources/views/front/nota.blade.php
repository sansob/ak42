<html>

<head>
    <meta name=Title content="">
    <meta name=Keywords content="">
    <meta http-equiv=Content-Type content="text/html; charset=utf-8">
    <meta name=Generator content="Microsoft Word 15 (filtered)">
    <style>
        <!--
        /* Font Definitions */
        @font-face {
            font-family: "Cambria Math";
            panose-1: 2 4 5 3 5 4 6 3 2 4;
        }

        @font-face {
            font-family: Calibri;
            panose-1: 2 15 5 2 2 2 4 3 2 4;
        }

        /* Style Definitions */
        p.MsoNormal, li.MsoNormal, div.MsoNormal {
            margin: 0cm;
            margin-bottom: .0001pt;
            font-size: 12.0pt;
            font-family: "Calibri", sans-serif;
        }

        a:link, span.MsoHyperlink {
            color: #0563C1;
            text-decoration: underline;
        }

        a:visited, span.MsoHyperlinkFollowed {
            color: #954F72;
            text-decoration: underline;
        }

        p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph {
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 0cm;
            margin-left: 36.0pt;
            margin-bottom: .0001pt;
            font-size: 12.0pt;
            font-family: "Calibri", sans-serif;
        }

        .MsoChpDefault {
            font-family: "Calibri", sans-serif;
        }

        @page WordSection1 {
            size: 595.0pt 842.0pt;
            margin: 72.0pt 72.0pt 72.0pt 72.0pt;
        }

        div.WordSection1 {
            page: WordSection1;
        }

        /* List Definitions */
        ol {
            margin-bottom: 0cm;
        }

        ul {
            margin-bottom: 0cm;
        }

        -->
    </style>

</head>

<body bgcolor=white lang=EN-US link="#0563C1" vlink="#954F72">

<div class=WordSection1>

    <table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0
           style='border-collapse:collapse;border:none'>
        <tr>
            <td width=165 valign=top style='width:164.8pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='text-align:center'><img width=133
                                                                               height=40 id="Picture 1"
                                                                               src="passband.fld/image001.png"></p>
                <p class=MsoNormal align=center style='text-align:center'><span
                            style='font-size:8.0pt'>BANDARA INTERNASIONAL AHMAD YANI<br>
  SEMARANG</span></p>
            </td>
            <td width=286 valign=top style='width:285.7pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-left:120.35pt'><span style='font-size:8.0pt'>PT.
  ANGKASA PURA 1 (PERSERO)</span></p>
                <p class=MsoNormal style='margin-left:120.35pt'><span style='font-size:8.0pt'>Kantor
  Cabang Semarang</span></p>
                <p class=MsoNormal style='margin-left:120.35pt'><span style='font-size:8.0pt'>Bandara
  Internasional Ahmad Yani</span></p>
                <p class=MsoNormal style='margin-left:120.35pt'><span style='font-size:8.0pt'>Jl
  Puad A. Yani Semarang (50145)</span></p>
                <p class=MsoNormal style='margin-left:120.35pt'><span style='font-size:8.0pt'>Tel:
  (024) 7608735 Fax: (024) 7603506</span></p>
                <p class=MsoNormal style='margin-left:120.35pt'><span style='font-size:8.0pt;
  color:black'>Web: www.ayani-airport.com</span></p>
                <p class=MsoNormal style='margin-left:120.35pt'><span style='font-size:8.0pt;
  color:black'>Email: <a href="mailto:tata.usaha@ayani-airport.com"><span
                                    style='color:black;text-decoration:none'>tata.usaha@ayani-airport.com</span></a>
  </span></p>
            </td>
        </tr>
    </table>

    <p class=MsoNormal align=center style='text-align:center'>&nbsp;</p>

    <p class=MsoNormal align=center style='text-align:center'><u><span
                    style='font-size:18.0pt'>N O T A – D I N A S</span></u></p>

    <p class=MsoNormal align=center style='text-align:center'><span
                style='font-size:11.0pt'>Nomor: SRG.BD.087 / KU.07.08 / 2018-PAS-B</span></p>

    <p class=MsoNormal align=center style='text-align:center'><span
                style='font-size:11.0pt'>&nbsp;</span></p>

    <table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0
           style='border-collapse:collapse;border:none'>
        <tr>
            <td width=85 valign=top style='width:84.8pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>KEPADA
  YTH</span></p>
            </td>
            <td width=14 valign=top style='width:14.2pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>:</span></p>
            </td>
            <td width=352 valign=top style='width:351.5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>PROPERTI
  &amp; ADVERTISING SALES SECTION HEAD</span></p>
            </td>
        </tr>
        <tr>
            <td width=85 valign=top style='width:84.8pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>DARI</span></p>
            </td>
            <td width=14 valign=top style='width:14.2pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>:</span></p>
            </td>
            <td width=352 valign=top style='width:351.5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>AIRPORT
  LANDSIDE &amp; TERMINAL SECTION HEAD</span></p>
            </td>
        </tr>
        <tr>
            <td width=85 valign=top style='width:84.8pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>PERIHAL</span></p>
            </td>
            <td width=14 valign=top style='width:14.2pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>:</span></p>
            </td>
            <td width=352 valign=top style='width:351.5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>PEMBAYARAN
  PAS BANDARA</span></p>
            </td>
        </tr>
    </table>

    <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>&nbsp;</span></p>

    <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>Diharapkan
menerima dan membuatkan kwitansi pembayaran Pas Bandara sesuai daftar terlampir
dengan perincian sebagai berikut:</span></p>

    <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>&nbsp;</span></p>

    <table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0 width=448
           style='width:448.05pt;border-collapse:collapse;border:none'>
        <tr>
            <td width=169 valign=top style='width:169.25pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>Nama
  Perusahaan / Instansi</span></p>
            </td>
            <td width=14 valign=top style='width:14.05pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>:</span></p>
            </td>
            <td width=265 valign=top style='width:264.75pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>KOKAPURA</span></p>
            </td>
        </tr>
        <tr>
            <td width=169 valign=top style='width:169.25pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>Sesuai
  surat permohonan Pas No</span></p>
            </td>
            <td width=14 valign=top style='width:14.05pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>:</span></p>
            </td>
            <td width=265 valign=top style='width:264.75pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>158/PH/KT</span></p>
            </td>
        </tr>
        <tr style='height:12.75pt'>
            <td width=169 valign=top style='width:169.25pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:12.75pt'>
                <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>Periode</span></p>
            </td>
            <td width=14 valign=top style='width:14.05pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:12.75pt'>
                <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>:</span></p>
            </td>
            <td width=265 valign=top style='width:264.75pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:12.75pt'>
                <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>Bulanan
  2018</span></p>
            </td>
        </tr>
    </table>

    <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>&nbsp;</span></p>

    <p class=MsoNormal style='text-align:justify'><b><span style='font-size:11.0pt'>PAS
ORANG / AERONAUTIKA / PAS BULANAN 2018 sbb:</span></b></p>

    <div align=center>

        <table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0
               style='border-collapse:collapse;border:none'>
            <tr>
                <td width=25 valign=top style='width:24.95pt;border:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>No</span></p>
                </td>
                <td width=90 valign=top style='width:90.0pt;border:solid windowtext 1.0pt;
  border-left:none;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal align=center style='text-align:center'><span
                                style='font-size:11.0pt'>NO. URUT</span></p>
                    <p class=MsoNormal align=center style='text-align:center'><span
                                style='font-size:11.0pt'>PAS/STICKER</span></p>
                </td>
                <td width=112 valign=top style='width:112.2pt;border:solid windowtext 1.0pt;
  border-left:none;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal align=center style='text-align:center'><span
                                style='font-size:11.0pt'>Jenis Pass</span></p>
                    <p class=MsoNormal align=center style='text-align:center'><span
                                style='font-size:11.0pt'>Kategori – NPA/RPA</span></p>
                </td>
                <td width=75 valign=top style='width:74.75pt;border:solid windowtext 1.0pt;
  border-left:none;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal align=center style='text-align:center'><span
                                style='font-size:11.0pt'>Jangka Waktu</span></p>
                    <p class=MsoNormal align=center style='text-align:center'><span
                                style='font-size:11.0pt'>Bln/Tahun</span></p>
                </td>
                <td width=74 valign=top style='width:74.3pt;border:solid windowtext 1.0pt;
  border-left:none;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal align=center style='text-align:center'><span
                                style='font-size:11.0pt'>Jumlah PAS</span></p>
                </td>
                <td width=74 valign=top style='width:74.3pt;border:solid windowtext 1.0pt;
  border-left:none;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal align=center style='text-align:center'><span
                                style='font-size:11.0pt'>Jumlah Total</span></p>
                </td>
            </tr>
            <tr>
                <td width=25 valign=top style='width:24.95pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>&nbsp;</span></p>
                </td>
                <td width=90 valign=top style='width:90.0pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>&nbsp;</span></p>
                </td>
                <td width=112 valign=top style='width:112.2pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>&nbsp;</span></p>
                </td>
                <td width=75 valign=top style='width:74.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>&nbsp;</span></p>
                </td>
                <td width=74 valign=top style='width:74.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>&nbsp;</span></p>
                </td>
                <td width=74 valign=top style='width:74.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>&nbsp;</span></p>
                </td>
            </tr>
            <tr>
                <td width=25 valign=top style='width:24.95pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>&nbsp;</span></p>
                </td>
                <td width=90 valign=top style='width:90.0pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>&nbsp;</span></p>
                </td>
                <td width=112 valign=top style='width:112.2pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>&nbsp;</span></p>
                </td>
                <td width=75 valign=top style='width:74.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>&nbsp;</span></p>
                </td>
                <td width=74 valign=top style='width:74.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>&nbsp;</span></p>
                </td>
                <td width=74 valign=top style='width:74.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>&nbsp;</span></p>
                </td>
            </tr>
            <tr>
                <td width=25 valign=top style='width:24.95pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>&nbsp;</span></p>
                </td>
                <td width=90 valign=top style='width:90.0pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>&nbsp;</span></p>
                </td>
                <td width=112 valign=top style='width:112.2pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>&nbsp;</span></p>
                </td>
                <td width=75 valign=top style='width:74.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>&nbsp;</span></p>
                </td>
                <td width=74 valign=top style='width:74.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>&nbsp;</span></p>
                </td>
                <td width=74 valign=top style='width:74.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>&nbsp;</span></p>
                </td>
            </tr>
        </table>

    </div>

    <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>&nbsp;</span></p>

    <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>Demikian
disampaikan atas perhatian dan kerjasamanya diucapkan terima kasih.</span></p>

    <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>&nbsp;</span></p>

    <p class=MsoNormal align=right style='text-align:right'><span style='font-size:
11.0pt'>Semarang, 2 April 2018</span></p>

    <p class=MsoNormal align=right style='text-align:right'><span style='font-size:
11.0pt'>AIRPORT OPS. LANDSIDE &amp; TERMINAL</span></p>

    <p class=MsoNormal align=right style='text-align:right'><span style='font-size:
11.0pt'>SECTION HEAD,</span></p>

    <p class=MsoNormal align=right style='text-align:right'><span style='font-size:
11.0pt'>&nbsp;</span></p>

    <p class=MsoNormal align=right style='text-align:right'><span style='font-size:
11.0pt'>&nbsp;</span></p>

    <p class=MsoNormal align=right style='text-align:right'><span style='font-size:
11.0pt'>SUKIMAN</span></p>

    <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>&nbsp;</span></p>

    <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>Tembusan
Yth:</span></p>

    <p class=MsoListParagraph style='text-align:justify;text-indent:-18.0pt'><span
                style='font-size:11.0pt'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:11.0pt'>Airport Operation &amp; Sales
Departement Head</span></p>

    <p class=MsoListParagraph style='text-align:justify;text-indent:-18.0pt'><span
                style='font-size:11.0pt'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:11.0pt'>Property &amp; Advertising Sales
Section Head (untuk penerbitan tagihan dan pajak)</span></p>

    <table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0
           style='border-collapse:collapse;border:none'>
        <tr>
            <td width=451 valign=top style='width:450.5pt;border:none;border-bottom:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='text-align:justify'><span style='font-size:11.0pt'>&nbsp;</span></p>
            </td>
        </tr>
    </table>

    <p class=MsoNormal style='text-align:justify'><span style='font-size:8.0pt'>ND.
087 Pembayaran Pas Bln.03/2018</span></p>

</div>

</body>

</html>
