@extends('front.template')
@section('content')

    <section class="tm-margin-large-top">
        <div class="uk-container uk-text-center">

            <p class="uk-margin-remove-bottom tm-overtitle uk-margin-top uk-text-uppercase letter-spacing-l">Pass
                Dashboard</p>
            <h1 class="uk-margin-small-top uk-heading-hero">

                <?php
                echo session('trans_id');


                if (!Session::has('trans_id')) {
                    return redirect(route('front.index'));
                }

                ?>
            </h1>


            <div class="uk-grid-medium uk-child-width-1-3@m uk-grid-small uk-grid-match tm-margin-large-top uk-text-center"
                 data-uk-grid>
                {{--<div>--}}
                    {{--<a href="{{route('front.newpass')}}" class="uk-card uk-card-box uk-card-body uk-border-rounded">--}}
                    {{--<span class="uk-icon-forder uk-border-rounded"><span--}}
                                {{--data-uk-icon="icon: plus; ratio: 2"></span></span>--}}
                        {{--<p>Data Pribadi</p>--}}
                    {{--</a>--}}
                {{--</div>--}}
                <div>
                    <a href="{{route('front.security')}}"
                       class="uk-card uk-card-box uk-card-body uk-border-rounded">
                    <span class="uk-icon-forder uk-border-rounded"><span
                                data-uk-icon="icon: calendar; ratio: 2"></span></span>
                        <p>Screening Form</p>
                    </a>
                </div>
                <div>
                    <a href="{{ url('/logoff') }}" class="uk-card uk-card-box uk-card-body uk-border-rounded">
                    <span class="uk-icon-forder uk-border-rounded"><span
                                data-uk-icon="icon: check; ratio: 2"></span></span>
                        <p>Keluar</p>
                    </a>
                </div>

            </div>

        </div>
    </section>

@endsection