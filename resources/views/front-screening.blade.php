@extends('template-front')
@section('content')
    <div class="row">
        <div class="col-lg-12">

        </div>
        <!-- /.col-lg-12 -->
        <div class="col-lg-12">
            <div class="panel panel-default">

                <div class="panel-body">
                    <h1 class="page-header">Pengisian Data Diri Pendaftar</h1>
                    <h3 style="text-align:center">Silahkan tunggu instruksi petugas untuk mengisi data keluarga dan data
                        pendidikan Anda.</h3>
                    <h3 style="text-align:center">Anda memulai dengan klik pada tombol di bawah ini.</h3>
                    <div class="box-body" style="text-align:center">
                        <!-- /.box-header -->
                        <a href="{{ route('inputscreening.create') }}" class="btn btn-success btn-lg"
                           data-toggle="confirmation" data-title="Mulai isi data?" data-btn-ok-label="Ya"
                           data-btn-cancel-label="Batal">Mulai Isi Data</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>




@endsection
@section('custom_script')
    <!-- jQuery 2.2.3 -->
    <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <!-- page script -->
    <script type="text/javascript" src="{{URL::asset('plugins/jQuery/bootstrap-confirmation.min.js')}}"></script>
    <script>
        $(document).find('[data-toggle="confirmation"]').confirmation();
    </script>

    </body>










    <script type="text/javascript">

    </script>

@endsection
