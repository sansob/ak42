@extends('template-show')
@section('content')

    <div id="inputpass" class="box">
        <!-- /.box-header -->
        <div class="panel-body">
            <?php
            $idtrans = $trans;
            //$pemohon= DB::table('transaksi_pass')->where('ID_TRANSAKSI','=',$idtrans)->get();
            ?>
            <h2>Isi Data Pembuatan Pass</h2>
            <p>Silahkan pilih data pendaftar dan/atau data kendaraan yang akan dibuatkan pass dengan menekan tombol "+"
                di sebelah kanan data.</p>
            <p>Setelah selesai menginputkan data untuk perpanjangan, silahkan tekan tombol "Selesai"</p>
            <div class="form-group">
                <a href="{{ route('transaksi.index2') }}" class="btn btn-success btn-lg">Selesai</a>
            </div>

        </div>

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#pendaftar" data-toggle="tab">Data Pendaftar</a></li>
                <li><a href="#kendaraan" data-toggle="tab">Data Kendaraan</a></li>
            </ul>

            <div class="box-body">
                <div class="tab-content">
                    <div class="active tab-pane" id="pendaftar">
                        <div class="tab-content">

                            <table id="tblpendaftar" class="table table-bordered table-hover">
                                <thead>
                                <th>ID Pendaftar</th>
                                <th>Nama</th>
                                <th>Alamat Rumah</th>
                                <th>Nomor Telepon</th>
                                <th>Nomor Pegawai</th>
                                <th>Pekerjaan</th>
                                <th>Instansi</th>
                                <th>Alamat Kantor</th>
                                <th>Nomor Telepon Kantor</th>
                                <th>Aksi</th>

                                </thead>
                                <tbody>
                                <?php $Pendaftar = \App\PendaftarModel::where('STATUS_PENDAFTAR', '=', 'Terdaftar')->get();?>
                                @foreach($Pendaftar as $Pendaf)
                                    <tr>
                                        <td>{{ $Pendaf->ID_PENDAFTAR }}</td>
                                        <td>{{ $Pendaf->NAMA_PENDAFTAR }}</td>
                                        <td>{{ $Pendaf->ALAMAT_RUMAH}}</td>
                                        <td>{{ $Pendaf->NO_TELP}}</td>
                                        <td>{{ $Pendaf->NO_PEGAWAI }}</td>
                                        <td>{{ $Pendaf->PEKERJAAN }}</td>
                                        <td>{{ $Pendaf->INSTANSI}}</td>
                                        <td>{{ $Pendaf->ALAMAT_KANTOR}}</td>
                                        <td>{{ $Pendaf->NO_TELP_KANTOR}}</td>
                                        <td>

                                            <a href="{{route('passorang.getpendaf',[$Pendaf->ID_PENDAFTAR,$idtrans])}}"
                                               class="btn btn-success btn-xs  fa fa-plus" data-toggle="confirmation"
                                               data-title="Anda yakin data pendaftar yang dipilih sudah benar?"
                                               data-btn-ok-label="Ya" data-btn-cancel-label="Batal"></a>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                    </div>

                    <!-- /.tab-pane -->


                    <div class="tab-pane" id="kendaraan">
                        <div class="tab-content">
                            <?php
                            $kendaraan = \App\KendaraanModel::all();
                            ?>

                            <table id="tblkendaraan" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>ID Kendaraan</th>
                                    <th>Nama Kendaraan</th>
                                    <th>Jenis Kendaraan</th>
                                    <th>Nomor Polisi</th>
                                    <th>Warna Plat</th>
                                    <th>Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($kendaraan as $kend)
                                    <tr>
                                        <td>{{ $kend->ID_KENDARAAN}}</td>
                                        <td>{{ $kend->NAMA_KENDARAAN}}</td>
                                        <td>{{ $kend->JenisKend->NAMA_JENIS}}</td>
                                        <td>{{ $kend->NO_POLISI}}</td>
                                        <td>{{ $kend->WARNA_PLAT}}</td>
                                        <td>
                                            <a href="{{route('passkendaraan.getkend',[$kend->ID_KENDARAAN,$idtrans])}}"
                                               class="btn btn-success btn-xs  fa fa-plus" data-toggle="confirmation"
                                               data-title="Anda yakin data kendaraan yang dipilih benar?"
                                               data-btn-ok-label="Ya" data-btn-cancel-label="Batal"></a></td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>

                        </div>

                    </div>

                </div>

                <!-- /.tab-pane -->

            </div>
            <!-- /.tab-content -->

        </div>
        <!-- /.nav-tabs-custom -->


    </div>

    <!-- /.box-body -->
    </div>

    <!-- /.box -->
    </div>


    <!-- /.col -->
    </div>
    <!-- /.row -->

    <!--/.Modal-->
    <!-- Modal -->

@endsection
@section('custom_script')
    <!-- jQuery 2.2.3 -->
    <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <!-- page script -->
    <script type="text/javascript" src="{{URL::asset('plugins/jQuery/bootstrap-confirmation.min.js')}}"></script>
    <script>
        $(document).find('[data-toggle="confirmation"]').confirmation();
    </script>
    <script>
        $(function () {
            $("#tblpendaftar").DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": false,
                "autoWidth": true
            });
            $("#tblkendaraan").DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": false,
                "autoWidth": true
            });
        });
    </script>
    </body>










    <script type="text/javascript">

    </script>

@endsection
