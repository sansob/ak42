@extends('template-front')
@section('content')

    <div id="inputpass" class="box">
        <!-- /.box-header -->
        <div class="panel-body">
            <?php
            $getmaxtrans = $trans;
            $pemohon = DB::table('transaksi_pass')->select('NAMA_PEMOHON')->where('ID_TRANSAKSI', '=', $getmaxtrans)->get();
            ?>
            <h2>Isi Data Perpanjangan Pass</h2>
            <p>Silahkan perbarui data kendaraan yang akan dibuatkan pass jika diperlukan.</p>
        </div>
        <div class="box-body">
            <h2>Input Data Pass Orang</h2>
            <form action="{{ route('passkendaraan.perpanjang') }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <?php
                $kend = $kendaraan;


                $now = date('Y-m-d');
                ?>
                <input type="hidden" name="_method" value="PATCH">
                <input type="hidden" name="ID_KENDARAAN" value="<?= $kend['ID_KENDARAAN'] ?>">
                <input type="hidden" name="ID_TRANSAKSI" value="<?= $getmaxtrans ?>">
                <div class="form-group">
                    <label for="title">Nama Kendaraan</label>
                    <input type="text" name="NAMA_KENDARAAN" class="form-control" value="<?= $kend['NAMA_KENDARAAN'] ?>"
                           required>
                </div>
                <div class="form-group">
                    <label for="title">Nomor Polisi</label>
                    <input type="text" name="NO_POLISI" class="form-control" value="<?= $kend['NO_POLISI'] ?>" required>
                </div>


                <div class="form-group">
                    <label for="title">Jenis Kendaraan</label>
                    <select class="form-control" id="jenis" name="ID_JENIS_KEND" value="">
                        <option value="JK-01">Mobil Airlines s/d 2 ton (APRON)</option>
                        <option value="JK-02">R4 / Mobil Mitra s/d 2 ton</option>
                        <option value="JK-03">ATN / T. Refuel < 10 ton</option>
                        <option value="JK-04">Tangki Refuel > 10 ton</option>
                        <option value="JK-05">BTT s/d > 2 ton</option>
                        <option value="JK-06">Roda 4 minibus / pelataran</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="title">Warna Plat</label>
                    <select class="form-control" id="plat" name="WARNA_PLAT" value="<?= $kend['WARNA_PLAT'] ?>">
                        <option value="Merah">Merah</option>
                        <option value="Hitam">Hitam</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="title">Nama Pemegang Pass</label>
                    <input type="text" name="PEMEGANG" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="title">Periode</label>
                    <select class="form-control" name="periode">
                        <option value="Bulanan">Bulanan</option>
                        <option value="Tahunan">Tahunan</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="title">Tanggal Mulai</label>
                    <input type="date" name="TGL_MULAI" class="form-control" value="{{$now}}" required>
                </div>
                <div class="form-group">
                    <label for="title">Tanggal Selesai</label>
                    <input type="date" name="TGL_SELESAI" class="form-control" value="{{$now}}" required>
                </div>


                <div class="form-group">
                    <button type="submit" class="btn btn-success" data-toggle="confirmation"
                            data-title="Anda yakin data yang dimasukkan sudah benar?" data-btn-ok-label="Ya"
                            data-btn-cancel-label="Batal">
                        Simpan
                    </button>
                    <a href="{{ route('passorang.show',$getmaxtrans) }}" type="button" class="btn btn-danger"><i
                                class="glyphicon glyphicon-floppy-remove"></i>Batal</a>
                </div>
            </form>
        </div>
    </div>
    <!-- /.tab-pane -->




    </div>

    </div>

    <!-- /.tab-pane -->

    </div>
    <!-- /.tab-content -->

    </div>
    <!-- /.nav-tabs-custom -->

    </div>
    </div>


    <!-- /.box-body -->
    </div>

    <!-- /.box -->
    </div>


    <!-- /.col -->
    </div>
    <!-- /.row -->

    <!--/.Modal-->
    <!-- Modal -->






@endsection
@section('custom_script')
    <!-- jQuery 2.2.3 -->
    <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <!-- page script -->
    <script type="text/javascript" src="{{URL::asset('plugins/jQuery/bootstrap-confirmation.min.js')}}"></script>
    <script>
        document.getElementById('jenis').value = '<?= $kend['ID_JENIS_KEND'] ?>';
        document.getElementById('plat').value = '<?= $kend['WARNA_PLAT'] ?>';
    </script>
    <script>
        $(document).find('[data-toggle="confirmation"]').confirmation();

    </script>
    <script>

        $(function () {
            $("#kendaraanbulan").DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": false,
                "autoWidth": true
            });
            $("#kendaraantahun").DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": false,
                "autoWidth": true
            });
            $("#pendaftarbulan").DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": false,
                "autoWidth": true
            });
            $("#pendaftartahun").DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": false,
                "autoWidth": true
            });
        });
    </script>
    </body>










    <script type="text/javascript">

        $(document).on("click", '.edit_button', function (e) {

            var id = $(this).data('id');
            $(".id").val(id);
            var nama = $(this).data('nama');
            $(".nama").val(nama);

            var identitas = $(this).data('identitas');
            $(".identitas").val(identitas);

            var alamatrmh = $(this).data('alamatrmh');
            $(".alamatrmh").val(alamatrmh);
            var telprmh = $(this).data('telprmh');
            $(".telprmh").val(telprmh);
            var nopeg = $(this).data('nopeg');
            $(".nopeg").val(nopeg);
            var pkrjn = $(this).data('pkrjn');
            $(".pkrjn").val(pkrjn);
            var instansi = $(this).data('instansi');
            $(".instansi").val(instansi);
            var alamatkntr = $(this).data('alamatkntr');
            $(".alamatkntr").val(alamatkntr);
            var telpkntr = $(this).data('telpkntr');
            $(".telpkntr").val(telpkntr);
            var nopeg = $(this).data('nopeg');
            $(".nopeg").val(nopeg);
            var tmptlhr = $(this).data('tmptlhr');
            $(".tmptlhr").val(tmptlhr);

            var tgllhr = $(this).data('tgllhr');
            $(".tgllhr").val(tgllhr);

            var masa = $(this).data('masa');
            $(".masa").val(masa);

            var tmptkerjasblm = $(this).data('tmptkerjasblm');
            $(".tmptkerjasblm").val(tmptkerjasblm);


            var status = $(this).data('status');
            if (status == 'Tetap')
                $(".status option[value=Tetap]").attr('selected', 'selected');
            else
                $(".status option[value=Kontrak]").attr('selected', 'selected');

            var agama = $(this).data('agama');
            if (agama == 'Islam')
                $(".agama option[value=Islam]").attr('selected', 'selected');
            else if (agama == 'Katholik')
                $(".agama option[value=Katholik]").attr('selected', 'selected');
            else if (status == 'Kristen')
                $(".agama option[value=Kristen]").attr('selected', 'selected');
            else if (status == 'Hindu')
                $(".agama option[value=Hindu]").attr('selected', 'selected');
            else if (status == 'Buddha')
                $(".agama option[value=Buddha]").attr('selected', 'selected');
            else
                $(".agama option[value=Lain-lain]").attr('selected', 'selected');


        });
        $(document).on("click", '.edit_kend', function (e) {

            var idkend = $(this).data('idkend');
            $(".idkend").val(idkend);
            var namakend = $(this).data('namakend');
            $(".namakend").val(namakend);

            var nopol = $(this).data('nopol');
            $(".nopol").val(nopol);


            var warna = $(this).data('warna');
            if (warna == 'Hitam')
                $(".warna option[value=Hitam]").attr('selected', 'selected');
            else
                $(".warna option[value=Merah]").attr('selected', 'selected');

            var jeniskend = $(this).data('jeniskend');
            if (jeniskend == 'JK-01')
                $(".jeniskend option[value=JK-01]").attr('selected', 'selected');
            else if (jeniskend == 'JK-02')
                $(".jeniskend option[value=JK-02]").attr('selected', 'selected');
            else if (jeniskend == 'JK-03')
                $(".jeniskend option[value=JK-03]").attr('selected', 'selected');
            else if (jeniskend == 'JK-04')
                $(".jeniskend option[value=JK-04]").attr('selected', 'selected');
            else if (jeniskend == 'JK-05')
                $(".jeniskend option[value=JK-05]").attr('selected', 'selected');
            else
                $(".jeniskend option[value=JK-06]").attr('selected', 'selected');

        });

        }
    </script>

@endsection
