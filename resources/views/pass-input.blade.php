@extends('template-show')
@section('content')

    <div id="inputpass" class="box">
        <!-- /.box-header -->
        <div class="panel-body">
            <?php
            $getmaxtrans = $idtrans;

            ?>
            <h2>Isi Data Pembuatan Pass</h2>
            <p>Silahkan isi data pendaftar dan/atau data kendaraan yang akan dibuatkan pass.</p>
            <p>Setelah selesai menginputkan data silahkan tekan tombol "Selesai"</p>
            <div class="form-group">
                <a href="{{ route('transaksi.index2') }}" class="btn btn-success btn-lg">Selesai</a>
            </div>
        </div>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#pendaftar" data-toggle="tab">Data Pendaftar</a></li>
                <li><a href="#kendaraan" data-toggle="tab">Data Kendaraan</a></li>
            </ul>
            <div class="box-body">
                <div class="tab-content">
                    <div class="active tab-pane" id="pendaftar">
                        <div class="tab-content">
                            <h2>Input Data Pass Orang</h2>
                            <form action="{{ route('passorang.store') }}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <?php
                                $getmax = DB::table('data_pendaftar')->max('ID_PENDAFTAR');
                                if ($getmax == null) {
                                    $next = "0001";
                                } else {
                                    $angka = substr($getmax, 3, 4);
                                    $next = str_repeat("0", 4 - strlen($angka + 1)) . ($angka + 1);
                                }
                                $id = "PD-" . $next;

                                $now = date('Y-m-d');
                                ?>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="ID_PENDAFTAR" value="<?= $id ?>">
                                <input type="hidden" name="ID_TRANSAKSI" value="<?= $getmaxtrans ?>">

                                <div class="form-group">
                                    <label for="title">Nama Pendaftar</label>
                                    <input type="text" name="NAMA_PENDAFTAR" id="NAMA_PENDAFTAR" class="form-control" maxlength="1024"
                                           required>
                                </div>
                                <div class="form-group">
                                    <label for="title">Nomor Identitas (KTP)</label>
                                    <input type="number" name="NO_IDENTITAS" id="NO_IDENTITAS" class="form-control" maxlength="50"
                                           required>
                                </div>
                                <div class="form-group">
                                    <label for="title">Tempat Lahir</label>
                                    <input type="text" name="TEMPAT_LAHIR" id="TEMPAT_LAHIR" class="form-control" maxlength="50" required>
                                </div>
                                <div class="form-group">
                                    <label for="title">Tanggal Lahir</label>
                                    <input type="date" name="TGL_LAHIR" id="TGL_LAHIR" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="title">Agama</label>
                                    <select class="form-control" name="AGAMA">
                                        <option value="Islam">Islam</option>
                                        <option value="Katholik">Katholik</option>
                                        <option value="Kristen">Kristen</option>
                                        <option value="Hindu">Hindu</option>
                                        <option value="Buddha">Buddha</option>
                                        <option value="Lain-lain">Lain-lain</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="title">Alamat Rumah</label>
                                    <input type="text" name="ALAMAT_RUMAH" id="ALAMAT_RUMAH" class="form-control" maxlength="1024"
                                           required>
                                </div>
                                <div class="form-group">
                                    <label for="title">Nomor Telepon</label>
                                    <input type="number" name="NO_TELP" id="NO_TELP" class="form-control" maxlength="20" required>
                                </div>
                                <div class="form-group">
                                    <label for="title">Instansi</label>
                                    <input type="text" name="INSTANSI" id="INSTANSI" class="form-control" maxlength="1024" required>
                                </div>
                                <div class="form-group">
                                    <label for="title">Alamat Kantor</label>
                                    <input type="text" name="ALAMAT_KANTOR" id="ALAMAT_KANTOR" class="form-control" maxlength="1024"
                                           required>
                                </div>
                                <div class="form-group">
                                    <label for="title">Nomor Telepon Kantor</label>
                                    <input type="number" name="NO_TELP_KANTOR" id="NO_TELP_KANTOR" class="form-control" maxlength="20"
                                           required>
                                </div>
                                <div class="form-group">
                                    <label for="title">Nomor Pegawai</label>
                                    <input type="text" name="NO_PEGAWAI" id="NO_PEGAWAI" class="form-control" maxlength="50" required>
                                </div>
                                <div class="form-group">
                                    <label for="title">Pekerjaan</label>
                                    <input type="text" name="PEKERJAAN" id="PEKERJAAN" class="form-control" maxlength="1024" required>
                                </div>
                                <div class="form-group">
                                    <label for="title">Status Pekerjaan</label>
                                    <select class="form-control" name="STATUS_KERJA" id="STATUS_KERJA">
                                        <option value="Tetap">Tetap</option>
                                        <option value="Kontrak">Kontrak</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="title">Masa Kerja</label>
                                    <input type="number" name="MASA_KERJA_tahun" id="MASA_KERJA_tahun" required>
                                    <label for="title" style="text-align:inline">tahun</label>
                                    <select id="year" name="MASA_KERJA_bulan"  class="form_control">
                                        <script>
                                            for (var i = 1; i <= 11; i++) {
                                                document.write('<option value="' + i + '">' + i + '</option>');
                                            }
                                        </script>
                                    </select>
                                    <label for="title" style="text-align:inline">bulan</label>
                                </div>
                                <div class="form-group">
                                    <label for="title">Tempat Kerja Sebelumnya</label>
                                    <input type="text" name="TEMPAT_KERJA_SBLMNYA" id="TEMPAT_KERJA_SBLMNYA" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="title">Periode</label>
                                    <select class="form-control" name="PERIODE" id="PERIODE">
                                        <option value="Bulanan">Bulanan</option>
                                        <option value="Tahunan">Tahunan</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="title">Tanggal Mulai</label>
                                    <input type="date" name="TGL_MULAI" id="TGL_MULAI" class="form-control" value="{{$now}}" required>
                                </div>
                                <div class="form-group">
                                    <label for="title">Tanggal Selesai</label>
                                    <input type="date" name="TGL_SELESAI" id="TGL_SELESAI" class="form-control" value="{{$now}}"
                                           required>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">
                                        <i class="glyphicon glyphicon-floppy-saved"></i>Simpan
                                    </button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i
                                                class="glyphicon glyphicon-floppy-remove"></i> Batal
                                    </button>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- /.tab-pane -->


                    <div class="tab-pane" id="kendaraan">
                        <div class="tab-content">
                            <h2>Input Data Pass Kendaraan</h2>
                            <form action="{{ route('passkendaraan.store') }}" method="POST"
                                  enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <?php
                                $getmax = DB::table('data_kendaraan')->max('ID_KENDARAAN');
                                if ($getmax == null) {
                                    $next = "0001";
                                } else {
                                    $angka = substr($getmax, 3, 4);
                                    $next = str_repeat("0", 4 - strlen($angka + 1)) . ($angka + 1);
                                }
                                $id = "KD-" . $next;
                                ?>

                                <input type="hidden" name="ID_KENDARAAN" value="<?= $id ?>">
                                <input type="hidden" name="ID_TRANSAKSI" value="<?= $getmaxtrans ?>">
                                <div class="form-group">
                                    <label for="title">Nama Kendaraan</label>
                                    <input type="text" name="NAMA_KENDARAAN" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="title">Nomor Polisi</label>
                                    <input type="text" name="NO_POLISI" class="form-control" required>
                                </div>


                                <div class="form-group">
                                    <label for="title">Jenis Kendaraan</label>
                                    <select class="form-control" name="ID_JENIS_KEND">
                                        <option value="JK-01">Mobil Airlines s/d 2 ton (APRON)</option>
                                        <option value="JK-02">R4 / Mobil Mitra s/d 2 ton</option>
                                        <option value="JK-03">ATN / T. Refuel < 10 ton</option>
                                        <option value="JK-04">Tangki Refuel > 10 ton</option>
                                        <option value="JK-05">BTT s/d > 2 ton</option>
                                        <option value="JK-06">Roda 4 minibus / pelataran</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="title">Warna Plat</label>
                                    <select class="form-control" name="WARNA_PLAT">
                                        <option value="Merah">Merah</option>
                                        <option value="Hitam">Hitam</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="title">Nama Pemegang Pass</label>
                                    <input type="text" name="PEMEGANG" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="title">Periode</label>
                                    <select class="form-control" name="periode">
                                        <option value="Bulanan">Bulanan</option>
                                        <option value="Tahunan">Tahunan</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="title">Tanggal Mulai</label>
                                    <input type="date" name="TGL_MULAI" class="form-control" value="{{$now}}" required>
                                </div>
                                <div class="form-group">
                                    <label for="title">Tanggal Selesai</label>
                                    <input type="date" name="TGL_SELESAI" class="form-control" value="{{$now}}"
                                           required>
                                </div>


                                <div class="form-group">
                                    <button type="submit" class="btn btn-success" data-toggle="confirmation"
                                            data-title="Anda yakin data yang dimasukkan sudah benar?"
                                            data-btn-ok-label="Ya" data-btn-cancel-label="Batal">
                                        <i class="glyphicon glyphicon-floppy-saved"></i>Simpan
                                    </button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i
                                                class="glyphicon glyphicon-floppy-remove"></i>Batal
                                    </button>
                                    </button>
                                </div>
                            </form>


                        </div>

                    </div>

                </div>

                <!-- /.tab-pane -->

            </div>
            <!-- /.tab-content -->

        </div>
        <!-- /.nav-tabs-custom -->

    </div>
    </div>


    <!-- /.box-body -->
    </div>

    <!-- /.box -->
    </div>


    <!-- /.col -->
    </div>
    <!-- /.row -->

    <!--/.Modal-->
    <!-- Modal -->






@endsection
@section('custom_script')
    <!-- jQuery 2.2.3 -->
    <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <!-- page script -->
    <script type="text/javascript" src="{{URL::asset('plugins/jQuery/bootstrap-confirmation.min.js')}}"></script>
    <script>
        $(document).find('[data-toggle="confirmation"]').confirmation();
    </script>
    <script>

        $(function () {
            $("#kendaraanbulan").DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": false,
                "autoWidth": true
            });
            $("#kendaraantahun").DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": false,
                "autoWidth": true
            });
            $("#pendaftarbulan").DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": false,
                "autoWidth": true
            });
            $("#pendaftartahun").DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": false,
                "autoWidth": true
            });
        });
    </script>
    </body>










    <script type="text/javascript">

        $(document).on("click", '.edit_button', function (e) {

            var id = $(this).data('id');
            $(".id").val(id);
            var nama = $(this).data('nama');
            $(".nama").val(nama);

            var identitas = $(this).data('identitas');
            $(".identitas").val(identitas);

            var alamatrmh = $(this).data('alamatrmh');
            $(".alamatrmh").val(alamatrmh);
            var telprmh = $(this).data('telprmh');
            $(".telprmh").val(telprmh);
            var nopeg = $(this).data('nopeg');
            $(".nopeg").val(nopeg);
            var pkrjn = $(this).data('pkrjn');
            $(".pkrjn").val(pkrjn);
            var instansi = $(this).data('instansi');
            $(".instansi").val(instansi);
            var alamatkntr = $(this).data('alamatkntr');
            $(".alamatkntr").val(alamatkntr);
            var telpkntr = $(this).data('telpkntr');
            $(".telpkntr").val(telpkntr);
            var nopeg = $(this).data('nopeg');
            $(".nopeg").val(nopeg);
            var tmptlhr = $(this).data('tmptlhr');
            $(".tmptlhr").val(tmptlhr);

            var tgllhr = $(this).data('tgllhr');
            $(".tgllhr").val(tgllhr);

            var masa = $(this).data('masa');
            $(".masa").val(masa);

            var tmptkerjasblm = $(this).data('tmptkerjasblm');
            $(".tmptkerjasblm").val(tmptkerjasblm);


            var status = $(this).data('status');
            if (status == 'Tetap')
                $(".status option[value=Tetap]").attr('selected', 'selected');
            else
                $(".status option[value=Kontrak]").attr('selected', 'selected');

            var agama = $(this).data('agama');
            if (agama == 'Islam')
                $(".agama option[value=Islam]").attr('selected', 'selected');
            else if (agama == 'Katholik')
                $(".agama option[value=Katholik]").attr('selected', 'selected');
            else if (status == 'Kristen')
                $(".agama option[value=Kristen]").attr('selected', 'selected');
            else if (status == 'Hindu')
                $(".agama option[value=Hindu]").attr('selected', 'selected');
            else if (status == 'Buddha')
                $(".agama option[value=Buddha]").attr('selected', 'selected');
            else
                $(".agama option[value=Lain-lain]").attr('selected', 'selected');


        });
        $(document).on("click", '.edit_kend', function (e) {

            var idkend = $(this).data('idkend');
            $(".idkend").val(idkend);
            var namakend = $(this).data('namakend');
            $(".namakend").val(namakend);

            var nopol = $(this).data('nopol');
            $(".nopol").val(nopol);


            var warna = $(this).data('warna');
            if (warna == 'Hitam')
                $(".warna option[value=Hitam]").attr('selected', 'selected');
            else
                $(".warna option[value=Merah]").attr('selected', 'selected');

            var jeniskend = $(this).data('jeniskend');
            if (jeniskend == 'JK-01')
                $(".jeniskend option[value=JK-01]").attr('selected', 'selected');
            else if (jeniskend == 'JK-02')
                $(".jeniskend option[value=JK-02]").attr('selected', 'selected');
            else if (jeniskend == 'JK-03')
                $(".jeniskend option[value=JK-03]").attr('selected', 'selected');
            else if (jeniskend == 'JK-04')
                $(".jeniskend option[value=JK-04]").attr('selected', 'selected');
            else if (jeniskend == 'JK-05')
                $(".jeniskend option[value=JK-05]").attr('selected', 'selected');
            else
                $(".jeniskend option[value=JK-06]").attr('selected', 'selected');

        });

        }
    </script>

@endsection
