@extends('template')
@section('content')

    <div id="inputpass" class="box">
        <!-- /.box-header -->
        <div class="panel-body">


            <h2>Ubah Data Pendaftar</h2>
            <form action="{{ route('pendaftar.update') }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <?php
                $pendaf = $pendaftar;
                $now = date('Y-m-d');
                ?>
                <input type="hidden" name="_method" value="PATCH">
                <input type="hidden" name="ID_PENDAFTAR" value="<?= $pendaf['ID_PENDAFTAR'] ?>">


                <div class="form-group">
                    <label for="title">Nama Pendaftar</label>
                    <input type="text" name="NAMA_PENDAFTAR" class="form-control" value="{{$pendaf['NAMA_PENDAFTAR']}}"
                           required>
                </div>
                <div class="form-group">
                    <label for="title">Nomor Identitas (KTP)</label>
                    <input type="number" name="NO_IDENTITAS" class="form-control" value="{{$pendaf['NO_IDENTITAS']}}"
                           required>
                </div>
                <div class="form-group">
                    <label for="title">Tempat Lahir</label>
                    <input type="text" name="TEMPAT_LAHIR" class="form-control" value="{{$pendaf['TEMPAT_LAHIR']}}"
                           required>
                </div>
                <div class="form-group">
                    <label for="title">Tanggal Lahir</label>
                    <input type="date" name="TGL_LAHIR" class="form-control" value="{{$pendaf['TGL_LAHIR']}}" required>
                </div>
                <div class="form-group">
                    <label for="title">Agama</label>
                    <select class="form-control" id="agama" name="AGAMA">
                        <option value="Islam">Islam</option>
                        <option value="Katholik">Katholik</option>
                        <option value="Kristen">Kristen</option>
                        <option value="Hindu">Hindu</option>
                        <option value="Buddha">Buddha</option>
                        <option value="Lain-lain">Lain-lain</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="title">Alamat Rumah</label>
                    <input type="text" name="ALAMAT_RUMAH" class="form-control" value="{{$pendaf['ALAMAT_RUMAH']}}"
                           required>
                </div>
                <div class="form-group">
                    <label for="title">Nomor Telepon</label>
                    <input type="number" name="NO_TELP" class="form-control" value="{{$pendaf['NO_TELP']}}" required>
                </div>
                <div class="form-group">
                    <label for="title">Instansi</label>
                    <input type="text" name="INSTANSI" class="form-control" value="{{$pendaf['INSTANSI']}}" required>
                </div>
                <div class="form-group">
                    <label for="title">Alamat Kantor</label>
                    <input type="text" name="ALAMAT_KANTOR" class="form-control" value="{{$pendaf['ALAMAT_KANTOR']}}"
                           required>
                </div>
                <div class="form-group">
                    <label for="title">Nomor Telepon Kantor</label>
                    <input type="number" name="NO_TELP_KANTOR" class="form-control"
                           value="{{$pendaf['NO_TELP_KANTOR']}}" required>
                </div>
                <div class="form-group">
                    <label for="title">Nomor Pegawai</label>
                    <input type="text" name="NO_PEGAWAI" class="form-control" value="{{$pendaf['NO_PEGAWAI']}}"
                           required>
                </div>
                <div class="form-group">
                    <label for="title">Pekerjaan</label>
                    <input type="text" name="PEKERJAAN" class="form-control" value="{{$pendaf['PEKERJAAN']}}" required>
                </div>
                <div class="form-group">
                    <label for="title">Status Pekerjaan</label>
                    <select class="form-control" id="statuskerja" name="STATUS_KERJA"
                            value="{{$pendaf['STATUS_KERJA']}}">
                        <option value="Tetap">Tetap</option>
                        <option value="Kontrak">Kontrak</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="title">Masa Kerja</label>
                    <input type="text" name="MASA_KERJA" class="form-control" value="{{$pendaf['MASA_KERJA']}}"
                           required>
                </div>
                <div class="form-group">
                    <label for="title">Tempat Kerja Sebelumnya</label>
                    <input type="text" name="TEMPAT_KERJA_SBLMNYA" class="form-control"
                           value="{{$pendaf['TEMPAT_KERJA_SBLMNYA']}}" required>
                </div>

                <div class="form-group">
                    <button class="btn btn-success" href="{{ route('pendaftar.update') }}" data-toggle="confirmation"
                            data-title="Anda yakin data yang dimasukkan sudah benar?" data-btn-ok-label="Ya"
                            data-btn-cancel-label="Batal"><i class="glyphicon glyphicon-floppy-saved"></i>Simpan
                    </button>
                    <a href="{{ route('pendaftar.index',$pendaf['ID_PENDAFTAR']) }}" type="button"
                       class="btn btn-danger"><i class="glyphicon glyphicon-floppy-remove"></i>Batal</a>

                </div>
            </form>
        </div>
    </div>
    <!-- /.tab-pane -->
    <!--/.Modal-->
    <!-- Modal -->






@endsection
@section('custom_script')
    <!-- jQuery 2.2.3 -->
    <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <!-- page script -->
    <script type="text/javascript" src="{{URL::asset('plugins/jQuery/bootstrap-confirmation.min.js')}}"></script>
    <script>
        $(document).find('[data-toggle="confirmation"]').confirmation();
    </script>
    <script>
        document.getElementById('agama').value = '{{$pendaf['AGAMA']}}';
        document.getElementById('statuskerja').value = '{{$pendaf['STATUS_KERJA']}}';
    </script>
    </body>










    <script type="text/javascript">

        $(document).on("click", '.edit_button', function (e) {

            var id = $(this).data('id');
            $(".id").val(id);
            var nama = $(this).data('nama');
            $(".nama").val(nama);

            var identitas = $(this).data('identitas');
            $(".identitas").val(identitas);

            var alamatrmh = $(this).data('alamatrmh');
            $(".alamatrmh").val(alamatrmh);
            var telprmh = $(this).data('telprmh');
            $(".telprmh").val(telprmh);
            var nopeg = $(this).data('nopeg');
            $(".nopeg").val(nopeg);
            var pkrjn = $(this).data('pkrjn');
            $(".pkrjn").val(pkrjn);
            var instansi = $(this).data('instansi');
            $(".instansi").val(instansi);
            var alamatkntr = $(this).data('alamatkntr');
            $(".alamatkntr").val(alamatkntr);
            var telpkntr = $(this).data('telpkntr');
            $(".telpkntr").val(telpkntr);
            var nopeg = $(this).data('nopeg');
            $(".nopeg").val(nopeg);
            var tmptlhr = $(this).data('tmptlhr');
            $(".tmptlhr").val(tmptlhr);

            var tgllhr = $(this).data('tgllhr');
            $(".tgllhr").val(tgllhr);

            var masa = $(this).data('masa');
            $(".masa").val(masa);

            var tmptkerjasblm = $(this).data('tmptkerjasblm');
            $(".tmptkerjasblm").val(tmptkerjasblm);


            var status = $(this).data('status');
            if (status == 'Tetap')
                $(".status option[value=Tetap]").attr('selected', 'selected');
            else
                $(".status option[value=Kontrak]").attr('selected', 'selected');

            var agama = $(this).data('agama');
            if (agama == 'Islam')
                $(".agama option[value=Islam]").attr('selected', 'selected');
            else if (agama == 'Katholik')
                $(".agama option[value=Katholik]").attr('selected', 'selected');
            else if (status == 'Kristen')
                $(".agama option[value=Kristen]").attr('selected', 'selected');
            else if (status == 'Hindu')
                $(".agama option[value=Hindu]").attr('selected', 'selected');
            else if (status == 'Buddha')
                $(".agama option[value=Buddha]").attr('selected', 'selected');
            else
                $(".agama option[value=Lain-lain]").attr('selected', 'selected');


        });
        $(document).on("click", '.edit_kend', function (e) {

            var idkend = $(this).data('idkend');
            $(".idkend").val(idkend);
            var namakend = $(this).data('namakend');
            $(".namakend").val(namakend);

            var nopol = $(this).data('nopol');
            $(".nopol").val(nopol);


            var warna = $(this).data('warna');
            if (warna == 'Hitam')
                $(".warna option[value=Hitam]").attr('selected', 'selected');
            else
                $(".warna option[value=Merah]").attr('selected', 'selected');

            var jeniskend = $(this).data('jeniskend');
            if (jeniskend == 'JK-01')
                $(".jeniskend option[value=JK-01]").attr('selected', 'selected');
            else if (jeniskend == 'JK-02')
                $(".jeniskend option[value=JK-02]").attr('selected', 'selected');
            else if (jeniskend == 'JK-03')
                $(".jeniskend option[value=JK-03]").attr('selected', 'selected');
            else if (jeniskend == 'JK-04')
                $(".jeniskend option[value=JK-04]").attr('selected', 'selected');
            else if (jeniskend == 'JK-05')
                $(".jeniskend option[value=JK-05]").attr('selected', 'selected');
            else
                $(".jeniskend option[value=JK-06]").attr('selected', 'selected');

        });

        }
    </script>

@endsection
