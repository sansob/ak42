@extends('template')
@section('content')

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Data Table With Full Features</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @if($periode == 'bulanan')
                <form action="{{ route('transaksi.update') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <?php $U = $seripass;?>
                    <input type="hidden" name="_method" value="PATCH">
                    <input type="hidden" name="SERI_PASS_ORG_BLN" value="<?= $U['SERI_PASS_ORG_BLN'] ?>">
                    <div class="form-group">
                        <label for="title">Catatan Khusus</label>
                        <input type="text" name="CATATAN_ORG_BLN">
                    </div>
                    <div class="form-group">
                        <label for="title">Jenis Pass</label>
                        <select class="form-control" name="ID_JENIS_PASS" value="<?= $U['ID_JENIS_PASS'] ?>">
                            <option value="JP-01">CIQ / NPA</option>
                            <option value="JP-02">INSTANSI / NPA</option>
                            <option value="JP-03">Airlines / NPA</option>
                            <option value="JP-04">Airlines / RPA</option>
                            <option value="JP-05">Non Aeronautika / NP</option>
                            <option value="JP-06">Aeronautika / NPA</option>
                            <option value="JP-07">OJT / RPA</option>
                            <option value="JP-08">OJT / NPA</option>
                            <option value="JP-09">Area Publik</option>
                            <option value="JP-10">Umum</option>
                            <option value="JP-11">Rek. / Kontrak AP.I</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="title">Keterangan Zona</label></br>
                        <!-- <input type="text" name="KET_ZONA_BLN"  > -->
                        <input type="checkbox" name="KET_ZONA_BLN[]" value="A">Arrival penumpang<br>
                        <input type="checkbox" name="KET_ZONA_BLN[]" value="B">Boarding lounge penumpang<br>
                        <input type="checkbox" name="KET_ZONA_BLN[]" value="C">Check-in / Pelaporan diri<br>
                        <input type="checkbox" name="KET_ZONA_BLN[] " value="F">bagian luar gudang kargo atau halaman
                        gudang kargo...<br>
                        <input type="checkbox" name="KET_ZONA_BLN[]" value="G">Bagian dalam gudang kargo<br>
                        <input type="checkbox" name="KET_ZONA_BLN[]" value="L">Gedung listrik<br>
                        <input type="checkbox" name="KET_ZONA_BLN[]" value="M">Daerah fasilitas meteorologi<br>
                        <input type="checkbox" name="KET_ZONA_BLN[]" value="N">Gedung daerah peralatan navigasi dan
                        telekomunikas...<br>
                        <input type="checkbox" name="KET_ZONA_BLN[]" value="O">Daerah fasilitas suplai bahan bakar<br>
                        <input type="checkbox" name="KET_ZONA_BLN[]" value="P">Platform / apron area<br>
                        <input type="checkbox" name="KET_ZONA_BLN[]" value="R">Gedung radar<br>
                        <input type="checkbox" name="KET_ZONA_BLN[]" value="T">Tower<br>
                        <input type="checkbox" name="KET_ZONA_BLN[]" value="U">Daerah penyimpanan bagasi tercatat sisi
                        udara kecu...<br>
                        <input type="checkbox" name="KET_ZONA_BLN[]" value="V">Tower, gedung radar, gedung daerah
                        navigasi & tele...<br>

                        <!-- <select class="form-control" name="id_role" value="">
                            <option value="1">ICT</option>
                            <option value="2">Pelayanan </option>
                            <option value="3">Security </option>
                            <option value="4">Sales </option>
                        </select> -->
                    </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-success">
                <i class="glyphicon glyphicon-floppy-saved"></i> Simpan
            </button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i
                        class="glyphicon glyphicon-floppy-remove"><a href="{{ route('transaksi.index') }}"></i>
                Batal</a></button>
            </button>
        </div>
        </form>
        @endif
        @if($periode =='tahunan')
            <form action="{{ route('transaksi.updateorangtahun') }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <?php $U = $seripass;?>
                <input type="hidden" name="_method" value="PATCH">
                <input type="hidden" name="SERI_PASS_ORG_THN" value="<?= $U['SERI_PASS_ORG_THN'] ?>">
                <div class="form-group">
                    <label for="title">Catatan Khusus</label>
                    <input type="text" name="CATATAN_ORG_THN">
                </div>
                <div class="form-group">
                    <label for="title">Jenis Pass</label>
                    <select class="form-control" name="ID_JENIS_PASS" value="<?= $U['ID_JENIS_PASS'] ?>">
                        <option value="JP-01">CIQ / NPA</option>
                        <option value="JP-02">INSTANSI / NPA</option>
                        <option value="JP-03">Airlines / NPA</option>
                        <option value="JP-04">Airlines / RPA</option>
                        <option value="JP-05">Non Aeronautika / NP</option>
                        <option value="JP-06">Aeronautika / NPA</option>
                        <option value="JP-07">OJT / RPA</option>
                        <option value="JP-08">OJT / NPA</option>
                        <option value="JP-09">Area Publik</option>
                        <option value="JP-10">Umum</option>
                        <option value="JP-11">Rek. / Kontrak AP.I</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="title">Keterangan Zona</label></br>
                    <!-- <input type="text" name="KET_ZONA_THN"  > -->
                    <!-- <input type="checkbox" name="A" value="A">Arrival penumpang<br>
                    <input type="checkbox" name="B" value="B" >Boarding lounge penumpang<br> -->
                    <!-- <input type="checkbox" name="C" value="C">Check-in / Pelaporan diri<br>
                    <input type="checkbox" name="F" value="F" >bagian luar gudang kargo atau halaman gudang kargo...<br>
                    <input type="checkbox" name="G" value="G">Bagian dalam gudang kargo<br>
                    <input type="checkbox" name="L" value="L" >Gedung listrik<br>
                    <input type="checkbox" name="M" value="M">Daerah fasilitas meteorologi<br>
                    <input type="checkbox" name="N" value="N" >Gedung daerah peralatan navigasi dan telekomunikas...<br>
                    <input type="checkbox" name="O" value="O">Daerah fasilitas suplai bahan bakar<br>
                    <input type="checkbox" name="P" value="P" >Platform / apron area<br>
                    <input type="checkbox" name="R" value="R">Gedung radar<br>
                    <input type="checkbox" name="T" value="T" >Tower<br>
                    <input type="checkbox" name="U" value="U">Daerah penyimpanan bagasi tercatat sisi udara kecu...<br>
                    <input type="checkbox" name="V" value="V" >Tower, gedung radar, gedung daerah navigasi & tele...<br> -->
                    <input type="checkbox" name="KET_ZONA_BLN[]" value="A">Arrival penumpang<br>
                    <input type="checkbox" name="KET_ZONA_BLN[]" value="B">Boarding lounge penumpang<br>
                    <input type="checkbox" name="KET_ZONA_BLN[]" value="C">Check-in / Pelaporan diri<br>
                    <input type="checkbox" name="KET_ZONA_BLN[] " value="F">bagian luar gudang kargo atau halaman gudang
                    kargo...<br>
                    <input type="checkbox" name="KET_ZONA_BLN[]" value="G">Bagian dalam gudang kargo<br>
                    <input type="checkbox" name="KET_ZONA_BLN[]" value="L">Gedung listrik<br>
                    <input type="checkbox" name="KET_ZONA_BLN[]" value="M">Daerah fasilitas meteorologi<br>
                    <input type="checkbox" name="KET_ZONA_BLN[]" value="N">Gedung daerah peralatan navigasi dan
                    telekomunikas...<br>
                    <input type="checkbox" name="KET_ZONA_BLN[]" value="O">Daerah fasilitas suplai bahan bakar<br>
                    <input type="checkbox" name="KET_ZONA_BLN[]" value="P">Platform / apron area<br>
                    <input type="checkbox" name="KET_ZONA_BLN[]" value="R">Gedung radar<br>
                    <input type="checkbox" name="KET_ZONA_BLN[]" value="T">Tower<br>
                    <input type="checkbox" name="KET_ZONA_BLN[]" value="U">Daerah penyimpanan bagasi tercatat sisi udara
                    kecu...<br>
                    <input type="checkbox" name="KET_ZONA_BLN[]" value="V">Tower, gedung radar, gedung daerah navigasi &
                    tele...<br>
                    <!-- <select class="form-control" name="id_role" value="">
                        <option value="1">ICT</option>
                        <option value="2">Pelayanan </option>
                        <option value="3">Security </option>
                        <option value="4">Sales </option>
                    </select> -->
                </div>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-success">
            <i class="glyphicon glyphicon-floppy-saved"></i> Simpan
        </button>
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i
                    class="glyphicon glyphicon-floppy-remove"><a href="{{ route('transaksi.index') }}"></i> Batal</a>
        </button>
        </button>
    </div>
    </form>
    @endif
    </div>
    <!-- /.box-body -->
    </div>
    <button type="button" class="btn btn-success btn-xs  fa fa-plus" data-toggle="modal"
            data-target="#myModal"></button>
    <!-- /.box -->
    </div>
    <!-- /.col -->
    </div>
    <!-- /.row -->

    <!--/.Modal-->
    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah User</h4>
                </div>
                <div class="modal-body">

                </div>
            </div>

        </div>
    </div>






@endsection
@section('custom_script')
    <!-- jQuery 2.2.3 -->
    <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <!-- page script -->
    <script type="text/javascript" src="{{URL::asset('plugins/jQuery/bootstrap-confirmation.min.js')}}"></script>
    <script>
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });
    </script>
    <script>
        $(document).find('[data-toggle="confirmation"]').confirmation();
    </script>
    <script type="text/javascript">
        $(document).on("click", '.edit_button', function (e) {

            var id = $(this).data('id');
            $(".id").val(id);
            var nama = $(this).data('nama');
            $(".nama").val(nama);

            var username = $(this).data('username');
            $(".username").val(username);

            var password = $(this).data('password');
            $(".password").val(password);


            var unitkerja = $(this).data('unitkerja');
            if (unitkerja == 'ICT')
                $(".unitkerja option[value=1]").attr('selected', 'selected');
            else if (unitkerja == 'Pelayanan')
                $(".unitkerja option[value=2]").attr('selected', 'selected');
            else if (unitkerja == 'Security')
                $(".unitkerja option[value=3]").attr('selected', 'selected');
            else if (unitkerja == 'Sales')
                $(".unitkerja option[value=4]").attr('selected', 'selected');

            var hakakses = $(this).data('hakakses');
            if (hakakses == 'Super Admin')
                $(".hakakses option[value=1]").attr('selected', 'selected');
            else if (hakakses == 'Admin')
                $(".hakakses option[value=2]").attr('selected', 'selected');
            else if (hakakses == 'User')
                $(".hakakses option[value=3]").attr('selected', 'selected');


        });
    </script>



    <script type="text/javascript">
        function autoRefresh_div() {
            $("#links").load('coba #links', function () {
                setTimeout(autoRefresh_div, 60000);
            });
        }

        autoRefresh_div();
    </script>





@endsection
