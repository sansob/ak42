@extends('template-front')
@section('content')
    <div class="row">
        <!-- /.col-lg-12 -->
        <div class="col-lg-12">
            <h2>Mendaftar Pembuatan Pass</h2>
        </div>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div id="inputtrans" class="panel-body">
                    <form action="{{ route('transaksi.store') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <?php

                        $getmax = DB::table('transaksi_pass')->max('ID_TRANSAKSI');

                        if ($getmax == null) {
                            $next = "0001";
                        } else {
                            $angka = substr($getmax, 3, 4);
                            $next = str_repeat("0", 4 - strlen($angka + 1)) . ($angka + 1);
                        }

                        $id = "TR-" . $next;

                        $now = date('Y-m-d');
                        ?>

                        <input type="hidden" id="ID_TRANSAKSI" name="ID_TRANSAKSI" value="<?= $id ?>">

                        <input type="hidden" name="TGL_TRANSAKSI" value="<?= $now ?>">
                        <div class="form-group">
                            <label for="title">ID Transaksi</label>
                            <input type="text" id="idtrans" name="ID_TRANSAKSI" class="form-control" value="{{$id}}"
                                   required disabled>
                        </div>
                        <div class="form-group">
                            <label for="title">Nama Pemohon</label>
                            <input type="text" id="NAMA_PEMOHON" name="NAMA_PEMOHON" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="title">Nomor Surat Permohonan</label>
                            <input type="text" id="NO_SURAT_PEMOHON" name="NO_SURAT_PEMOHON" class="form-control"
                                   required>
                        </div>
                        <div class="form-group">
                            <label for="title">Keperluan</label>
                            <select class="form-control" name="STATUS_DAFTAR">
                                <option value="Baru">Mendaftar pass baru</option>
                                <option value="Perpanjang">Perpanjangan</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success btn-lg" data-toggle="confirmation"
                                    data-title="Anda yakin data yang diisikan sudah benar?" data-btn-ok-label="Ya"
                                    data-btn-cancel-label="Batal">Simpan
                            </button>
                            <a href="{{ route('home') }}" class="btn btn-danger btn-lg">Batal</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('custom_script')
    <!-- jQuery 2.2.3 -->
    <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <!-- page script -->
    <script type="text/javascript" src="{{URL::asset('plugins/jQuery/bootstrap-confirmation.min.js')}}"></script>
    <script>
        $(document).find('[data-toggle="confirmation"]').confirmation();
    </script>
    <script>

        $(function () {
            $("#kendaraanbulan").DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": false,
                "autoWidth": true
            });
            $("#kendaraantahun").DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": false,
                "autoWidth": true
            });
            $("#pendaftarbulan").DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": false,
                "autoWidth": true
            });
            $("#pendaftartahun").DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": false,
                "autoWidth": true
            });
        });
    </script>
    </body>










    <script type="text/javascript">

        $(document).on("click", '.edit_button', function (e) {

            var id = $(this).data('id');
            $(".id").val(id);
            var nama = $(this).data('nama');
            $(".nama").val(nama);

            var identitas = $(this).data('identitas');
            $(".identitas").val(identitas);

            var alamatrmh = $(this).data('alamatrmh');
            $(".alamatrmh").val(alamatrmh);
            var telprmh = $(this).data('telprmh');
            $(".telprmh").val(telprmh);
            var nopeg = $(this).data('nopeg');
            $(".nopeg").val(nopeg);
            var pkrjn = $(this).data('pkrjn');
            $(".pkrjn").val(pkrjn);
            var instansi = $(this).data('instansi');
            $(".instansi").val(instansi);
            var alamatkntr = $(this).data('alamatkntr');
            $(".alamatkntr").val(alamatkntr);
            var telpkntr = $(this).data('telpkntr');
            $(".telpkntr").val(telpkntr);
            var nopeg = $(this).data('nopeg');
            $(".nopeg").val(nopeg);
            var tmptlhr = $(this).data('tmptlhr');
            $(".tmptlhr").val(tmptlhr);

            var tgllhr = $(this).data('tgllhr');
            $(".tgllhr").val(tgllhr);

            var masa = $(this).data('masa');
            $(".masa").val(masa);

            var tmptkerjasblm = $(this).data('tmptkerjasblm');
            $(".tmptkerjasblm").val(tmptkerjasblm);


            var status = $(this).data('status');
            if (status == 'Tetap')
                $(".status option[value=Tetap]").attr('selected', 'selected');
            else
                $(".status option[value=Kontrak]").attr('selected', 'selected');

            var agama = $(this).data('agama');
            if (agama == 'Islam')
                $(".agama option[value=Islam]").attr('selected', 'selected');
            else if (agama == 'Katholik')
                $(".agama option[value=Katholik]").attr('selected', 'selected');
            else if (status == 'Kristen')
                $(".agama option[value=Kristen]").attr('selected', 'selected');
            else if (status == 'Hindu')
                $(".agama option[value=Hindu]").attr('selected', 'selected');
            else if (status == 'Buddha')
                $(".agama option[value=Buddha]").attr('selected', 'selected');
            else
                $(".agama option[value=Lain-lain]").attr('selected', 'selected');


        });
        $(document).on("click", '.edit_kend', function (e) {

            var idkend = $(this).data('idkend');
            $(".idkend").val(idkend);
            var namakend = $(this).data('namakend');
            $(".namakend").val(namakend);

            var nopol = $(this).data('nopol');
            $(".nopol").val(nopol);


            var warna = $(this).data('warna');
            if (warna == 'Hitam')
                $(".warna option[value=Hitam]").attr('selected', 'selected');
            else
                $(".warna option[value=Merah]").attr('selected', 'selected');

            var jeniskend = $(this).data('jeniskend');
            if (jeniskend == 'JK-01')
                $(".jeniskend option[value=JK-01]").attr('selected', 'selected');
            else if (jeniskend == 'JK-02')
                $(".jeniskend option[value=JK-02]").attr('selected', 'selected');
            else if (jeniskend == 'JK-03')
                $(".jeniskend option[value=JK-03]").attr('selected', 'selected');
            else if (jeniskend == 'JK-04')
                $(".jeniskend option[value=JK-04]").attr('selected', 'selected');
            else if (jeniskend == 'JK-05')
                $(".jeniskend option[value=JK-05]").attr('selected', 'selected');
            else
                $(".jeniskend option[value=JK-06]").attr('selected', 'selected');

        });

        }
    </script>

@endsection
