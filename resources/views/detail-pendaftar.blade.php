@extends('template')
@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Transaksi</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>

                    <th>ID Pendaftar</th>
                    <th>Nama Pendaftar</th>
                    <th>Nomor Identitas</th>
                    <th>Nomor Pegawai</th>
                    <th>Tempat Lahir</th>
                    <th>Tanggal Lahir</th>
                    <th>Agama</th>
                    <th>Instansi</th>
                    <th>Alamat Kantor</th>
                    <th>Alamat Rumah</th>
                    <th>Nomor Telpon</th>
                    <th>Nomor Telpon Kantor</th>
                    <th>Pekerjaan</th>
                    <th>Status Pekerjaan</th>
                    <th>Masa Kerja</th>
                    <th>Tempat Kerja Sebelumnya</th>
                    <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                @foreach($pendaftardetil as $Pe)
                    <tr>

                        <td>{{$Pe->ID_PENDAFTAR}}</td>
                        <td>{{ $Pe->NAMA_PENDAFTAR}}</td>
                        <td>{{ $Pe->NO_IDENTITAS}}</td>
                        <td>{{ $Pe->NO_PEGAWAI}}</td>
                        <td>{{ $Pe->TEMPAT_LAHIR}}</td>
                        <td>{{$Pe->TGL_LAHIR}}</td>
                        <td>{{ $Pe->AGAMA}}</td>
                        <td>{{ $Pe->INSTANSI}}</td>
                        <td>{{ $Pe->ALAMAT_KANTOR}}</td>
                        <td>{{ $Pe->ALAMAT_RUMAH}}</td>
                        <td>{{$Pe->NO_TELP}}</td>
                        <td>{{ $Pe->NO_TELP_KANTOR}}</td>
                        <td>{{ $Pe->PEKERJAAN}}</td>
                        <td>{{ $Pe->STATUS_KERJA}}</td>
                        <td>{{ $Pe->MASA_KERJA}}</td>
                        <td>{{ $Pe->TEMPAT_KERJA_SBLMNYA}}</td>
                        <td>

                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>

                </tfoot>
            </table>
        </div>
        <!-- /.box-body -->
    </div>

    <!-- /.box -->
    </div>
    <!-- /.col -->
    </div>
    <!-- /.row -->




    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
        <!-- <div class="modal-content">


    </div>
  </div>







@endsection
        @section('custom_script')
            <!-- jQuery 2.2.3 -->
                <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
                <!-- Bootstrap 3.3.6 -->
                <script src="bootstrap/js/bootstrap.min.js"></script>
                <!-- DataTables -->
                <script src="plugins/datatables/jquery.dataTables.min.js"></script>
                <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
                <!-- SlimScroll -->
                <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
                <!-- FastClick -->
                <script src="plugins/fastclick/fastclick.js"></script>
                <!-- AdminLTE App -->
                <script src="dist/js/app.min.js"></script>
                <!-- AdminLTE for demo purposes -->
                <script src="dist/js/demo.js"></script>
                <!-- page script -->
                <script>
                    $(function () {
                        $("#example1").DataTable();
                        $('#example2').DataTable({
                            "paging": true,
                            "lengthChange": false,
                            "searching": false,
                            "ordering": true,
                            "info": true,
                            "autoWidth": false
                        });
                    });
                </script>

                <script type="text/javascript">
                    $(document).on("click", '.edit_button', function (e) {

                        var id = $(this).data('id');
                        $(".id").val(id);

                    });
                </script>

@endsection
