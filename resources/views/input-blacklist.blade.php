@extends('template')
@section('content')
    <div class="row">
        <!-- /.col-lg-12 -->
        <div class="col-lg-12">
            <h2>Data Blacklist Pendaftar</h2>
        </div>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    {{ csrf_field() }}
                    <?php
                    $pendaf = $pendaftar;
                    $now = date('Y-m-d');
                    ?>

                    <form action="{{ route('blacklist.store') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <!-- <input type="hidden" name="_method" value="PATCH"> -->
                        <input type="hidden" name="ID_PENDAFTAR" value="<?=$pendaf['ID_PENDAFTAR']?>">
                        <div class="form-group">
                            <label for="title">Nama Pendaftar</label>
                            <input type="text" name="NAMA_PENDAFTAR" class="form-control" maxlength="1024"
                                   value="<?= $pendaf['NAMA_PENDAFTAR'] ?>" disabled>
                        </div>
                        <div class="form-group">
                            <label for="title">Nomor Identitas (KTP)</label>
                            <input type="number" name="NO_IDENTITAS" class="form-control"
                                   value="<?= $pendaf['NO_IDENTITAS'] ?>" maxlength="50" disabled>
                        </div>
                        <div class="form-group">
                            <label for="title">Alamat Rumah</label>
                            <input type="text" name="ALAMAT_RUMAH" class="form-control" maxlength="1024"
                                   value="<?= $pendaf['ALAMAT_RUMAH'] ?>" disabled>
                        </div>
                        <div class="form-group">
                            <label for="title">Nomor Telepon</label>
                            <input type="number" name="NO_TELP" class="form-control" maxlength="20"
                                   value="<?= $pendaf['NO_TELP'] ?>" disabled>
                        </div>
                        <div class="form-group">
                            <label for="title">Instansi</label>
                            <input type="text" name="INSTANSI" class="form-control" maxlength="1024"
                                   value="<?= $pendaf['INSTANSI'] ?>" disabled>
                        </div>
                        <div class="form-group">
                            <label for="title">Alamat Kantor</label>
                            <input type="text" name="ALAMAT_KANTOR" class="form-control" maxlength="1024"
                                   value="<?= $pendaf['ALAMAT_KANTOR'] ?>" disabled>
                        </div>
                        <div class="form-group">
                            <label for="title">Nomor Telepon Kantor</label>
                            <input type="number" name="NO_TELP_KANTOR" class="form-control" maxlength="20"
                                   value="<?= $pendaf['NO_TELP_KANTOR'] ?>" disabled>
                        </div>
                        <div class="form-group">
                            <label for="title">Nomor Pegawai</label>
                            <input type="text" name="NO_PEGAWAI" class="form-control" maxlength="50"
                                   value="<?= $pendaf['NO_PEGAWAI'] ?>" disabled>
                        </div>
                        <div class="form-group">
                            <label for="title">Pekerjaan</label>
                            <input type="text" name="PEKERJAAN" class="form-control" maxlength="1024"
                                   value="<?= $pendaf['PEKERJAAN'] ?>" disabled>
                        </div>
                        <div class="form-group">
                            <label for="title">Tanggal Blacklist</label>
                            <input type="date" name="TGL_BLACKLIST" class="form-control" value="{{$now}}" required>
                        </div>
                        <div class="form-group">
                            <label for="title">Alasan</label>
                            <input type="text" name="ALASAN" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn bg-navy btn-flat">
                                <i class="glyphicon glyphicon-floppy-saved"></i>Blacklist
                            </button>
                            <a href="{{ route('pilihblacklist') }}" type="button" class="btn btn-danger"><i
                                        class="glyphicon glyphicon-floppy-remove"></i>Batal</a>
                        </div>
                    </form>
                </div>
            </div>


        @endsection
        @section('custom_script')
            <!-- jQuery 2.2.3 -->
                <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
                <!-- Bootstrap 3.3.6 -->
                <script src="bootstrap/js/bootstrap.min.js"></script>
                <!-- DataTables -->
                <script src="plugins/datatables/jquery.dataTables.min.js"></script>
                <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
                <!-- SlimScroll -->
                <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
                <!-- FastClick -->
                <script src="plugins/fastclick/fastclick.js"></script>
                <!-- AdminLTE App -->
                <script src="dist/js/app.min.js"></script>
                <!-- AdminLTE for demo purposes -->
                <script src="dist/js/demo.js"></script>
                <!-- page script -->
                <script>

                    $(function () {
                        $("#blacklist").DataTable({
                            "paging": true,
                            "lengthChange": false,
                            "searching": true,
                            "ordering": false,
                            "info": false,
                            "autoWidth": true
                        });

                    });
                </script>
                </body>










                <script type="text/javascript">

                    $(document).on("click", '.edit_button', function (e) {

                        var id = $(this).data('id');
                        $(".id").val(id);
                        var nama = $(this).data('nama');
                        $(".nama").val(nama);

                        var identitas = $(this).data('identitas');
                        $(".identitas").val(identitas);

                        var alamatrmh = $(this).data('alamatrmh');
                        $(".alamatrmh").val(alamatrmh);
                        var telprmh = $(this).data('telprmh');
                        $(".telprmh").val(telprmh);
                        var nopeg = $(this).data('nopeg');
                        $(".nopeg").val(nopeg);
                        var pkrjn = $(this).data('pkrjn');
                        $(".pkrjn").val(pkrjn);
                        var instansi = $(this).data('instansi');
                        $(".instansi").val(instansi);
                        var alamatkntr = $(this).data('alamatkntr');
                        $(".alamatkntr").val(alamatkntr);
                        var telpkntr = $(this).data('telpkntr');
                        $(".telpkntr").val(telpkntr);
                        var nopeg = $(this).data('nopeg');
                        $(".nopeg").val(nopeg);
                        var tmptlhr = $(this).data('tmptlhr');
                        $(".tmptlhr").val(tmptlhr);

                        var tgllhr = $(this).data('tgllhr');
                        $(".tgllhr").val(tgllhr);

                        var masa = $(this).data('masa');
                        $(".masa").val(masa);

                        var tmptkerjasblm = $(this).data('tmptkerjasblm');
                        $(".tmptkerjasblm").val(tmptkerjasblm);


                        var status = $(this).data('status');
                        if (status == 'Tetap')
                            $(".status option[value=Tetap]").attr('selected', 'selected');
                        else
                            $(".status option[value=Kontrak]").attr('selected', 'selected');

                        var agama = $(this).data('agama');
                        if (agama == 'Islam')
                            $(".agama option[value=Islam]").attr('selected', 'selected');
                        else if (agama == 'Katholik')
                            $(".agama option[value=Katholik]").attr('selected', 'selected');
                        else if (status == 'Kristen')
                            $(".agama option[value=Kristen]").attr('selected', 'selected');
                        else if (status == 'Hindu')
                            $(".agama option[value=Hindu]").attr('selected', 'selected');
                        else if (status == 'Buddha')
                            $(".agama option[value=Buddha]").attr('selected', 'selected');
                        else
                            $(".agama option[value=Lain-lain]").attr('selected', 'selected');


                    });
                    $(document).on("click", '.edit_kend', function (e) {

                        var idkend = $(this).data('idkend');
                        $(".idkend").val(idkend);
                        var namakend = $(this).data('namakend');
                        $(".namakend").val(namakend);

                        var nopol = $(this).data('nopol');
                        $(".nopol").val(nopol);


                        var warna = $(this).data('warna');
                        if (warna == 'Hitam')
                            $(".warna option[value=Hitam]").attr('selected', 'selected');
                        else
                            $(".warna option[value=Merah]").attr('selected', 'selected');

                        var jeniskend = $(this).data('jeniskend');
                        if (jeniskend == 'JK-01')
                            $(".jeniskend option[value=JK-01]").attr('selected', 'selected');
                        else if (jeniskend == 'JK-02')
                            $(".jeniskend option[value=JK-02]").attr('selected', 'selected');
                        else if (jeniskend == 'JK-03')
                            $(".jeniskend option[value=JK-03]").attr('selected', 'selected');
                        else if (jeniskend == 'JK-04')
                            $(".jeniskend option[value=JK-04]").attr('selected', 'selected');
                        else if (jeniskend == 'JK-05')
                            $(".jeniskend option[value=JK-05]").attr('selected', 'selected');
                        else
                            $(".jeniskend option[value=JK-06]").attr('selected', 'selected');

                    });

                    }
                </script>

@endsection
