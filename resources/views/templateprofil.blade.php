<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sippaban</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="{{ URL::asset('bootstrap/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ URL::asset('plugins/datatables/dataTables.bootstrap.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ URL::asset('dist/css/AdminLTE.min.css') }}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ URL::asset('dist/css/skins/_all-skins.min.css') }}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ URL::asset('plugins/iCheck/flat/blue.css') }}">
    <!-- Morris chart -->
    <link rel="stylesheet" href="{{ URL::asset('plugins/morris/morris.css') }}">
    <!-- jvectormap -->
    <link rel="stylesheet" href="{{ URL::asset('plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
    <!-- Date Picker -->
    <link rel="stylesheet" href="{{ URL::asset('plugins/datepicker/datepicker3.css') }}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{ URL::asset('plugins/daterangepicker/daterangepicker.css') }}">
    <!-- bootstrap wysihtml5 - text editor -->


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-green-light sidebar-mini">
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini">AK1S</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg">Sippaban</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- Messages: style can be found in dropdown.less-->
                    <li>
                        <a href="{{ url('logout') }}"><i class="glyphicon glyphicon-log-out"></i> Keluar</a>
                    </li>


                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->

            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ asset('dist/img/avatar5.png') }}" class="img-circle" alt="User Image"/>

                </div>
                <div class="pull-left info">

                    <p>Hello {{ Auth::user()->NAMA_USER }}</p>


                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> online</a>
                </div>
            </div>


            <!-- search form -->

            <?php
            $transaksis = App\TransaksiModel::all();
            $jc = count($transaksis);
            $role = Auth::user()->id_role;
            $unit = Auth::user()->id_unit;
            $transaksireq = App\TransaksiModel::where('STATUS_TRANSAKSI', '=', 'Belum Ajukan Konfirmasi')->orWhere('STATUS_TRANSAKSI', '=', 'Perpanjang')->count();

            ?>

            </form>


            <ul class="sidebar-menu" id="links">
                <li class="header">MAIN NAVIGATION</li>
                @if($role==1 && $unit==1 )
                    <li>
                        <a href="{{ route('User.index') }}">
                            <i class="glyphicon glyphicon-user"></i> <span>User</span>
                            <span class="pull-right-container">
            </span>
                        </a>
                    </li>

                @endif

                <li>
                    <a href="{{ route('transaksi.index') }}">
                        <i class="glyphicon glyphicon-th-large"></i> <span>Pengajuan Pass </span>
                        <span class="pull-right-container">
                    <small class="label pull-right bg-red">{{$transaksireq}}</small>
            </span>
                    </a>
                </li>
            </ul>
            @if($unit != 4)
                <ul class="sidebar-menu">
                    <li class="header">Pass</li>
                    <!-- Optionally, you can add icons to the links -->
                    <li>
                        <a href="{{ route('expiredpass.index') }}">

                            <i class="glyphicon glyphicon-time"></i> <span>Expired Pass</span>
                            <span class="pull-right-container">

              </span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('transaksi.logtampil') }}">
                            <i class="glyphicon glyphicon-file"></i> <span>Log Transaksi</span>
                            <span class="pull-right-container">
            </span>
                        </a>
                    </li>
                </ul><!-- /.sidebar-menu -->
                <ul class="sidebar-menu">
                    <li class="header">Pendaftar & Kendaraan</li>
                    <!-- Optionally, you can add icons to the links -->
                    <li>
                        <a href="{{ route('pendaftar.index') }}">

                            <i class="glyphicon glyphicon-book"></i> <span>Pendaftar</span>
                            <span class="pull-right-container">

            </span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('kendaraan.index') }}">

                            <i class="fa fa-car"></i> <span>Kendaraan</span>
                            <span class="pull-right-container">

            </span>
                        </a>
                    </li>
                </ul>
                @if($unit != 3)
                    <ul class="sidebar-menu">
                        <li class="header">Report</li>
                        <!-- Optionally, you can add icons to the links -->

                        <li>
                            <a href="{{ route('report.index') }}">
                                <i class="fa fa-flag"></i> <span>Laporan </span>
                                <span class="pull-right-container">

              </span>
                            </a>
                        </li>
                    </ul>
                @endif
            @endif
            <ul class="sidebar-menu">
                <li class="header">Blacklist</li>
                <!-- Optionally, you can add icons to the links -->
                <li>
                    <a href="{{ route('datablacklist.index') }}">

                        <i class="fa fa-stop-circle"></i> <span>Data Blacklist</span>
                        <span class="pull-right-container">

             </span>
                    </a>
                </li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Sipabban
                <small>Administrative</small>
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            @yield('content')
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 2.3.8
        </div>
        <strong>Copyright &copy; 2014-2017. </strong> All rights
        reserved.
    </footer>

    <!-- Control Sidebar -->

    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>

<!-- jQuery 2.2.3 -->
<script type="text/javascript" src="{{URL::asset('plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
<!-- Bootstrap 3.3.6 -->
<script type="text/javascript" src="{{URL::asset('bootstrap/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('plugins/jQuery/bootstrap-confirmation.min.js')}}"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script type="text/javascript" src="{{URL::asset('plugins/morris/morris.min.js')}}"></script>
<!-- Sparkline -->
<script type="text/javascript" src="{{URL::asset('plugins/sparkline/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script type="text/javascript" src="{{URL::asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- jQuery Knob Chart -->
<script type="text/javascript" src="{{URL::asset('plugins/knob/jquery.knob.js')}}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script type="text/javascript" src="{{URL::asset('plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- datepicker -->
<script type="text/javascript" src="{{URL::asset('plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script type="text/javascript"
        src="{{URL::asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<!-- Slimscroll -->
<script type="text/javascript" src="{{URL::asset('plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script type="text/javascript" src="{{URL::asset('plugins/fastclick/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script type="text/javascript" src="{{URL::asset('dist/js/app.min.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script type="text/javascript" src="{{URL::asset('dist/js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script type="text/javascript" src="{{URL::asset('dist/js/demo.js')}}"></script>
@yield('custom_script')


</body>
</html>
