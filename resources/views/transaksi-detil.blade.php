@extends('templateprofil')
@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Transaksi</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <!-- /.col -->
            <div class="col-md-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#passob" data-toggle="tab">Data Pass Orang Bulanan</a></li>
                        <li><a href="#passot" data-toggle="tab">Data Pass Orang Tahunan</a></li>
                        <li><a href="#passkb" data-toggle="tab">Data Pass Kendaraan Bulanan</a></li>
                        <li><a href="#passkt" data-toggle="tab">Data Pass Kendaraan Tahunan</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="active tab-pane" id="passob">
                            <div class="tab-content">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <th>Seri Pass</th>
                                    <th>ID Transaksi</th>
                                    <th>ID Jenis Pass</th>
                                    <th>Nama Pendaftar</th>
                                    <th>Tanggal Mulai</th>
                                    <th>Tanggal Selesai</th>
                                    <th>Status</th>
                                    <th>Keterangan Zona</th>
                                    <th>Catatan</th>
                                    <th>Aksi</th>

                                    </thead>
                                    <tbody>
                                    @foreach($pass_orang_bulan as $PassOB)
                                        <tr>
                                            <td>{{ $PassOB->SERI_PASS_ORG_BLN}}</td>
                                            <td>{{ $PassOB->ID_TRANSAKSI}}</td>
                                            <td>{{ $PassOB->ID_JENIS_PASS}}</td>
                                            <td>{{ $PassOB->PendaftarPassBln->NAMA_PENDAFTAR}}</td>
                                            <td>{{ $PassOB->TGL_MULAI_ORG_BLN }}</td>
                                            <td>{{ $PassOB->TGL_SELESAI_ORG_BLN}}</td>
                                            <td>{{ $PassOB->STATUS_ORG_BLN}}</td>
                                            <td>{{ $PassOB->KET_ZONA_BLN}}</td>
                                            <td>{{$PassOB->CATATAN_ORG_BLN}}</td>
                                            <td>
                                                <?php

                                                if ($PassOB->STATUS_ORG_BLN == "Belum Lengkap") {

                                                    $a = "Berkas Lengkap";
                                                    $b = "'btn btn-xs btn-success'";
                                                } else if ($PassOB->STATUS_ORG_BLN == "Lengkap Belum Screening") {

                                                    $a = "Isikan";
                                                    $b = "'btn btn-xs btn-success'";
                                                } else if ($PassOB->STATUS_ORG_BLN == "Isikan") {
                                                    $a = "Selesai Screening";
                                                    $b = "'btn btn-xs btn-success'";
                                                } else if ($PassOB->STATUS_ORG_BLN == "Lengkap Sudah Screening") {
                                                    $a = "Konfirmasi detail";
                                                    $b = "'btn btn-xs btn-success'";
                                                } else if ($PassOB->STATUS_ORG_BLN == "Tidak Berlaku") {
                                                    $a = "";
                                                    $b = "";
                                                } else {
                                                    $a = "Belum Lengkap";
                                                    $b = "'btn btn-xs btn-danger'";
                                                }
                                                ?>
                                                <?php

                                                if ($PassOB->STATUS_ORG_BLN == "Lengkap Belum Screening ") {

                                                    $a = "Isikan";
                                                    $b = "'btn btn-xs btn-success'";
                                                } else if ($PassOB->STATUS_ORG_BLN == "Lengkap Sudah Screening") {
                                                    $a = "";
                                                    $b = "";
                                                }
                                                $roleid = Auth::user()->id_role;
                                                $unitid = Auth::user()->id_unit;

                                                ?>
                                                @if($PassOB->STATUS_ORG_BLN=="Belum Lengkap" && $roleid==3 && $unitid==2)
                                                    <a href="{{ route('transaksi.aktivasideaktivasiorangbulan',$PassOB->SERI_PASS_ORG_BLN) }}"
                                                       class=<?php echo "$b"; ?>><?php echo "$a"; ?></a>
                                                @endif
                                                @if($PassOB->STATUS_ORG_BLN!="Belum Lengkap"  && $roleid==3 && $unitid==3)
                                                    <a href="{{ route('transaksi.isikonfirmasiorgbulan',$PassOB->SERI_PASS_ORG_BLN) }}"
                                                       class=<?php echo "$b"; ?>><?php echo "$a"; ?></a>
                                                @endif
                                                <a href="{{ route('pendaftar.detil', $PassOB->ID_PENDAFTAR) }}"
                                                   class="btn fa fa-eye btn-xs btn-primary">Detail</a>
                                                @if(($PassOB->STATUS_ORG_BLN != "Belum Lengkap" or $PassOB->STATUS_ORG_THN != "Lengkap Sudah Screening")  and ($roleid===3 and $unitid===3))

                                                    <a href="{{ route('transaksi.edit', $PassOB->SERI_PASS_ORG_BLN) }}"
                                                       class="btn btn-xs btn-primary edit_button fa  fa-edit">Zona</a>
                                                @endif
                                                {{--@if($PassOB->STATUS_ORG_BLN!="Lengkap Sudah Screening"   && $roleid==3 && $unitid==3)--}}
                                                {{--<a href="{{ route('transaksi.edit', $PassOB->SERI_PASS_ORG_BLN) }}"--}}
                                                {{--class="btn btn-xs btn-primary edit_button fa  fa-edit">Zona</a>--}}
                                                {{--@endif--}}


                                            </td>


                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    </tfoot>
                                </table>


                                <!-- <button type="button" class="btn btn-success btn-xs  fa fa-plus" data-toggle="modal" data-target="#modaladdkeluarga"></button> -->

                            </div>
                            <!-- /.tab-pane -->


                            <!-- /.tab-pane -->
                        </div>
                        <div class="tab-pane" id="passot">
                            <table id="tabel_ot" class="table table-bordered table-striped">
                                <thead>
                                <th>Seri Pass</th>
                                <th>ID Transaksi</th>
                                <th>ID Jenis Pass</th>
                                <th>Nama Pendaftar</th>
                                <th>Tanggal Mulai</th>
                                <th>Tanggal Selesai</th>
                                <th>Status</th>
                                <th>Keterangan Zona</th>
                                <th>Catatan</th>
                                <th>Aksi</th>

                                </thead>
                                <tbody>
                                @foreach($pass_orang_tahun as $PassOT)
                                    <tr>
                                        <td>{{ $PassOT->SERI_PASS_ORG_THN}}</td>
                                        <td>{{ $PassOT->ID_TRANSAKSI}}</td>
                                        <td>{{ $PassOT->ID_JENIS_PASS}}</td>
                                        <td>{{ $PassOT->ID_PENDAFTAR}}</td>
                                        <td>{{ $PassOT->TGL_MULAI_ORG_THN }}</td>
                                        <td>{{ $PassOT->TGL_SELESAI_ORG_THN}}</td>
                                        <td>{{ $PassOT->STATUS_ORG_THN}}</td>
                                        <td>{{ $PassOT->KET_ZONA_THN}}</td>
                                        <td>{{$PassOT->CATATAN_ORG_THN}}</td>
                                        <td>
                                            <?php

                                            if ($PassOT->STATUS_ORG_THN == "Belum Lengkap") {

                                                $a = "Berkas Lengkap";
                                                $b = "'btn btn-xs btn-success'";
                                            } else if ($PassOT->STATUS_ORG_THN == "Lengkap Belum Screening") {

                                                $a = "Isikan";
                                                $b = "'btn btn-xs btn-success'";
                                            } else if ($PassOT->STATUS_ORG_THN == "Isikan") {
                                                $a = "Selesai Screening";
                                                $b = "'btn btn-xs btn-success'";
                                            } else if ($PassOT->STATUS_ORG_THN == "Lengkap Sudah Screening") {
                                                $a = "Konfirmasi detail";
                                                $b = "'btn btn-xs btn-success'";
                                            } else {
                                                $a = "Belum Lengkap";
                                                $b = "'btn btn-xs btn-danger'";
                                            }
                                            ?>
                                            <?php

                                            if ($PassOT->STATUS_ORG_THN == "Lengkap Belum Screening ") {

                                                $a = "Isikan";
                                                $b = "'btn btn-xs btn-success'";
                                            } else if ($PassOT->STATUS_ORG_THN == "Lengkap Sudah Screening") {
                                                $a = "";
                                                $b = "";
                                            }
                                            $roleid = Auth::user()->id_role;
                                            $unitid = Auth::user()->id_unit;

                                            ?>
                                            @if($PassOT->STATUS_ORG_THN=="Belum Lengkap" && $roleid==3 && $unitid==2)
                                                <a href="{{ route('transaksi.aktivasideaktivasiorangtahun',$PassOT->SERI_PASS_ORG_THN) }}"
                                                   class=<?php echo "$b"; ?>><?php echo "$a"; ?></a>
                                            @endif
                                            @if($PassOT->STATUS_ORG_THN!="Belum Lengkap"  && $roleid==3 && $unitid==3)
                                                <a href="{{ route('transaksi.isikonfirmasiorgtahun',$PassOT->SERI_PASS_ORG_THN) }}"
                                                   class=<?php echo "$b"; ?>><?php echo "$a"; ?></a>
                                            @endif
                                            <a href="{{ route('pendaftar.detil', $PassOT->ID_PENDAFTAR) }}"
                                               class="btn fa fa-eye btn-xs btn-primary">Detail</a>
                                            @if(($PassOT->STATUS_ORG_THN != "Belum Lengkap" or $PassOT->STATUS_ORG_THN != "Lengkap Sudah Screening")  and ($roleid===3 and $unitid===3))
                                                <a href="{{ route('transaksi.editorangtahun', $PassOT->SERI_PASS_ORG_THN) }}"
                                                   class="btn btn-xs btn-primary edit_button fa  fa-edit"> </a>
                                            @endif
                                        </td>


                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                </tfoot>
                            </table>


                        </div>
                        <!-- /.tab-pane -->


                        <div class="tab-pane" id="passkb">
                            <table id="tabel_kb" class="table table-bordered table-striped">
                                <thead>
                                <th>Seri Pass</th>
                                <th>ID Transaksi</th>
                                <th>ID Jenis Pass</th>
                                <th>Nama Pendaftar</th>
                                <th>Tanggal Mulai</th>
                                <th>Tanggal Selesai</th>
                                <th>Status</th>
                                <th>Aksi</th>

                                </thead>
                                <tbody>
                                @foreach($pass_kend_bulan as $PassKB)
                                    <tr>
                                        <td>{{ $PassKB->SERI_PASS_KEND_BLN}}</td>
                                        <td>{{ $PassKB->ID_TRANSAKSI}}</td>
                                        <td>{{ $PassKB->ID_KENDARAAN}}</td>
                                        <td>{{ $PassKB->PEMEGANG_BLN}}</td>
                                        <td>{{ $PassKB->TGL_MULAI_KEND_BLN }}</td>
                                        <td>{{ $PassKB->TGL_SELESAI_KEND_BLN}}</td>
                                        <td>{{ $PassKB->STATUS_KEND_BLN}}</td>
                                        <td>

                                            <?php
                                            $roleid = Auth::user()->id_role;
                                            $unitid = Auth::user()->id_unit;
                                            if ($PassKB->STATUS_KEND_BLN == "Belum Lengkap") {

                                                $a = "Berkas Lengkap";
                                                $b = "'btn btn-xs btn-success'";
                                            } else {
                                                $a = "Belum Lengkap";
                                                $b = "'btn btn-xs btn-danger'";
                                            }
                                            ?>
                                            @if($PassKB->STATUS_KEND_BLN=="Belum Lengkap" && $roleid==3 && $unitid==2)
                                                <a href="{{ route('transaksi.aktivasideaktivasikendbulan',$PassKB->SERI_PASS_KEND_BLN) }}"
                                                   class=<?php echo "$b"; ?>><?php echo "$a"; ?></a>
                                            @endif

                                        </td>


                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                </tfoot>
                            </table>


                        </div>
                        <div class="tab-pane" id="passkt">
                            <table id="tabel_kb" class="table table-bordered table-striped">
                                <thead>
                                <th>Seri Pass</th>
                                <th>ID Transaksi</th>
                                <th>ID Jenis Pass</th>
                                <th>Nama Pendaftar</th>
                                <th>Tanggal Mulai</th>
                                <th>Tanggal Selesai</th>
                                <th>Status</th>
                                <th>Aksi</th>

                                </thead>
                                <tbody>
                                @foreach($pass_kend_tahun as $PassKT)
                                    <tr>
                                        <td>{{ $PassKT->SERI_PASS_KEND_THN}}</td>
                                        <td>{{ $PassKT->ID_TRANSAKSI}}</td>
                                        <td>{{ $PassKT->ID_KENDARAAN}}</td>
                                        <td>{{ $PassKT->PEMEGANG_THN}}</td>
                                        <td>{{ $PassKT->TGL_MULAI_KEND_THN}}</td>
                                        <td>{{ $PassKT->TGL_SELESAI_KEND_THN}}</td>
                                        <td>{{ $PassKT->STATUS_KEND_THN}}</td>

                                        <td>
                                            <?php
                                            $roleid = Auth::user()->id_role;
                                            $unitid = Auth::user()->id_unit;
                                            if ($PassKT->STATUS_KEND_THN == "Belum Lengkap") {

                                                $a = "Berkas Lengkap";
                                                $b = "'btn btn-xs btn-success'";
                                            } else {
                                                $a = "Belum Lengkap";
                                                $b = "'btn btn-xs btn-danger'";
                                            }
                                            ?>
                                            @if($PassKT->STATUS_KEND_THN=="Belum Lengkap" && $roleid==3 && $unitid==2)
                                                <a href="{{ route('transaksi.aktivasideaktivasikendtahun',$PassKT->SERI_PASS_KEND_THN) }}"
                                                   class=<?php echo "$b"; ?><?php echo "$a"; ?>></a>
                                            @endif

                                        </td>


                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                </tfoot>
                            </table>


                        </div>

                        <!-- /.tab-content -->
                    </div>
                    <!-- /.nav-tabs-custom -->
                </div>
                <!-- /.col -->


            </div>
            <!-- /.row -->


            <!-- Modal -->


            <!-- Modal for Edit button -->

            <!-- Modal for Edit button -->


            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Tambah Data Pendidkan</h4>
                        </div>
                        <div class="modal-body">

                        </div>
                    </div>

                </div>
            </div>


            <div class="modal fade" id="modaledit" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Ubah Data Pendidkan</h4>
                        </div>
                        <div class="modal-body">

                        </div>
                    </div>

                </div>
            </div>


            <div class="modal fade" id="modaleditkeluarga" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Ubah Data Keluarga</h4>
                        </div>
                        <div class="modal-body">

                        </div>
                    </div>
                </div>
            </div>


            <div class="modal fade" id="modaladdkeluarga" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Tambah Data Keluarga</h4>
                        </div>
                        <div class="modal-body">

                        </div>
                    </div>
                </div>
            </div>


            @endsection
            @section('custom_script')

                <script>
                    $(function () {
                        $("#example1").DataTable({
                            "paging": true,
                            "lengthChange": false,
                            "searching": false,
                            "ordering": true,
                            "info": true,
                            "autoWidth": false
                        });
                        $("#tabel_ot").DataTable({
                            "paging": true,
                            "lengthChange": false,
                            "searching": false,
                            "ordering": true,
                            "info": true,
                            "autoWidth": false
                        });
                        $('#passkt').DataTable({
                            "paging": true,
                            "lengthChange": false,
                            "searching": false,
                            "ordering": true,
                            "info": true,
                            "autoWidth": false
                        });
                        $("#passot").DataTable({
                            "paging": true,
                            "lengthChange": false,
                            "searching": false,
                            "ordering": true,
                            "info": true,
                            "autoWidth": false
                        });
                        $('#passob').DataTable({
                            "paging": true,
                            "lengthChange": false,
                            "searching": false,
                            "ordering": true,
                            "info": true,
                            "autoWidth": false
                        });
                    });
                </script>
                <script>
                    $(document).find('[data-toggle="confirmation"]').confirmation();
                </script>


                <script type="text/javascript">
                    $(document).on("click", '.edit_button', function (e) {

                        var id = $(this).data('id');
                        $(".id").val(id);
                        var peg = $(this).data('peg');
                        $(".peg").val(peg);
                        var fakultas = $(this).data('fakultas');
                        $(".fakultas").val(fakultas);
                        var institusi = $(this).data('institusi');
                        $(".institusi").val(institusi);
                        var masuk = $(this).data('masuk');
                        $(".masuk").val(masuk);
                        var lulus = $(this).data('lulus');
                        $(".lulus").val(lulus);


                        var jenjang = $(this).data('jenjang');
                        if (jenjang == 'SD')
                            $(".jenjang option[value=SD]").attr('selected', 'selected');
                        else if (jenjang == 'SMP')
                            $(".jenjang option[value=SMP]").attr('selected', 'selected');
                        else if (jenjang == 'SMA')
                            $(".jenjang option[value=SMA]").attr('selected', 'selected');
                        else if (jenjang == 'S1')
                            $(".jenjang option[value=S1]").attr('selected', 'selected');
                        else if (jenjang == 'S2')
                            $(".jenjang option[value=S2]").attr('selected', 'selected');
                        else if (jenjang == 'S3')
                            $(".jenjang option[value=S3]").attr('selected', 'selected');


                    });
                </script>
                <script type="text/javascript">
                    $(document).on("click", '.keluargaedit_button', function (e) {

                        var kelid = $(this).data('kelid');
                        $(".kelid").val(kelid);
                        var kelpeg = $(this).data('kelpeg');
                        $(".kelpeg").val(kelpeg);
                        var kelnama = $(this).data('kelnama');
                        $(".kelnama").val(kelnama);
                        var kelpekerjaan = $(this).data('kelpekerjaan');
                        $(".kelpekerjaan").val(kelpekerjaan);
                        var keltgl = $(this).data('keltgl');
                        $(".keltgl").val(keltgl);
                        var keltelp = $(this).data('keltelp');
                        $(".keltelp").val(keltelp);
                        var kelemail = $(this).data('kelemail');
                        $(".kelemail").val(kelemail);
                        var kelalamat = $(this).data('kelalamat');
                        $(".kelalamat").val(kelalamat)
                        var keljk = $(this).data('keljk');
                        if (keljk == 'Pria') {

                            $(".pria").prop('checked', true);
                        }
                        else {
                            $(".wanita").prop('checked', true);
                        }

                        var kelposisi = $(this).data('kelposisi');
                        if (kelposisi == 'Ayah')
                            $(".kelposisi option[value=Ayah]").attr('selected', 'selected');
                        else if (kelposisi == 'Ibu')
                            $(".kelposisi option[value=Ibu]").attr('selected', 'selected');
                        else if (kelposisi == 'Anak')
                            $(".kelposisi option[value=Anak]").attr('selected', 'selected');
                        else if (kelposisi == 'Istri/Suami')
                            $(".kelposisi option[value=Istri/Suami]").attr('selected', 'selected');


                    });
                </script>


                <script type="text/javascript">
                    function autoRefresh_div() {
                        $("#links").load('coba #links', function () {
                            setTimeout(autoRefresh_div, 60000);
                        });
                    }

                    autoRefresh_div();
                </script>


@endsection
