@extends('template')
@section('content')
    <div class="row">
        <!-- /.col-lg-12 -->
        <div class="col-lg-12">
            <h2>Data Pendaftar</h2>
        </div>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    {{ csrf_field() }}
                    <?php
                    ?>

                    <label for="title">Data Pendaftar</label>
                    <table id="tblpendaftar" class="table table-bordered table-hover">
                        <thead>
                        <th>ID Pendaftar</th>
                        <th>Nama</th>
                        <th>Alamat Rumah</th>
                        <th>Nomor Telepon</th>
                        <th>Nomor Pegawai</th>
                        <th>Pekerjaan</th>
                        <th>Instansi</th>
                        <th>Alamat Kantor</th>
                        <th>Nomor Telepon Kantor</th>
                        <th>Aksi</th>

                        </thead>
                        <tbody>
                        <?php $Pendaftar = \App\PendaftarModel::where('STATUS_PENDAFTAR', '=', 'Terdaftar')->get();?>
                        @foreach($Pendaftar as $Pendaf)
                            <tr>
                                <td>{{ $Pendaf->ID_PENDAFTAR }}</td>
                                <td>{{ $Pendaf->NAMA_PENDAFTAR }}</td>
                                <td>{{ $Pendaf->ALAMAT_RUMAH}}</td>
                                <td>{{ $Pendaf->NO_TELP}}</td>
                                <td>{{ $Pendaf->NO_PEGAWAI }}</td>
                                <td>{{ $Pendaf->PEKERJAAN }}</td>
                                <td>{{ $Pendaf->INSTANSI}}</td>
                                <td>{{ $Pendaf->ALAMAT_KANTOR}}</td>
                                <td>{{ $Pendaf->NO_TELP_KANTOR}}</td>
                                <td>

                                    <a href="{{route('blacklist.show',$Pendaf->ID_PENDAFTAR)}}"
                                       class="btn bg-navy btn-flat" data-toggle="confirmation"
                                       data-title="Anda yakin data pendaftar yang dipilih sudah benar?"
                                       data-btn-ok-label="Ya" data-btn-cancel-label="Batal">Blacklist</a>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
            </div>


        @endsection
        @section('custom_script')
            <!-- jQuery 2.2.3 -->
                <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
                <!-- Bootstrap 3.3.6 -->
                <script src="bootstrap/js/bootstrap.min.js"></script>
                <!-- DataTables -->
                <script src="plugins/datatables/jquery.dataTables.min.js"></script>
                <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
                <!-- SlimScroll -->
                <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
                <!-- FastClick -->
                <script src="plugins/fastclick/fastclick.js"></script>
                <!-- AdminLTE App -->
                <script src="dist/js/app.min.js"></script>
                <!-- AdminLTE for demo purposes -->
                <script src="dist/js/demo.js"></script>
                <!-- page script -->
                <script type="text/javascript"
                        src="{{URL::asset('plugins/jQuery/bootstrap-confirmation.min.js')}}"></script>
                <script>
                    $(document).find('[data-toggle="confirmation"]').confirmation();
                </script>
                <script>

                    $(function () {
                        $("#tblpendaftar").DataTable({
                            "paging": true,
                            "lengthChange": false,
                            "searching": true,
                            "ordering": false,
                            "info": false,
                            "autoWidth": true
                        });

                    });
                </script>
                </body>










                <script type="text/javascript">

                    $(document).on("click", '.edit_button', function (e) {

                        var id = $(this).data('id');
                        $(".id").val(id);
                        var nama = $(this).data('nama');
                        $(".nama").val(nama);

                        var identitas = $(this).data('identitas');
                        $(".identitas").val(identitas);

                        var alamatrmh = $(this).data('alamatrmh');
                        $(".alamatrmh").val(alamatrmh);
                        var telprmh = $(this).data('telprmh');
                        $(".telprmh").val(telprmh);
                        var nopeg = $(this).data('nopeg');
                        $(".nopeg").val(nopeg);
                        var pkrjn = $(this).data('pkrjn');
                        $(".pkrjn").val(pkrjn);
                        var instansi = $(this).data('instansi');
                        $(".instansi").val(instansi);
                        var alamatkntr = $(this).data('alamatkntr');
                        $(".alamatkntr").val(alamatkntr);
                        var telpkntr = $(this).data('telpkntr');
                        $(".telpkntr").val(telpkntr);
                        var nopeg = $(this).data('nopeg');
                        $(".nopeg").val(nopeg);
                        var tmptlhr = $(this).data('tmptlhr');
                        $(".tmptlhr").val(tmptlhr);

                        var tgllhr = $(this).data('tgllhr');
                        $(".tgllhr").val(tgllhr);

                        var masa = $(this).data('masa');
                        $(".masa").val(masa);

                        var tmptkerjasblm = $(this).data('tmptkerjasblm');
                        $(".tmptkerjasblm").val(tmptkerjasblm);


                        var status = $(this).data('status');
                        if (status == 'Tetap')
                            $(".status option[value=Tetap]").attr('selected', 'selected');
                        else
                            $(".status option[value=Kontrak]").attr('selected', 'selected');

                        var agama = $(this).data('agama');
                        if (agama == 'Islam')
                            $(".agama option[value=Islam]").attr('selected', 'selected');
                        else if (agama == 'Katholik')
                            $(".agama option[value=Katholik]").attr('selected', 'selected');
                        else if (status == 'Kristen')
                            $(".agama option[value=Kristen]").attr('selected', 'selected');
                        else if (status == 'Hindu')
                            $(".agama option[value=Hindu]").attr('selected', 'selected');
                        else if (status == 'Buddha')
                            $(".agama option[value=Buddha]").attr('selected', 'selected');
                        else
                            $(".agama option[value=Lain-lain]").attr('selected', 'selected');


                    });
                    $(document).on("click", '.edit_kend', function (e) {

                        var idkend = $(this).data('idkend');
                        $(".idkend").val(idkend);
                        var namakend = $(this).data('namakend');
                        $(".namakend").val(namakend);

                        var nopol = $(this).data('nopol');
                        $(".nopol").val(nopol);


                        var warna = $(this).data('warna');
                        if (warna == 'Hitam')
                            $(".warna option[value=Hitam]").attr('selected', 'selected');
                        else
                            $(".warna option[value=Merah]").attr('selected', 'selected');

                        var jeniskend = $(this).data('jeniskend');
                        if (jeniskend == 'JK-01')
                            $(".jeniskend option[value=JK-01]").attr('selected', 'selected');
                        else if (jeniskend == 'JK-02')
                            $(".jeniskend option[value=JK-02]").attr('selected', 'selected');
                        else if (jeniskend == 'JK-03')
                            $(".jeniskend option[value=JK-03]").attr('selected', 'selected');
                        else if (jeniskend == 'JK-04')
                            $(".jeniskend option[value=JK-04]").attr('selected', 'selected');
                        else if (jeniskend == 'JK-05')
                            $(".jeniskend option[value=JK-05]").attr('selected', 'selected');
                        else
                            $(".jeniskend option[value=JK-06]").attr('selected', 'selected');

                    });

                    }
                </script>

@endsection
