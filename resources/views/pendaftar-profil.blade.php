@extends('templateprofil')
@section('content')

    <div class="row">
        <div class="col-md-3">

            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">

                    <h3 class="profile-username text-center">{{$pendaftar->NAMA_PENDAFTAR}}</h3>

                    <p class="text-muted text-center">{{$pendaftar->NO_IDENTITAS}}</p>
                    <hr>
                    <strong><i class="glyphicon glyphicon-user margin-r-5"></i>Data Pribadi</strong>
                    <p class="text-muted">
                        <br>
                        ID : {{$pendaftar->ID_PENDAFTAR}}
                        <br>
                        Nomor Pegawai : {{$pendaftar->NO_PEGAWAI}}
                        <br>
                        Tempat Lahir : {{$pendaftar->TEMPAT_LAHIR}}
                        <br>
                        Tanggal Lahir : {{$pendaftar->TGL_LAHIR}}
                        <br>
                        Agama : {{$pendaftar->AGAMA}}
                        <br>
                        Instansi : {{$pendaftar->INSTANSI}}
                    </p>
                    <hr>


                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <!-- About Me Box -->

            <!-- /.box -->
        </div>
        <!-- /.col -->

        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#activity" data-toggle="tab">Data Keluarga</a></li>
                    <li><a href="#timeline" data-toggle="tab">Data Pendidikan</a></li>
                    <li><a href="#dokumen" data-toggle="tab">Dokumen</a></li>
                    <li><a href="#cetak" data-toggle="tab">Cetak Pass</a></li>


                </ul>
                <div class="tab-content">
                    <div class="active tab-pane" id="activity">
                        <div class="tab-content">
                            <?php
                            $keluarga = DB::table('data_keluarga')
                                ->where('ID_PENDAFTAR', '=', $pendaftar->ID_PENDAFTAR)
                                ->get();
                            ?>
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <th>Nama Bapak</th>
                                <th>Nama Ibu</th>
                                <th>Alamat Orang Tua</th>
                                <th>Nama Pasangan</th>
                                <th>Alamat Pasangan</th>
                                <th>No Telepon Pasangan</th>
                                <th>Aksi</th>

                                </thead>
                                <tbody>
                                @foreach($keluarga as $Kel)
                                    <tr>
                                        <td>{{ $Kel->NAMA_BAPAK}}</td>
                                        <td>{{ $Kel->NAMA_IBU}}</td>
                                        <td>{{ $Kel->ALAMAT_ORTU}}</td>
                                        <td>{{ $Kel->NAMA_PASANGAN}}</td>
                                        <td>{{ $Kel->ALAMAT_PASANGAN }}</td>
                                        <td>{{ $Kel->NO_TELP_PSG}}</td>
                                        <td>

                                            <a class="btn btn-info btn-xs  fa  fa-edit keluargaedit_button"
                                               data-toggle="modal"></a>


                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                </tfoot>
                            </table>


                            <!-- <button type="button" class="btn btn-success btn-xs  fa fa-plus" data-toggle="modal" data-target="#modaladdkeluarga"></button> -->
                        </div>
                    </div>
                    <!-- /.tab-pane -->


                    <div class="tab-pane" id="timeline">
                        <?php
                        $pendidikan = DB::table('data_pendidikan')
                            ->where('ID_PENDAFTAR', '=', $pendaftar->ID_PENDAFTAR)
                            ->get();
                        ?>
                        <table id="table_pegawai" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>SD</th>
                                <th>Lokasi SD</th>
                                <th>Tahun Lulus SD</th>
                                <th>SMP</th>
                                <th>Lokasi SMP</th>
                                <th>Tahun Lulus SMP</th>
                                <th>SMA</th>
                                <th>Lokasi SMA</th>
                                <th>Tahun Lulus SMA</th>
                                <th>Pendidikan Tinggi</th>
                                <th>Lokasi PT</th>
                                <th>Tahun Lulus PT</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($pendidikan as $P)
                                <tr>
                                    <td>{{ $P->SD}}</td>
                                    <td>{{ $P->LOKASI_SD}}</td>
                                    <td>{{ $P->THN_LULUS_SD}}</td>
                                    <td>{{ $P->SMP}}</td>
                                    <td>{{ $P->LOKASI_SMP}}</td>
                                    <td>{{ $P->THN_LULUS_SMP}}</td>
                                    <td>{{ $P->SMA}}</td>
                                    <td>{{ $P->LOKASI_SMA}}</td>
                                    <td>{{ $P->THN_LULUS_SMA}}</td>
                                    <td>{{ $P->PEND_TINGGI}}</td>
                                    <td>{{ $P->LOKASI_PEND_TINGGI}}</td>
                                    <td>{{ $P->THN_LULUS_PEND_TINGGI}}</td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>


                    </div>

                    <div class="tab-pane" id="dokumen">

                        <?php
                        $dokuments = DB::table('documents')
                            ->where('ID_PENDAFTAR', '=', $pendaftar->ID_PENDAFTAR)
                            ->get();
                        ?>
                        <table id="table_pegawai" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Tipe Dokumen</th>
                                <th>Dokumen Name</th>
                                <th>Dokumen Preview</th>
                                <th>Action</th>


                            </tr>
                            </thead>
                            <tbody>
                            @foreach($dokuments as $Docs)
                                <tr>
                                    <td>{{ $Docs->DOCUMENT_TYPE}}</td>
                                    <td>{{ $Docs->DOCUMENT_NAME}}</td>
                                    <td><img src="{{url('/uploads', $Docs->DOCUMENT_NAME)}}" height="100px"
                                             width="100px" id="myImg"></td>
                                    <td>
                                        <button onclick="window.open('{{url('/uploads', $Docs->DOCUMENT_NAME)}}', '_blank')"
                                                type="button" class="btn btn-primary">
                                            Lihat {{$Docs->DOCUMENT_TYPE}}</button>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>

                        </table>


                    </div>

                    <?php

                    if ($pendaftar->STATUS_KERJA == "Bulanan") {
                        $cetaks = DB::table('pass_orang_bulan')
                            ->where('ID_PENDAFTAR', '=', $pendaftar->ID_PENDAFTAR)
                            ->get();
                    } else {
                        $cetaks = DB::table('pass_orang_tahun')
                            ->where('ID_PENDAFTAR', '=', $pendaftar->ID_PENDAFTAR)
                            ->get();

                    }

                    ?>

                    <div class="tab-pane" id="cetak">
                        <h3>Pass Print Wizard</h3>
                        <table id="table_pegawai" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Value</th>
                            </tr>
                            <tr>
                                <td>Nama Pendaftar</td>
                                <td><input type name="text" value="{{$pendaftar->NAMA_PENDAFTAR}}" readonly></td>

                            </tr>
                            <tr>
                                <td>Zona Pass</td>
                                <td>
                                    @foreach($cetaks as $cetak)
                                        {{$cetak->KET_ZONA_BLN}}

                                </td>
                            </tr>
                            <tr>
                                <td>Warna Pass</td>
                                <td>
                                    <select name="warnapass">
                                        <option  name="Kuning">Kuning</option>
                                        <option  name="Merah">Merah</option>
                                        <option  name="Biru">Biru</option>
                                    </select>
                                </td>

                            </tr>

                            <tr>
                                <td>Foto</td>
                                <td>
                                    <div class="form-group">
                                        <input class="default" id="foto" name="foto" type="file"
                                               accept="image/*">
                                    </div>
                                </td>

                            </tr>

                            <tr>
                                <td>Kode</td>
                                <td><input type name="text" name="kode_pass"></td>
                            </tr>

                            <tr>
                                <td>Berlaku Sampai</td>
                                <td>

                                    <?php  if ($pendaftar->STATUS_KERJA == "Bulanan") { ?>
                                    <input type name="text" value="
                                    {{$cetak->TGL_SELESAI_ORG_BLN}}
                                            " readonly></td>

                                <?php

                                }
                                else {
                                ?>
                                // set akhir tahun
                                // form akhir tahun
                                <?php
                                }
                                ?>
                                @endforeach
                            </tr>

                            <tr>
                                <td>

                                </td>
                                <td><input class="btn btn-info btn-xs" type="submit" name="cetak" value="Cetak"> </td>

                            </tr>

                            </thead>
                        </table>

                    </div>
                    <!-- /.tab-pane -->

                </div>
                <!-- /.row -->


                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- /.nav-tabs-custom -->
    </div>
    <!-- /.col -->


    </div>
    <!-- /.row -->




    <!-- Modal -->





    <!-- Modal for Edit button -->

    <!-- Modal for Edit button -->



    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Data Pendidkan</h4>
                </div>
                <div class="modal-body">

                </div>
            </div>

        </div>
    </div>





    <div class="modal fade" id="modaledit" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Ubah Data Pendidkan</h4>
                </div>
                <div class="modal-body">

                </div>
            </div>

        </div>
    </div>





    <div class="modal fade" id="modaleditkeluarga" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Ubah Data Keluarga</h4>
                </div>
                <div class="modal-body">

                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modaladdkeluarga" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Tambah Data Keluarga</h4>
                </div>
                <div class="modal-body">

                </div>
            </div>
        </div>
    </div>













@endsection
@section('custom_script')


    <script>
        $(document).find('[data-toggle="confirmation"]').confirmation();
    </script>


    <script type="text/javascript">
        $(document).on("click", '.edit_button', function (e) {

            var id = $(this).data('id');
            $(".id").val(id);
            var peg = $(this).data('peg');
            $(".peg").val(peg);
            var fakultas = $(this).data('fakultas');
            $(".fakultas").val(fakultas);
            var institusi = $(this).data('institusi');
            $(".institusi").val(institusi);
            var masuk = $(this).data('masuk');
            $(".masuk").val(masuk);
            var lulus = $(this).data('lulus');
            $(".lulus").val(lulus);


            var jenjang = $(this).data('jenjang');
            if (jenjang == 'SD')
                $(".jenjang option[value=SD]").attr('selected', 'selected');
            else if (jenjang == 'SMP')
                $(".jenjang option[value=SMP]").attr('selected', 'selected');
            else if (jenjang == 'SMA')
                $(".jenjang option[value=SMA]").attr('selected', 'selected');
            else if (jenjang == 'S1')
                $(".jenjang option[value=S1]").attr('selected', 'selected');
            else if (jenjang == 'S2')
                $(".jenjang option[value=S2]").attr('selected', 'selected');
            else if (jenjang == 'S3')
                $(".jenjang option[value=S3]").attr('selected', 'selected');


        });
    </script>



    <script type="text/javascript">
        $(document).on("click", '.keluargaedit_button', function (e) {

            var kelid = $(this).data('kelid');
            $(".kelid").val(kelid);
            var kelpeg = $(this).data('kelpeg');
            $(".kelpeg").val(kelpeg);
            var kelnama = $(this).data('kelnama');
            $(".kelnama").val(kelnama);
            var kelpekerjaan = $(this).data('kelpekerjaan');
            $(".kelpekerjaan").val(kelpekerjaan);
            var keltgl = $(this).data('keltgl');
            $(".keltgl").val(keltgl);
            var keltelp = $(this).data('keltelp');
            $(".keltelp").val(keltelp);
            var kelemail = $(this).data('kelemail');
            $(".kelemail").val(kelemail);
            var kelalamat = $(this).data('kelalamat');
            $(".kelalamat").val(kelalamat)
            var keljk = $(this).data('keljk');
            if (keljk == 'Pria') {

                $(".pria").prop('checked', true);
            }
            else {
                $(".wanita").prop('checked', true);
            }

            var kelposisi = $(this).data('kelposisi');
            if (kelposisi == 'Ayah')
                $(".kelposisi option[value=Ayah]").attr('selected', 'selected');
            else if (kelposisi == 'Ibu')
                $(".kelposisi option[value=Ibu]").attr('selected', 'selected');
            else if (kelposisi == 'Anak')
                $(".kelposisi option[value=Anak]").attr('selected', 'selected');
            else if (kelposisi == 'Istri/Suami')
                $(".kelposisi option[value=Istri/Suami]").attr('selected', 'selected');


        });
    </script>





@endsection
