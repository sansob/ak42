@extends('template')
@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Edit User</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <form action="{{ route('User.update') }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <?php $U = $user;?>
                <input type="hidden" name="_method" value="PATCH">
                <input type="hidden" name="id_user" value="<?= $U['id_user'] ?>">

                <div class="form-group">
                    <label for="title">Nama</label>
                    <input type="text" name="NAMA_USER" class="form-control" value="<?= $U['NAMA_USER'] ?>" required>
                </div>

                <div class="form-group">
                    <label for="title">Password (Kosongkan jika tidak di ganti)</label>
                    <input type="text" name="PASSWORD" class="form-control" value="<?= $U['PASSWORD'] ?>">
                </div>

                <div class="form-group">
                    <label for="title">Hak Akses</label>
                    <select class="form-control" id="id_role" name="id_role" value="">
                        <option value="1">Super Admin</option>
                        <option value="2">Admin</option>
                        <option value="3">User</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="title">Unit Kerja</label>
                    <select class="form-control" id="id_unit" name="id_unit" value="">
                        <option value="1">ICT</option>
                        <option value="2">Pelayanan</option>
                        <option value="3">Security</option>
                        <option value="4">Sales</option>
                    </select>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-success">
                        <i class="glyphicon glyphicon-floppy-saved"></i> Simpan
                    </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i
                                class="glyphicon glyphicon-floppy-remove"><a href="{{ route('User.index') }}"/></i>
                        Batal</a>
                    </button>
                    </button>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
    </div>
    {{--<button type="button" class="btn btn-success btn-xs  fa fa-plus" data-toggle="modal"--}}
    {{--data-target="#myModal"></button>--}}
    <!-- /.box -->
    </div>
    <!-- /.col -->
    </div>
    <!-- /.row -->

    <!--/.Modal-->
    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah User</h4>
                </div>
                <div class="modal-body">

                </div>
            </div>

        </div>
    </div>






@endsection
@section('custom_script')
    <!-- jQuery 2.2.3 -->
    <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <!-- page script -->
    <script type="text/javascript" src="{{URL::asset('plugins/jQuery/bootstrap-confirmation.min.js')}}"></script>
    <script>
        document.getElementById('id_unit').value = '<?= $U['id_unit'] ?>';
        document.getElementById('id_role').value = '<?= $U['id_role'] ?>';
    </script>
    <script>
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });
    </script>
    <script>
        $(document).find('[data-toggle="confirmation"]').confirmation();

    </script>
    <script type="text/javascript">
        $(document).on("click", '.edit_button', function (e) {

            var id = $(this).data('id');
            $(".id").val(id);
            var nama = $(this).data('nama');
            $(".nama").val(nama);

            var username = $(this).data('username');
            $(".username").val(username);

            var password = $(this).data('password');
            $(".password").val(password);


            var unitkerja = $(this).data('unitkerja');
            if (unitkerja == 'ICT')
                $(".unitkerja option[value=1]").attr('selected', 'selected');
            else if (unitkerja == 'Pelayanan')
                $(".unitkerja option[value=2]").attr('selected', 'selected');
            else if (unitkerja == 'Security')
                $(".unitkerja option[value=3]").attr('selected', 'selected');
            else if (unitkerja == 'Sales')
                $(".unitkerja option[value=4]").attr('selected', 'selected');

            var hakakses = $(this).data('hakakses');
            if (hakakses == 'Super Admin')
                $(".hakakses option[value=1]").attr('selected', 'selected');
            else if (hakakses == 'Admin')
                $(".hakakses option[value=2]").attr('selected', 'selected');
            else if (hakakses == 'User')
                $(".hakakses option[value=3]").attr('selected', 'selected');


        });
    </script>



    <script type="text/javascript">
        function autoRefresh_div() {
            $("#links").load('coba #links', function () {
                setTimeout(autoRefresh_div, 60000);
            });
        }

        autoRefresh_div();
    </script>





@endsection
